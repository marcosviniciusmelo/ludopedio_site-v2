=== AdRotate - Ad manager & AdSense Ads ===
Contributors: adegans
Donate link: https://ajdg.solutions/go/donate/adrotatefree
Tags: adverts, ads, banners, advert manager, ad manager, banner manager, monetise, revenue, place banners, google, adsense, dfp, doubleclick, amazon, affiliate, referral
Requires at least: 5.0
Requires PHP: 5.6
Tested up to: 5.7.2
Stable tag: 5.8.19
License: GPLv3

AdRotate is the only advert manager you'll ever need. Manage all your adverts and campaigns with ease. From any ad publisher. Manage your ads.txt, widgets and use the many powerful features to run successful campaigns.

== Description ==

If you are looking for an easy to use way to manage all your adverts from the dashboard you have come to the right place. AdRotate is easy to set up, easy to use and is built for your website.
AdRotate is designed to be simple to use, yet offers a professional set of features for newcomers and experienced users alike.

With AdRotate you can easily set up your own adverts and campaigns with standard HTML and/or Javascript code or use adverts from your favorite Ad Server such as Media.net, Blind Ferret, Yahoo! Adverts, DFP, Google AdSense, Google Ad Manager, Bing Ads, Amazon Affiliates, Chitika, Doubleclick, JuiceAds and many more similar ad servers and affiliate programs.

**Some of the AdRotate Features**

* Works with any advert
* Place random, or selected, adverts anywhere on your site
* Easily manage Adverts
* Easily manage Groups
* Easily manage Assets and media
* Track clicks, impressions and calculate the Cick-Through-Rate (CTR)
* Disguise adverts from ad blockers so they're less likely to be blocked
* Automagically disable Ads after they expire
* Use shortcodes anywhere they are supported in WordPress
* Place widgets for single adverts or groups
* Insert PHP snippets in your theme files to put Adverts anywhere on your site
* Dashboard and Email notifications when Adverts are about to expire or need attention
* Easily set up groups of Adverts in a grid, column or row
* Preview Adverts when editing
* Advert statistics
* Automatically delete short running adverts and stats after they expire
* Advertisers can add/edit/manage their own Adverts
* Geo Targeting for every country and city
* Mobile Adverts
* Portable Adverts
* Export statistics to CSV files

AdRotate and AdRotate Professional share many features. But some features are exclusively available in AdRotate Professional. Learn more about [AdRotate Professional](https://ajdg.solutions/product-category/adrotate-pro/?pk_campaign=adrotatefree&pk_keyword=readme) on my website.

**Translations**

* English (default)
* German
* Spanish
* French
* Dutch
* Italian
* Other translations available are provided by the community

== Installation ==

Installing AdRotate is as easy as searching for "AdRotate Arnan" or simply "AdRotate" in your plugin dashboard and clicking the "Install Now" button in your dashboards plugin page. Just like every other plugin.
Once activated, a new menu called "AdRotate" appears in the WordPress navigation from which you'll handle everything in AdRotate.

For more detailed instructions check out the [installation steps](https://ajdg.solutions/support/adrotate-manuals/installing-adrotate-on-your-website/?pk_campaign=adrotatefree&pk_keyword=readme) on the AdRotate website.

== Changelog ==

For the full changelog check out the [development page](https://ajdg.solutions/support/adrotate-development/?pk_campaign=adrotatefree&pk_keyword=readme).

= AdRotate 5.8.19 =
* [i18n] Improved Italian translation (Thanks Davide)
* [i18n] Improved Dutch translation
* [i18n] All translations updated using auto-translate
* [change] More consistent use of the word advert vs ad
* [change] Simplified settings text in General Settings
* [change] Cleaner dashboard when creating adverts
* [fix] Tabindex on dashboards now in the right order

= AdRotate 5.8.18 =
* [i18n] Added Portuguese translation (pt_BR)

= AdRotate 5.8.17 =
* [fix] Labels for all checkboxes
* [update] HTML dashboard code cleanup

= AdRotate Professional 5.8.13 =
* [fix] Disabled AMP group option
* [new] Assets now list generated reports
* [new] Post Injection up to 20 paragraphs
* [update] Asset and media dashboard
* [update] Reworded error when deleting a file fails
* [change] Silently fail when deleting a non-existing file
* [change] Removed option to disable dynamic mode on mobile
* [i18n] Improved Italian translation (Thanks Davide)

= AdRotate Professional 5.8.12 =
* [fix] Labels for all checkboxes
* [new] Labels for all radio buttons
* [update] HTML dashboard code cleanup

Be a Pro and get [AdRotate Professional](https://ajdg.solutions/product-category/adrotate-pro/?pk_campaign=adrotatefree&pk_keyword=readme)!

== Upgrade Notice ==

Enjoy this update with the latest tweaks and improvements for AdRotate for WordPress!

== Frequently Asked Questions ==

= How do I use AdRotate? =
Take a look at the [user guides](https://ajdg.solutions/support/adrotate-manuals/?pk_campaign=adrotatefree&pk_keyword=readme).
You can also post your questions on the [forum](https://ajdg.solutions/forums/forum/adrotate-for-wordpress/?pk_campaign=adrotatefree&pk_keyword=readme).

= I need help with this plugin =
The best place to ask your question is on my [support forum](https://ajdg.solutions/forums/forum/adrotate-for-wordpress/?pk_campaign=adrotatefree&pk_keyword=readme) or you can send a message through [Telegram](https://t.me/arnandegans).

= Does AdRotate offer click tracking? =
Yes, AdRotate counts clicks and impressions.

= Can I use my adverts from Google AdSense? =
Yes, usually you can use their code as-is.
Most adverts work without special tricks or tweaks.

= Does AdRotate support HTML5 adverts? =
Yes!

= This is cool, do you have more plugins? =
Yep, check out my website [AJdG Solutions](https://ajdg.solutions/?pk_campaign=adrotatefree&pk_keyword=readme)

= Why do some dashboard notifications look so ugly =
If a dashboard notification misses its layout or looks out of shape try clearing your browser cache.
Some ad blockers block parts of the AdRotate dashboard, check out this page to make an exception for your website in adblockers - [Whitelist your site](https://ajdg.solutions/support/adrotate-manuals/configure-adblockers-for-your-own-website/?pk_campaign=adrotatefree&pk_keyword=readme).

= Is AdRotate compatible with Yoast SEO or other SEO plugins? =
Yes, Yoast SEO, All-in-One SEO pack and all other SEO plugins work fine with AdRotate.

= Is Jetpack compatible with AdRotate? =
Yes.

= Does AdRotate work alongside caching plugins? =
AdRotate works best with Borlabs Cache and W3 Total Cache.
Personally I prefer Borlabs Cache.
Other plugins such as WP Super Cache, WP Rocket or WP Fastest Cache may work, but are untested.

= Does AdRotate work with WooCommerce? =
Yes!

= Does AdRotate work with bbPress? =
Yes!

== Screenshots ==

1. The AdRotate menu
2. Managing adverts and general overview in AdRotate
3. Error checking for adverts in AdRotate
4. Easily create and edit your advert campaigns with AdRotate
5. Schedule you advert to the minute with AdRotate
6. Manage groups, which can act as locations or slots on your website via AdRotate
7. Create groups to easily place multiple adverts in one spot on your site with AdRotate
8. Customize your group and select adverts from the group settings with AdRotate
9. Set up groups to do exactly what you want with AdRotate