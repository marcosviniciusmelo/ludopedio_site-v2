<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
?>
<main class="biblioteca-page">

	<section class="bannerTop">
		<div class="main">
			<div class="title">
				<h3 class="subtitle">Biblioteca</h3>
			</div>
		</div>
		<img src="<?php echo get_field('banner_biblioteca_page', 'options')['url'] ?>" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/biblioteca">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>

					<div class="linkDropdown">
						<span>Categoria <i class="fas fa-bars"></i></span>
						<div class="dropdown">
						<?php
							$terms = get_terms( 'category_biblioteca', array( 'hide_empty' => false, 'parent' => 0 ) );
						    foreach( $terms as $parent_term ):
						        $term_children = get_term_children($parent_term->term_id, 'category_biblioteca');

						        $class = '';
						        if( $term_children )
						        	$class = 'menu-item-has-children';
						        	echo '<li class="menu-item '.$class.'">
						        			<a href="'.get_term_link($parent_term->slug, 'category_biblioteca').'">'.$parent_term->name.'</a>';

								        if( $term_children ){
								        	echo '<ul class="sub-menu">';
								        	foreach ($term_children as $key) {
								        		$t = get_term_by('id', $key, 'category_biblioteca');
								        		echo '<li><a href="'.get_term_link($t->slug, 'category_biblioteca').'">'.$t->name.'</a></li>';
								        	}
								        	echo '</ul>';
								        }

						       		echo '</li>';

						    endforeach;
						?>
						</div>
					</div>

				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="bloco-first">
		<div class="main">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'large'); ?>
			<figure><img src="<?php echo $urlThumb ?>"></figure>
			<div class="right">
				<?php if( get_field('qd_biblioteca_article_isbn') ): ?>
					<span>ISBN <?php the_field('qd_biblioteca_article_isbn') ?></span>
				<?php endif; ?>
				<?php if( get_field('qd_biblioteca_article_issn') ): ?>
					<span>ISSN <?php the_field('qd_biblioteca_article_issn') ?></span>
				<?php endif; ?>
				<?php
					if( get_field('qd_biblioteca_article_tema') )
						echo '<h5>'.get_field('qd_biblioteca_article_tema').'</h5>';
				?>
				<h2><?php the_title() ?></h2>
				<?php
					if( get_field('qd_biblioteca_article_subtitulo') )
						echo '<h5>'.get_field('qd_biblioteca_article_subtitulo').'</h5>';
				?>
				<div class="ctnFields">
				<?php
					echo '<div class="item"><strong>Autor</strong><p>'.ludo_the_author_link($post->ID, false, 'autores').'</p></div>';
					if( get_field('qd_biblioteca_article_periodico') )
						echo '<div class="item"><strong>Periódico / Revista</strong><p><a href="'.$link_blog.'?s='.get_field('qd_biblioteca_article_periodico').'">'.get_field('qd_biblioteca_article_periodico').'</a></p></div>';
					
					if( get_field('qd_article_number') )
						echo '<div class="item"><strong>Número</strong><p>'.get_field('qd_article_number').'</p></div>';
					
					if( get_field('qd_biblioteca_article_numero') )
						echo '<div class="item"><strong>Número</strong><p>'.get_field('qd_biblioteca_article_numero').'</p></div>';

					if( get_field('qd_biblioteca_article_titulo_original') )
						echo '<div class="item"><strong>Título original / alternativo</strong><p>'.get_field('qd_biblioteca_article_titulo_original').'</p></div>';
					
					if( get_field('qd_biblioteca_article_mes') )
						echo '<div class="item"><strong>Dia e Mês</strong><p>'.get_field('qd_biblioteca_article_mes').'</p></div>';

					if( get_field('qd_biblioteca_article_data') ){
						if( strripos(get_field('qd_biblioteca_article_data'), '-') === false ){
							echo '<div class="item"><strong>Ano</strong><p>'.get_field('qd_biblioteca_article_data').'</p></div>';
						}else{
							$data = explode('-', get_field('qd_biblioteca_article_data'));
							echo '<div class="item"><strong>Ano</strong><p>'.$data[2].'/'.$data[1].'/'.$data[0].'</p></div>';
						}
					}

					if( get_field('qd_biblioteca_article_ano') )
						echo '<div class="item"><strong>Ano</strong><p>'.get_field('qd_biblioteca_article_ano').'</p></div>';

					if( get_field('qd_biblioteca_article_volume') )
						echo '<div class="item"><strong>Volume</strong><p>'.get_field('qd_biblioteca_article_volume').'</p></div>';

					if( get_field('qd_biblioteca_article_faculdade') )
						echo '<div class="item"><strong>Faculdade/Universidade</strong><p>'.get_field('qd_biblioteca_article_faculdade').'</p></div>';
					if( get_field('qd_biblioteca_article_orientador') )
						echo '<div class="item"><strong>Orientador(a)</strong><p><a href="'.$link_blog.'?s='.get_field('qd_biblioteca_article_orientador').'">'.get_field('qd_biblioteca_article_orientador').'</a></p></div>';

					if( get_field('qd_biblioteca_article_co_orientador') )
						echo '<div class="item"><strong>Co-rientador</strong><p>'.get_field('qd_biblioteca_article_co_orientador').'</p></div>';
					
					if( get_field('qd_biblioteca_article_banca') ){
						$bancaArr = explode(',', get_field('qd_biblioteca_article_banca'));
						$bancaFinal = array();
						foreach ($bancaArr as $key) {
							array_push($bancaFinal, '<a href="'.$link_blog.'?s='.trim($key).'">'.trim($key).'</a>');
						}
						echo '<div class="item"><strong>Banca</strong><p>'.implode(', ', $bancaFinal).'</p></div>';
					}
					
					if( get_field('qd_biblioteca_article_tema') )
						echo '<div class="item"><strong>Tema</strong><p>'.get_field('qd_biblioteca_article_tema').'</p></div>';
					if( get_field('qd_biblioteca_article_area_concentracao') )
						echo '<div class="item"><strong>Área de concentração</strong><p>'.get_field('qd_biblioteca_article_area_concentracao').'</p></div>';

					if( get_field('qd_biblioteca_article_congresso') )
						echo '<div class="item"><strong>Nome do congresso</strong><p>'.get_field('qd_biblioteca_article_congresso').'</p></div>';

					if( get_field('qd_biblioteca_article_congresso_edicao') )
						echo '<div class="item"><strong>Edição do Congresso</strong><p>'.get_field('qd_biblioteca_article_congresso_edicao').'</p></div>';

					if( get_field('qd_biblioteca_article_local') )
						echo '<div class="item"><strong>Cidade</strong><p>'.get_field('qd_biblioteca_article_local').'</p></div>';

					// if( get_field('qd_biblioteca_article_cidade') )
					// 	echo '<div class="item"><strong>Cidade</strong><p>'.get_field('qd_biblioteca_article_cidade').'</p></div>';

					if( get_field('qd_biblioteca_article_titulo_original') )
						echo '<div class="item"><strong>Título em inglês</strong><p>'.get_field('qd_biblioteca_article_titulo_original').'</p></div>';
					if( get_field('qd_biblioteca_article_format_filme') )
						echo '<div class="item"><strong>Formato do vídeo / especificação</strong><p>'.get_field('qd_biblioteca_article_format_filme')[0].'</p></div>';
					if( get_field('qd_biblioteca_article_cat_filme1') )
						echo '<div class="item"><strong>Categoria do filme</strong><p>'.get_field('qd_biblioteca_article_cat_filme1')[0].'</p></div>';
					if( get_field('qd_biblioteca_article_genero') )
						echo '<div class="item"><strong>Gênero</strong><p>'.get_field('qd_biblioteca_article_genero')[0].'</p></div>';
					if( get_field('qd_biblioteca_article_duracao') )
						echo '<div class="item"><strong>Duração</strong><p>'.get_field('qd_biblioteca_article_duracao').'</p></div>';
					if( get_field('qd_biblioteca_article_produtora') )
						echo '<div class="item"><strong>Produtora</strong><p>'.get_field('qd_biblioteca_article_produtora').'</p></div>';
					
					if( get_field('qd_biblioteca_article_pais') )
						echo '<div class="item"><strong>País</strong><p>'.get_field('qd_biblioteca_article_pais').'</p></div>';
					if( get_field('qd_biblioteca_article_paginas') )
						echo '<div class="item"><strong>Páginas</strong><p>'.get_field('qd_biblioteca_article_paginas').'</p></div>';
					
					if( get_field('qd_biblioteca_article_editora') )
						echo '<div class="item"><strong>Editora</strong><p>'.get_field('qd_biblioteca_article_editora').'</p></div>';

					if( get_field('qd_biblioteca_article_local_publicacao') )
						echo '<div class="item"><strong>Local de publicação</strong><p>'.get_field('qd_biblioteca_article_local_publicacao').'</p></div>';

					if( get_field('qd_biblioteca_article_evento_nome') )
						echo '<div class="item"><strong>Entidade Organizadora</strong><p>'.get_field('qd_biblioteca_article_evento_nome').'</p></div>';

					if( get_field('qd_biblioteca_article_edicoes') )
						echo '<div class="item"><strong>Edições</strong><p>'.get_field('qd_biblioteca_article_edicoes').'</p></div>';

					

					?>
				</div>
				<div class="links">
					<?php
						if( get_field('qd_biblioteca_buy_places_new') ):
							foreach (get_field('qd_biblioteca_buy_places_new') as $key):
								echo '<div class="col">
										<img src="'.str_replace('v2/content/', 'wp-content/', $key['store_image']).'">
										<a href="'.$key['store_url'].'" target="_blank" class="btn">Comprar agora</a>
									</div>';
							endforeach;
						endif;

						if( get_field('qd_biblioteca_buy_places') && !get_field('qd_biblioteca_buy_places_new') ):
							foreach (get_field('qd_biblioteca_buy_places') as $key):
								echo '<div class="col">
										<img src="'.str_replace('v2/content/', 'wp-content/', $key['store_image']).'">
										<a href="'.$key['store_url'].'" target="_blank" class="btn">Comprar agora</a>
									</div>';
							endforeach;
						endif;
					?>
				</div>
			</div>
		</div>
	</section>

	<section class="bloco-second">
		<div class="main">

			<?php
				ludo_post_creative_commons_icons();
			?>
			
			<?php if( get_field('qd_biblioteca_file_new') || get_field('qd_biblioteca_file') || get_field('qd_biblioteca_file_url') || get_field('qd_biblioteca_file_url_new') ): ?>
			<div class="line">
				<h3 class="title">Arquivos</h3>
				<div class="links">
					<?php
						if( get_field('qd_biblioteca_file_new') ):
							foreach (get_field('qd_biblioteca_file_new') as $key):
								echo '<div class="link">
										<p>'.$key['titulo'].'</p>
										<a href="'.preparaLinks($key['arquivo']).'" class="btn" target="_blank">Download <i class="fas fa-download"></i></a>
									</div>';
							endforeach;
						endif;

						if( get_field('qd_biblioteca_file_url_new') ):
							foreach (get_field('qd_biblioteca_file_url_new') as $key):
								echo '<div class="link">
										<a href="'.preparaLinks($key['link']).'" class="btn" target="_blank">Download <i class="fas fa-download"></i></a>
									</div>';
							endforeach;
						endif;

						if( get_field('qd_biblioteca_file') ):
							foreach (get_field('qd_biblioteca_file') as $key):
								echo '<div class="link">
										<a href="'.preparaLinks($key).'" class="btn" target="_blank">Download <i class="fas fa-download"></i></a>
									</div>';
							endforeach;
						endif;

						if( get_field('qd_biblioteca_file_url') ):
							echo '<div class="link">
									<a href="'.preparaLinks(get_field('qd_biblioteca_file_url')).'" target="_blank" class="btn" target="_blank">Download <i class="fas fa-download"></i></a>
								</div>';
						endif;
					?>
				</div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_resumo_pt') ): ?>
			<div class="line">
				<h3 class="title">Resumo</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_resumo_pt') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_resumo_idioma') ): ?>
			<div class="line">
				<h3 class="title">Resumo (outro idioma)</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_resumo_idioma') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_resumo_outros') ): ?>
			<div class="line">
				<h3 class="title">Resumos (outros idiomas)</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_resumo_outros') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_elenco') ): ?>
			<div class="line">
				<h3 class="title">Elenco</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_elenco') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_sinopse') ): ?>
			<div class="line">
				<h3 class="title">Sinopse</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_sinopse') ?></div>
			</div>
			<?php endif; ?>


			<?php if( get_field('qd_biblioteca_article_resumo_en') ): ?>
			<div class="line">
				<h3 class="title">Abstract</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_resumo_en') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_livro_edicao') ): ?>
			<div class="line">
				<h3 class="title">Edição</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_livro_edicao') ?></div>
			</div>
			<?php endif; ?>


			<?php if( get_field('qd_biblioteca_article_livro_edicao') ): ?>
			<div class="line">
				<h3 class="title">Edição</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_livro_edicao') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_sumario') ): ?>
			<div class="line">
				<h3 class="title">Sumário</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_sumario') ?></div>
			</div>
			<?php endif; ?>

			<?php if( get_field('qd_biblioteca_article_observacoes') ): ?>
			<div class="line">
				<h3 class="title">Observações</h3>
				<div class="text"><?php echo get_field('qd_biblioteca_article_observacoes') ?></div>
			</div>
			<?php endif; ?>

			<?php the_content() ?>


			<?php
			/**
			*
			* Referencias
			*
			**/

			$authors = ludo_the_author_no_link( get_the_id(), false );
			$authors_array = explode( ',', $authors );
			$authors_total = count( $authors_array );
			$count = 1;
			$author_ref = '';

			// Autores
			foreach( $authors_array as $author ):

				$author_array_1 = explode( ' ', $author );

				$last_name = end($author_array_1);

				if( $last_name == 'Junior' || $last_name == 'Júnior' || $last_name == 'Neto' || $last_name == 'Filho' ):
					$array_total_indices = count( $author_array_1 );
					$last_indice = $array_total_indices - 1;
					unset( $author_array_1[$last_indice] );
					$array_total_indices = count( $author_array_1 );
					$last_indice = $array_total_indices - 1;
					$author_array_1[$last_indice] = $author_array_1[$last_indice] . ' ' . $last_name;
				endif;

				if( in_array(" de ", $author_array_1) ):
					$author_array_2 = explode( ' de ', $author );
					$total_words = count($author_array_2);
					$last_name = $author_array_2[ $total_words - 1 ];
					$first_name = $author_array_2[0] . 'de';
					// Autor
					$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
					if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
				else:
					if( in_array(" da ", $author_array_1) ):
						$author_array_2 = explode( ' da ', $author );
						$total_words = count($author_array_2);
						$last_name = $author_array_2[ $total_words - 1 ];
						$first_name = $author_array_2[0] . 'da';
						// Autor
						$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
						if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
					else:
						if( in_array(" dos ", $author_array_1) ):
							$author_array_2 = explode( ' dos ', $author );
							$total_words = count($author_array_2);
							$last_name = $author_array_2[ $total_words - 1 ];
							$first_name = $author_array_2[0] . 'dos';
							// Autor
							$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
							if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
						else:
							$total_words = count($author_array_1);
							$last_name = $author_array_1[ $total_words - 1 ];
							$first_name = null;
							for( $x=0; $x<($total_words - 1); $x++  ):
								$first_name .= $author_array_1[$x];
								if( $x < ( $total_words - 2 ) ) $first_name .= ' ';
							endfor;
							// Autor
							$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
							if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
						endif;
					endif;
				endif;
				$count++;
			endforeach;

			// Termo do post corrente
			$post_terms = wp_get_post_terms( get_the_ID(), 'category_biblioteca' );
			$post_term_slug = $post_terms [0]->slug;

			// Titulo
			$periodico = get_post_meta( get_the_ID(), 'qd_biblioteca_article_periodico', true );
			$title = get_the_title() . '. ';
			$title_no_html = get_the_title();

			// SubTítulo
			$subtitulo = get_post_meta( get_the_ID(), 'qd_biblioteca_article_subtitulo', true );

			if( !$periodico && $post_term_slug != 'filmes' && $post_term_slug != 'ludopedio-tv'   ) $title = '<strong>' . $title .'</strong>';

			// Periodico
			if( $periodico ) $periodico = '<strong>' . get_post_meta( get_the_ID(), 'qd_biblioteca_article_periodico', true ) . '</strong>.';

			// Local
			$local = get_post_meta( get_the_ID(), 'qd_biblioteca_article_local', true );
			if( $local ) $local .= ', ';

			// País
			$pais = get_post_meta( get_the_ID(), 'qd_biblioteca_article_pais', true );
			if( $pais ) $pais .= '. ';

			// Cidade
			$cidade = get_post_meta( get_the_ID(), 'qd_biblioteca_article_cidade', true );

			// Volume
			$volume = get_post_meta( get_the_ID(), 'qd_biblioteca_article_volume', true );
			if( $volume ) $volume .= ', ';

			// Número
			$numero = get_post_meta( get_the_ID(), 'qd_biblioteca_article_numero', true );
			if( $numero ) $numero .= ', ';

			// Páginas
			$paginas = get_post_meta( get_the_ID(), 'qd_biblioteca_article_paginas', true );
			if( $paginas ) $paginas .= ', ';

			// Data
			$data = get_post_meta( get_the_ID(), 'qd_biblioteca_article_data', true );

			// Ano
			$ano = get_post_meta( get_the_ID(), 'qd_biblioteca_article_ano', true );

			// Produtora
			$produtora = get_post_meta( get_the_ID(), 'qd_biblioteca_article_produtora', true );
			if( $produtora ) $produtora .= ', ';

			// Formato vídeo
			$formato_video = get_post_meta( get_the_ID(), 'qd_biblioteca_article_format_filme', true );
			//Vornei: 28/06/2019 - [error] 16703#0: *2814 FastCGI sent in stderr: "PHP message: PHP Warning:  count(): Parameter must be an array or an object that implements Countable in /usr/share/nginx/html/v2/content/themes/ludopedio/single-qd_biblioteca.php on line 416
			//$array_count = count( $formato_video );
			$array_count = (is_array($formato_video) ? count($formato_video) : 0);
			$count = 1;
			$formato_list = '';
			if( is_array($formato_video) ):
				foreach( $formato_video as $formato ):
					$formato_list .= $formato;
					if( $count< $array_count  ) $formato_list .= ', ';
					$count++;
				endforeach;
			else:
				$formato_list = str_replace('.', '', $formato_video);
			endif;

			// Editora
			$editora = get_post_meta( get_the_ID(), 'qd_biblioteca_article_editora', true );
			if( $editora ) $editora .= ', ';

			// Faculdade
			$faculdade = get_post_meta( get_the_ID(), 'qd_biblioteca_article_faculdade', true );
			if( $faculdade ) $faculdade .= ', ';

			// Tema
			$tema = get_post_meta( get_the_ID(), 'qd_biblioteca_article_tema', true );

			// Area de concentração
			$area_concentração = get_post_meta( get_the_ID(), 'qd_biblioteca_article_area_concentracao', true );

			// Data defesa
			$data_defesa = get_post_meta( get_the_ID(), 'qd_biblioteca_article_data_defesa', true );

			// Congresso
			$congresso = get_post_meta( get_the_ID(), 'qd_biblioteca_article_congresso', true );

			// Orgs. Organizadores
			$organizadores = get_post_meta( get_the_ID(), 'qd_biblioteca_article_livro_orgs', true );
			if( !$organizadores ): $organizadores = ''; else: $organizadores . '. '; endif;

			// Entidade Organizadora
			$entidade_organizadora = get_post_meta( get_the_ID(), 'qd_biblioteca_article_evento_nome', true );

			// Edição do Congresso
			$edicao_congresso = get_post_meta( get_the_ID(), 'qd_biblioteca_article_congresso_edicao', true );

			// Referencia
			// echo $post_term_slug;
			switch ( $post_term_slug ) {
				// Artigos em periodicos ( verificado )
				case 'artigos-em-periodicos':
					echo '<h4>Referência</h4>';
					if( $cidade ): $cidade = $cidade . ', '; else: $cidade = $local; endif;
					if( $ano ): $ano = $ano . '.'; else: $ano = $data . '.'; endif;
					$referencia = $author_ref . $title . $periodico . ' ' . $cidade . $volume . $numero . $paginas . $ano;
					break;
				// Filmes ( verificado )
				case 'filmes':
					echo '<h4>Referência</h4>';
					$referencia = strtoupper( $title ) . 'Direção: ' .  $authors . '. ' . $pais . $produtora . $ano . '. ' . $formato_list . '.';
					break;
				// Ludopedio TV ( verificado )
				case 'ludopedio-tv':
					echo '<h4>Referência</h4>';
					$referencia = strtoupper( $title ) . 'Direção: ' .  $authors . '. ' . $pais . $produtora . $ano . '. ' . $formato_list . '.';
					break;
				// Livros ( verificado  )
				case 'livros':
					echo '<h4>Referência</h4>';
					if( $cidade ) $cidade = $cidade . ': ';
                    //Vornei: 23/08/2020 - Problemas em pular linha desnecessaria
                    $organizadores = str_replace('<p>', '', $organizadores);
                    $organizadores = str_replace('</p>', '', $organizadores);

					if( $subtitulo ):
						$referencia = $author_ref . $organizadores . str_replace('.', ': ', $title) . $subtitulo . '. ' . $cidade . $editora . $ano . '.';
					else:
						$referencia = $author_ref . $organizadores .' '. $title . $cidade . $editora . $ano . '.';
					endif;
					break;
				// Revistas ( verificado )
				case 'revistas':
					echo '<h4>Referência</h4>';
					if( $cidade ): $cidade = $cidade . ', '; else: $cidade = $local; endif;

					if( strripos($data, '-') == TRUE ){
						$data = explode('-', $data);
						$data = $data[2].'/'.$data[1].'/'.$data[0];
					}
					
					$referencia = $author_ref . $title . $periodico . ' ' . $cidade . $numero . $paginas . $data . '.';
					break;

				case 'revista-relvado':
					echo '<h4>Referência</h4>';
					if( $cidade ): $cidade = $cidade . ', '; else: $cidade = $local; endif;

					if( strripos($data, '-') == TRUE ){
						$data = explode('-', $data);
						$data = $data[2].'/'.$data[1].'/'.$data[0];
					}

					$referencia = $author_ref . $title . $periodico . ' ' . $cidade . $numero . $paginas . $data . '.';
					break;

				case 'revista-placar':
					echo '<h4>Referência</h4>';
					if( $cidade ): $cidade = $cidade . ', '; else: $cidade = $local; endif;

					if( strripos($data, '-') == TRUE ){
						$data = explode('-', $data);
						$data = $data[2].'/'.$data[1].'/'.$data[0];
					}

					$referencia = $author_ref . $title . $periodico . ' ' . $cidade . $numero . $paginas . $data . '.';
					break;

				// Teses e dissertações ( verificado )
				case 'teses-e-dissertacoes':
					echo '<h4>Referência</h4>';
					if( $ano ): $ano = $ano . '. '; else: $ano = $data . '. '; endif;
					if( $subtitulo ):
						$referencia = $author_ref . str_replace('.', ': ', $title) . $subtitulo . '. ' . $ano . str_replace(',', '', $paginas) . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					else:
						$referencia = $author_ref . $title . $ano . str_replace(',', '', $paginas)  . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					endif;
					break;

				case 'dissertacoes':
					echo '<h4>Referência</h4>';
					if( $ano ): $ano = $ano . '. '; else: $ano = $data . '. '; endif;
					if( $subtitulo ):
						$referencia = $author_ref . str_replace('.', ': ', $title) . $subtitulo . '. ' . $ano . str_replace(',', '', $paginas) . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					else:
						$referencia = $author_ref . $title . $ano . str_replace(',', '', $paginas)  . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					endif;
					break;

				case 'teses':
					echo '<h4>Referência</h4>';
					if( $ano ): $ano = $ano . '. '; else: $ano = $data . '. '; endif;
					if( $subtitulo ):
						$referencia = $author_ref . str_replace('.', ': ', $title) . $subtitulo . '. ' . $ano . str_replace(',', '', $paginas) . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					else:
						$referencia = $author_ref . $title . $ano . str_replace(',', '', $paginas)  . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					endif;
					break;

				case 'tcc':
					echo '<h4>Referência</h4>';
					if( $ano ): $ano = $ano . '. '; else: $ano = $data . '. '; endif;
					if( $subtitulo ):
						$referencia = $author_ref . str_replace('.', ': ', $title) . $subtitulo . '. ' . $ano . str_replace(',', '', $paginas) . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					else:
						$referencia = $author_ref . $title . $ano . str_replace(',', '', $paginas)  . ' f. ' . $tema . ' (' . $area_concentração . ') - ' . $faculdade . $cidade . ', ' . $ano;
					endif;
					break;

					
				// Trabalhos em eventos ( verificado )
				case 'trabalhos-em-eventos':
					echo '<h4>Referência</h4>';
					if( $entidade_organizadora ):
						$in = 'In: ' . $entidade_organizadora . ', ' . $edicao_congresso . ', ';
						$congresso = $congresso . ', ';
						$enditdade_pos_tema = ', ' . $entidade_organizadora . ', ';
					endif;
					if( $ano ) $ano = $ano . ', ';
					if( $local ) $local = trim(str_replace( ',', '', $local ));
					$referencia = $author_ref . $title_no_html . '. ' . $in . $ano . $local . '. ' . $congresso . $entidade_organizadora . '. <strong>' . $tema  . '</strong> ' . $enditdade_pos_tema . trim( str_replace( ',', '', $ano )) . '.';
					break;
			}

			echo $referencia;
	        ?>


			<?php
				if( get_field('qd_biblioteca_article_youtube') )
					echo '<div class="line">
							<div class="text"><iframe width="560" height="315" src="'.get_field('qd_biblioteca_article_youtube').'"  frameborder="0" allowfullscreen></iframe></div>
						</div>';

				if( get_field('qd_biblioteca_article_embed_code') )
					echo '<div class="line">
							<div class="text">'.get_field('qd_biblioteca_article_embed_code').'</div>
						</div>';

				$book_isbn = get_field('qd_biblioteca_article_isbn');
				if( $book_isbn  ):
					echo '<div class="line">';
				?>
						<script type="text/javascript" src="https://books.google.com/books/previewlib.js"></script>
				        <script>GBS_setLanguage('pt-BR');</script>
				        <script>GBS_insertEmbeddedViewer('ISBN:<?php echo $book_isbn; ?>',600,500);</script>
					<?php
					echo '</div>';
				endif;
			?>

		</div>
	</section>

	<section class="listBilioteca grey">
		<div class="main">
			<h3 class="title">Leituras recomendadas</h3>
		<?php

			$cate = wp_get_object_terms(get_the_ID(), 'category_biblioteca');

			echo '<ul class="gridBiblioteca">';
				$args = array(
					'post_type' 	  => 'qd_biblioteca',
					'post_status' 	  => 'publish',
					'posts_per_page'  => 4,
					'post__not_in'   => array(get_the_ID()),
					'tax_query' => array(
				        array(
				            'taxonomy' => $cate[0]->taxonomy,
				            'field'    => 'slug',
				            'terms'    => $cate[0]->slug,
				        )
				    )
				);

				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
					$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
	
					echo '<li class="_item">
							<a href="'.get_the_permalink().'">
								<figure><img src="'.$urlThumb.'"></figure>
								<h4>'.get_the_title().'</h4>
								<span>'.ludo_the_author_no_link( $post->ID, false ).'</span>
							</a>
						</li>';
				endwhile;
				wp_reset_postdata();

			echo '</ul>';
		?>
		</div>
	</section>

</main>

<?php
get_footer();