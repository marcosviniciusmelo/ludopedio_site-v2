<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

$tipos = array('post', 'qd_arquibancada', 'qd_gallery_museu', 'qd_gallery', 'qep_type_event', 'qd_biblioteca');
$arrPost = '';
?>

<main class="home-page">
	
	<section class="sliderCtn">
		<ul class="slider">
			<?php
				$sliders = get_field('sliders', 'options');
				foreach ($sliders as $key):

					if( $key['tipo_do_slider'] == 'Customizado' ):

						$background = $key['background'];
						echo '<li class="itemSlider">
								
								<div class="main">
									<div class="left">
										<a href="'.$key['link'].'"><span>'.$key['categoria'].'</span></a>
										<a href="'.$key['link'].'"><h3>'.$key['titulo'].'</h3></a>
										<div class="descricao">'.$key['subtitulo'].'</div>
										<a href="'.$key['link'].'" class="btn">Leia</a>
									</div>
								</div>
								<a href="'.$key['link'].'" class="linkBg"></a>
								<img src="'.$background['sizes']['large'].'" alt="'.$background['title'].'" class="bg">
							</li>';


							$slugP = explode('/', $key['link']);
							$slugF = $slugP[count( $slugP ) - 1];
							if( $slugF == '' ):
								$slugF = $slugP[count( $slugP ) - 2];
							endif;

							$IDCurrent = get_page_by_path( $slugF, OBJECT, 'post' );

							if( $IDCurrent ):
								$arrPost .= ','.$IDCurrent->ID;
							endif;


					elseif( $key['tipo_do_slider'] == 'Evento' ):

						$post_ID = $key['evento'];
						$arrPost .= ','.$post_ID;
						$background = get_the_post_thumbnail_url($post_ID, 'large');
						echo '<li class="itemSlider">
								<div class="main">
									<div class="left">
										<div class="tags">'.getCategoryPost($post_ID, 'a').'</div>
										<a href="'.get_the_permalink($post_ID).'"><h3>'.get_the_title($post_ID).'</h3></a>
										<div class="descricao">'.get_field('subtitulo').'</div>
										<a href="'.get_the_permalink($post_ID).'" class="btn">Leia</a>
									</div>
								</div>
								<a href="'.get_the_permalink($post_ID).'" class="linkBg"></a>
								<img src="'.$background.'" alt="">
							</li>';

					else:

						switch ( $key['tipo_do_post'] ) {
					        case 'Arquibancada':
					            $post = 'qd_arquibancada';
					            $category = 'category_arquibancada';
					            $categoryPost = 'categoria_do_post_arquibancada';
					            break;

					        case 'Entrevista':
					            $post = 'qd_entrevista';
					            $category = 'category_entrevista';
					            $categoryPost = 'categoria_do_post_entrevista';
					            break;

					        case 'Museu':
					        	$post = 'qd_gallery_museu';
					            $category = 'category_gallery_museu';
					            $categoryPost = 'categoria_do_post_museu';
					            break;

					        case 'Futebol-Arte':
					        	$post = 'qd_gallery';
					            $category = 'category_gallery';
					            $categoryPost = 'categoria_do_post_arte';
					            break;

					        case 'Linha do Tempo':
					        	$post = 'qd_memoria';
					            $category = 'category_memoria';
					            $categoryPost = 'categoria_do_post_tempo';
					            break;

					        case 'Biblioteca':
					        	$post = 'qd_biblioteca';
					            $category = 'category_biblioteca';
					            $categoryPost = 'categoria_do_post_biblioteca';
					            break;

					        default:
					        	$post = 'post';
					            $category = 'category';
					            $categoryPost = 'categoria_do_post';
					            break;
						}

						$args = array( 'post_type' => $post, 'post_status' => 'publish', 'posts_per_page' => 1);

						$args['tax_query'] = array(
						    array(
						        'taxonomy'  => $category,
						        'field'     => 'id',
						        'terms'     => $key[$categoryPost],
						        // 'include_children' => false
						    )
						);


						$query = new WP_Query($args);
						while ($query->have_posts()) : $query->the_post();
							$arrPost .= ','.$post->ID;
							$background = get_the_post_thumbnail_url($post->ID, 'large');

							$label = 'Leia';

							if( $key['tipo_do_post'] == 'Ludosfera' ):
								if ( in_category('video') ):
									$label = 'Veja';
								else:
									$label = 'Ouça';
								endif;

							elseif( $key['tipo_do_post'] == 'Museu' || $key['tipo_do_post'] == 'Futebol-Arte' ):
								$label = 'Veja';
							endif;

							echo '<li class="itemSlider">
									<div class="main">
										<div class="left">
											<div class="tags">'.getCategoryPost($post->ID, 'a').'</div>
											<a href="'.get_the_permalink().'"><h3>'.get_the_title().'</h3></a>';
											echo '<p class="autor">Por ';
											ludo_the_author_no_link( $post->ID );
											echo '</p>
											<div class="descricao">'.get_field('subtitulo').'</div>
											<a href="'.get_the_permalink().'" class="btn">'.$label.'</a>
										</div>
									</div>
									<a href="'.get_the_permalink().'" class="linkBg"></a>
									<img src="'.$background.'" alt="">
								</li>';
						endwhile;

					endif;

				endforeach;
			?>
		</ul>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="bloco-posts first">
		<div class="main">
			<div class="title">
				<h3>Arquibancada</h3>
				<a href="<?php echo $link_blog ?>/arquibancada" class="btn-ver">Ver todas ></a>
			</div>

			<div class="posts col-crazy">
				<?php
					$args = array(
						'post_type'         => 'qd_arquibancada',
						'post_status'       => 'publish',
						'posts_per_page'    => 6,
						'post__not_in' => explode(',', $arrPost)
					);
					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						$arrPost .= ','.$post->ID;
						include(get_stylesheet_directory() . '/inc/post/post-style01.php');
					endwhile;
				?>
			</div>
		</div>

		<div class="circleYellow"></div>

	</section>


	<section class="bloco-posts second">
		<div class="main">
			<div class="title">
				<h3>Ludosfera</h3>
				<div class="right">
					<a href="<?php echo $link_blog ?>/categoria/podcast/" class="btn-ver">Ver todas</a>
					<div class="arrows">
						<div class="arrow prevNav primeiro"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav primeiro"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="posts slider-2" data-name="primeiro">
				<?php
					$args = array(
						'post_type'         => 'post',
						'post_status'       => 'publish',
						'posts_per_page'    => 6,
						'category_name'		=> 'podcast, video',
						'tax_query' => array(
					        array(
					            'taxonomy' => 'post_tag',
					            'field'    => 'slug',
					            'terms'    => array('poroutrofutebol'),
					            'operator'  => 'NOT IN'
					        )
					    ),
						'post__not_in' => explode(',', $arrPost)
					);
					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						$arrPost .= ','.$post->ID;
						include(get_stylesheet_directory() . '/inc/post/post-style02.php');
					endwhile;
				?>
			</div>
		</div>	
	</section>


	<section class="bloco-posts third">
		<div class="main">
			<div class="title">
				<h3>#Poroutrofutebol</h3>
				<div class="right">
					<a href="<?php echo $link_blog ?>/categoria/podcast/" class="btn-ver">Ver todas</a>
					<div class="arrows">
						<div class="arrow prevNav segundo"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav segundo"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="posts slider-4" data-name="segundo">
				<?php
					$args = array(
						'post_type'         => 'post',
						'post_status'       => 'publish',
						'posts_per_page'    => 8,
						'tax_query' => array(
					        array(
					            'taxonomy' => 'post_tag',
					            'field'    => 'slug',
					            'terms'    => 'poroutrofutebol'
					        )
					    ),
						'post__not_in' => explode(',', $arrPost)
					);
					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						$arrPost .= ','.$post->ID;
						include(get_stylesheet_directory() . '/inc/post/post-style01.php');
					endwhile;
				?>
			</div>
		</div>
		<div class="circleYellow"></div>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>


	<section class="bloco-posts loja">
		<div class="main">
			<div class="title">
				<h3>Loja</h3>
				<a href="<?php echo $link_blog ?>/loja" class="btn-ver">Conheça a loja  ></a>
			</div>

			<?php echo do_shortcode('[recent_products limit=4 category="apoie, cursos" cat_operator="NOT IN"]') ?>
		</div>	
	</section>


	<section class="bloco-posts">
		<div class="main">
			<div class="title">
				<h3>Em Campo</h3>
				<div class="right">
					<a href="<?php echo $link_blog ?>/categoria/ludopedio-em-campo/" class="btn-ver">Ver todas</a>
					<div class="arrows">
						<div class="arrow prevNav grid-3-crazy"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav grid-3-crazy"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="slider-1" data-name="grid-3-crazy">
				<div class="posts col-3-crazy">
					<?php
						$args = array(
							'post_type'         => 'post',
							'post_status'       => 'publish',
							'posts_per_page'    => 6,
							'category_name'		=> 'ludopedio-em-campo',
							'post__not_in' => explode(',', $arrPost)
						);
						$i=1;
						$query = new WP_Query($args);
					?>
					<?php
						while ($query->have_posts()) : $query->the_post();
							$arrPost .= ','.$post->ID;
							include(get_stylesheet_directory() . '/inc/post/post-style01.php');

							if( $i%3 == 0 && $i < $query->post_count )
								echo '</div><div class="posts col-3-crazy">';

						$i++;
						endwhile;
					?>
				</div>
			</div>
		</div>	
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-04.php'); ?>

	<!-- <section class="bloco-posts grey">
		<div class="main">
			<div class="title">
				<h3>Colunistas</h3>
				<div class="right">
					<a href="#" class="btn-ver">Ver todas</a>
					<div class="arrows">
						<div class="arrow prevNav quarto"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav quarto"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="posts slider-4" data-name="quarto">
				<?php
					foreach (get_field('colunistas', 'options') as $key):

						$user_info = get_userdata($key['membro']);
						$nome = $user_info->display_name;
						$userInsta = get_field( 'instagram', 'user_'.$key['membro']['ID'] );
						$userFace = get_field( 'facebook', 'user_'.$key['membro']['ID'] );
						$userTwitter = get_field( 'twitter', 'user_'.$key['membro']['ID'] );
				
						?>
						<li class="_itemGrid style-03">
							<div class="ctn">
								<div class="bottom">
									<a href=""><h3 class="title-c"><?php echo $key['membro']['display_name'] ?></h3></a>
									<div class="redes">
										<?php if( $userTwitter != '' ): ?>
										<a href="https://twitter.com/<?php echo $userTwitter; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
										<?php endif; ?>
										<?php if( $userInsta != '' ): ?>
										<a href="https://www.instagram.com/<?php echo $userInsta; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
										<?php endif; ?>
										<?php if( $userFace != '' ): ?>
										<a href="https://www.facebook.com/<?php echo $userFace; ?>" target="_blank"><i class="fab fa-facebook"></i></a>
										<?php endif; ?>
									</div>
								</div>
								<figure class="bg">
									<?php echo $key['membro']['user_avatar'] ?>
								</figure>
							</div>
						</li>
						<?php
					endforeach;
				?>
			</div>
		</div>	
	</section> -->


	<section class="bloco-posts grid-6-posts">
		<div class="main">
			<div class="title">
				<h3>Outros destaques</h3>
			</div>
			<div class="ctn-down">
				<div class="left">
					<?php
						$args = array(
							'post_type'         => $tipos,
							'post_status'       => 'publish',
							'posts_per_page'    => 6,
							'post__not_in' => explode(',', $arrPost)
						);
						$query = new WP_Query($args);
					?>
					<?php
						while ($query->have_posts()) : $query->the_post();
							$arrPost .= ','.$post->ID;
							include(get_stylesheet_directory() . '/inc/post/post-style04.php');
						endwhile;
					?>
				</div>
				<div class="ads-right">
					<?php echo do_shortcode('[adrotate group="11"]') ?>
				</div>
			</div>
		</div>	
	</section>


	<section class="bloco-posts acervo">
		<div class="main">
			<div class="title">
				<h3><img src="<?php echo $url_tema; ?>assets/images/icones/acervo.png" alt="">Acervo</h3>

				<div class="center">
					<?php echo get_field('acervo', 'options')['titulo_acervo'] ?>
				</div>

				<div class="right">
					<div class="arrows">
						<div class="arrow prevNav quinto"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav quinto"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="posts slider-3" data-name="quinto">
				<?php
					
					$args = array(
						'post_type'         => array('qd_arquibancada', 'post', 'qd_entrevista', 'qd_gallery_museu', 'qd_gallery', 'qd_memoria', 'qd_biblioteca'),
						'post_status'       => 'publish',
						'posts_per_page'    => 6,
						'post__not_in' => explode(',', $arrPost)
					);

					$opAcervo = get_field('acervo', 'options');
					if( $opAcervo['tipo_do_filtro'] == 'Por data' ){
						$args['date_query'] = array(
						    'column' => 'post_date',
						    'after' =>  $opAcervo['data_inicio'],
							'before' => $opAcervo['data_fim'] 
						);
					}else{
						$tags = explode(',', $opAcervo['tag']);
						$args['tax_query'] = array(
					        array(
					            'taxonomy' => 'post_tag',
					            'field'    => 'name',
					            'terms'    => $tags,
					            'operator' => 'IN',
					        )
					    );
					}

					$query = new WP_Query($args);

					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style05.php');
					endwhile;
				?>
			</div>
		</div>
		<div class="circleYellow"></div>
	</section>

</main>	

<?php
get_footer();