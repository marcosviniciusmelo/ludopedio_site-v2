<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$cate = get_queried_object();

$paged = 1;
if( isset($_GET['page']) )
	$paged = $_GET['page'];
$showposts = 10;
$offset = ($paged - 1) * $showposts;

?>
<main class="ludosfera-page">

	<section class="sliderCategory">
		<div class="slider">
			<?php
				$args = array(
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'category_name'  => array($cate->slug, 'video'),
				'posts_per_page' => 4,
			);
			$query = new WP_Query($args);

			while ($query->have_posts()) : $query->the_post();

				include(get_stylesheet_directory() . '/inc/post/post-style06.php');

			endwhile;
			wp_reset_postdata();
			?>
		</div>
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog . '/categoria/' . $cate->slug ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
					<div class="linkDropdown">
						<span>Filtrar por <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$terms = get_terms([
							    'taxonomy' => 'category',
							    'hide_empty' => false,
							]);
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug, 'category').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<?php if( !isset($_GET['busca']) && !isset($_GET['ordem']) ): ?>

	<section class="bloco-posts">
		<div class="main">
			<div class="title">
				<h3>Podcasts</h3>
				<a href="<?php echo get_permalink( get_page_by_path( 'podcasts' ) ); ?>" class="btn-ver">Ver todas ></a>
			</div>

			<div class="posts col-2">
				<?php

					$not_id = get_term_by('slug', 'video', 'category' );

					$args = array(
						'post_type'      => 'post',
						'post_status'    => 'publish',
						'posts_per_page' => 4,
						'offset'		 => 4,
						'category_name'  => $cate->slug,
						'category__not_in'   => array($not_id->term_id)
					);
					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style02.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>
	</section>

	<section class="bloco-posts">
		<div class="main">
			<div class="title">
				<h3>Vídeos</h3>
				<a href="<?php echo get_term_link('video', 'category') ?>" class="btn-ver">Ver todas ></a>
			</div>

			<div class="posts col-2">
				<?php
					$args = array(
						'post_type'         => 'post',
						'post_status'       => 'publish',
						'posts_per_page'    => 4,
						'category_name'  	=> 'video',
					);
					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style02.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>
	</section>

	<?php else: ?>

		<section class="bloco-posts">
			<div class="main">

				<div class="posts col-2">
					<?php
						$args = array(
							'post_type'      => 'post',
							'post_status'    => 'publish',
							'posts_per_page' => $showposts,
							'offset'		 => $offset,
							'category_name'  => $cate->slug,
						);

						if( isset($_GET['busca']) ){
							$args['s'] = $_GET['busca'];
						}

						if( isset($_GET['ordem']) ){
							if ( $_GET['ordem'] == 'cadastro' ) {
								$args['orderby'] = 'date';
								$args['order'] = 'DESC';
							}else{
								$args['orderby'] = 'title';
								$args['order'] = 'ASC';
							}
						}
						$query = new WP_Query($args);
					?>
					<?php
						while ($query->have_posts()) : $query->the_post();
							include(get_stylesheet_directory() . '/inc/post/post-style02.php');
						endwhile;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</section>

		<?php include( get_stylesheet_directory() . '/inc/pagination.php'); ?>

	<?php endif; ?>


	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();