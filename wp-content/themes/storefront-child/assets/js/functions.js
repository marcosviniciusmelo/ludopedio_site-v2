var $ = jQuery;

// SLICK //
$(".home-page .sliderCtn .slider").each(function(){

  $(".home-page .sliderCtn .slider").on('init', function(event, slick){
    $(this).addClass('show');
  });

  $(this).slick({
    dots: true,
    infinite: true,
    speed: 1000,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplaySpeed: 4000
  });
})




$(".slider-1").each(function(){
  var name = $(this).data('name')
  $(this).slick({
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.prevNav.'+name),
    nextArrow: $('.nextNav.'+name),
  });
});

$(".posts.slider-2").each(function(){
  var name = $(this).data('name')
  $(this).slick({
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: false,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow: $('.prevNav.'+name),
    nextArrow: $('.nextNav.'+name),
    responsive: [
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});

$(".posts.slider-4").each(function(){
  var name = $(this).data('name')
  $(this).slick({
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: $('.prevNav.'+name),
    nextArrow: $('.nextNav.'+name),
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 760,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });
});

$(".posts.slider-3").each(function(){
  var name = $(this).data('name')
  $(this).slick({
    dots: false,
    infinite: true,
    speed: 1000,
    autoplay: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: $('.prevNav.'+name),
    nextArrow: $('.nextNav.'+name),
    responsive: [
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});


$(".sliderCategory .slider").slick({
  dots: true,
  arrows: false,
  infinite: true,
  speed: 1000,
  autoplay: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
});


$(".quem-somos-page .bloco-fourth .slider").slick({
  dots: false,
  arrows: true,
  infinite: true,
  speed: 1000,
  autoplay: true,
  slidesToShow: 6,
  slidesToScroll: 6,
  responsive: [
    {
      breakpoint: 1110,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 400,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});



$(window).on("resize load", function(){
  setTimeout(function(){
    if( $('.home-page .sliderCtn .slider .slick-dots').length > 0 ){
      $('.home-page .sliderCtn .slider .slick-dots').css('right', $('.home-page .sliderCtn .slider .slick-current .main').offset().left + 5 ).addClass('active');
    }

    $(".sliderCategory").each(function(){
      $('.sliderCategory .slick-dots').css('right', $('.sliderCategory .main').offset().left + 5 );
    });
  }, 600);
});


updateNumberCart();

$( document.body ).on( 'added_to_cart', function(){
    console.log('update cart');
    updateNumberCart();
});


function updateNumberCart(){
  $('.linkCart .number').addClass('loading');
  $.ajax({
    type: "POST",
    url: url + "/wp-admin/admin-ajax.php",
    data: { action : 'updateNumberCart'},
    success: function(data) {
      $('.linkCart .number').text(data).removeClass('loading');

      if( data == 0 )
        if( $(document).width() <= 520 )
           $('.linkCart').attr('href', url+'/loja/')
    }
  });
}

/*
 * First word style
 */
 $(".getFirstWord").html(function(){
  var text= $(this).text().trim().split(" ");
  var first = text.shift();
  return (text.length > 0 ? "<span class='first'>"+ first + "</span> " : first) + text.join(" ");
});

 /*
  * Modal busca
  */
$('.clickBusca').click(function(){
  $('.overModalBusca, .modalBusca').fadeIn();
});

$('.modalBusca .topo .main .close, .overModalBusca').click(function(){
  $('.overModalBusca, .modalBusca').fadeOut();
});


$('.numberAnimated').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 3000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$('.storefront-sorting:first').show();


/*
* Click abas
*/
$('.abasPlanos ._item').click(function(){
  $('.abasPlanos ._item').removeClass('active');
  $(this).addClass('active');

  $('.conteudoPlano').removeClass('active');
  $('.conteudoPlano').eq( $(this).index() ).addClass('active');
});


/*
* Animated click #
*/
$(document).on('click', 'a[href^="#"]', function(e) {
  e.preventDefault();
  var id = $(this).attr('href');

  if( $('[name="'+id.replace('#', '')+'"]').length > 0 ){
    var pos = $('[name="'+id.replace('#', '')+'"]').offset().top;
  }else{
    // var pos = $(id).offset().top;
  }
  $('body, html').animate({scrollTop: pos - 120 }, 1000);
});


/*
* Modal Planos
*/
$('.conteudoPlano .toolTip').click(function(){

  let id = $(this).data('id');
  let mode = $(this).data('mode');
  
  $.ajax({
    type: "POST",
    url: url + "/wp-admin/admin-ajax.php",
    data: { id: id, mode: mode, action : 'contentPlano'},
    success: function(data) {
      $('.modalPlano .content').html(data);
      console.log(data);
    }
  });

  $('.modalPlano, .overflowPlano').fadeIn();
});

$('.modalPlano .close, .overflowPlano').click(function(){
  $('.modalPlano, .overflowPlano').fadeOut();
  $('.modalPlano .content').html('');
});

$('.barSearch form select').change(function(){
  $('.barSearch form').submit();
})

$('.powerpress_player').each(function(){
  let html = $(this).html();
  $(this).parents().find('.contentText').prepend('<div class="powerpress_player">'+ html +'</div>');
  $(this).remove();
});


$('img[loading="lazy"]').each(function(){
  $(this).attr('srcset', '');
});


$(function() {
  
  $(".fieldData").each(function(){

    var highlightDate = {};
    let arr = $(this).parent().find('[name="daySchedules"]').val().split(',')
    arr.forEach( function(value, indice) {
      highlightDate[ new Date(value)] = new Date(value);
    })

    $(this).datepicker({
        dateFormat: 'dd-mm-yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        beforeShowDay: function( date ) {
          var highlight = highlightDate[date];
          if( highlight ) {
              return [true, "highlight", 'Evento marcado'];
          } else {
              return [true, '', ''];
          }
        }
    });

  });
    
});

$('.fieldData').change(function(){
  $(this).parents('form').submit();
  console.log('asd');
});


if( $('.barFloatLeft').length > 0 ){
  $(window).on("scroll load", function(){
    if( $(document).width() > 500 ){
      let pos = $('.lineTopFixed').offset().top;
      let pos2 = $('.barFloatLeft').offset().top;
      let pos3 = $('.newsletter').offset().top;

      if( pos > pos2 ){
        if( pos < (pos3 - 250) ){
          $('.barFloatLeft').css('padding-top', pos - pos2 );
        }
      }
    }

    setTimeout(function(){
      $('.comments').each(function(){
        $(this).addClass('active');
      });
    }, 600)
  });
}






/*
* Remove V2 das imagens
*/
$('.contentText img').each(function(){
  var src = $(this).attr('src').replace('/v2', '/');
  $(this).attr('src', src);
});


$('.contentText a[href*=".jpg"], .contentText a[href*=".png"]').each(function(){
  $(this).attr('data-fancybox', 'galley');
});


$('.contentText img[src*="ludopedio.dl8it.com.br"]').each(function(){
  let href = $(this).attr('src');
  $(this).attr('src', href.replace('ludopedio.dl8it.com.br', 'ludopedio.org.br'));
});


$('.contentText img[src*="/content/uploads/"]').each(function(){
  let href = $(this).attr('src');
  $(this).attr('src', href.replace('/content/uploads/', '/wp-content/uploads/'));
});


$('.contentText img[src*="http://"]').each(function(){
  let href = $(this).attr('src');
  $(this).attr('src', href.replace('http://', 'https://'));
});


$('#calc_shipping_state').each(function(){
  $(this).val('');
});


$('#formNewsletter').submit(function(e){
  e.preventDefault()
  var email = $(this).find('[name="email"]').val()
  var time = $(this).find('[name="time"]').val()

  $('#mc_embed_signup form').find('[name="EMAIL"]').val( email )
  $('#mc_embed_signup form').find('[name="MMERGE5"]').val( time )
  $('#mc_embed_signup form').submit()
})


$('.wc-block-product-categories__dropdown select option').each(function(){
  if( $(this).val().search('/apoie/') > 0 )
    $(this).remove()
})