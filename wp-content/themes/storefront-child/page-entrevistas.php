<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 12;
$offset = ($paged - 1) * $showposts;
?>
<main class="arquibancada-page">

	<section class="sliderCategory hasBar">
		<div class="slider">
			<?php
				$args = array(
				'post_type'      => 'qd_entrevista',
				'post_status'    => 'publish',
				'posts_per_page' => 4,
			);
			$query = new WP_Query($args);

			while ($query->have_posts()) : $query->the_post();
			?>
				<li class="_itemGrid style-06">
					<div class="ctn">
						<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
						<div class="bottom">
							<div class="main">
								<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
								<h3 class="title-c" style="margin-top: 5px"><?php echo get_the_title($post->ID) ?></h3>
								<span class="autor">
									<span><?php ludo_the_author_no_link( $post->ID ); ?></span>
									| 
									<span><i class="far fa-calendar-alt"></i><?php echo get_the_date('j \d\e F \d\e Y') ?></span>
								</span>
							</div>
						</div>
						<figure class="bg">
							<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'full'); ?>
							<img src="<?php echo $urlThumb ?>" alt="">
						</figure>
					</div>
				</li>
			<?php
			endwhile;
			wp_reset_postdata();
			?>
		</div>
		<div class="barBottom">
			<div class="main">
				<div class="left">
					<a href="<?php echo $link_blog ?>/entrevistas" class="item">Arquivo</a>
					<a href="<?php echo $link_blog ?>/normas-entrevistas" class="item">Normas</a>
					<a href="<?php echo $link_blog ?>/expediente-entrevistas" class="item">Expediente</a>
				</div>
				<div class="issn">ISSN: <?php echo get_field('issn_entrevistas', 'options'); ?></div>
			</div>
		</div>
	</section>

	<div class="barSearch">
		<form action="<?php echo get_the_permalink() ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
					<div class="linkDropdown">
						<span>Volumes <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$termsV = get_terms([
							    'taxonomy' => 'category_entrevista_volume',
							    'hide_empty' => true,
							    'orderby' => 'ID',
    							'order' => 'DESC',
							]);
							foreach ($termsV as $key) {
								$name = explode('Volume ', $key->name);
								echo '<a href="'.get_term_link($key->slug, 'category_entrevista_volume').'">Vol. '.$name[1].' - '.$key->description.'</a>';
							}
						?>
						</div>
					</div>
					<div class="linkDropdown">
						<span>Entrevistas <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$terms = get_terms([
							    'taxonomy' => 'category_entrevista',
							    'hide_empty' => true,
							]);
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug, 'category_entrevista').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<?php if( !isset($_GET['busca']) ): ?>


		<?php $i=0; foreach ($termsV as $keyV): if( $i >= 2 ) continue; ?>
		<?php $name = explode('Volume ', $keyV->name); ?>
		<section class="bloco-volume">
			<div class="top">
				<div class="main">
					<div class="title">
						<span>Entrevista</span>
						<h3><?php echo 'Vol. '.$name[1].' - '.$keyV->description; ?></h3>
					</div>
					<a href="<?php echo get_term_link($keyV->slug, 'category_entrevista_volume') ?>" class="btn-ver">Ver todas ></a>
				</div>
			</div>

			<div class="main">

				<div class="posts col-1">
					<?php

						$args = array(
							'post_type'      => 'qd_entrevista',
							'post_status'    => 'publish',
							'posts_per_page' => 1,
							'category_entrevista_volume' => $keyV->slug
						);
						$query = new WP_Query($args);
						while ($query->have_posts()) : $query->the_post(); ?>
							
							<li class="_itemGridVolume col-1">
								<figure>
									<div class="number">
										<?php
											$numero = explode('Volume ', $keyV->name,);
											echo $numero[1].'.'.get_field('qd_article_number', $post->ID);
										?>
									</div>
									<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
									<a href="<?php the_permalink($post->ID) ?>"><img src="<?php echo $urlThumb ?>" alt=""></a>
								</figure>
								
								<div class="right">
									<div class="totalViews"><?php //echo do_shortcode('[post-views id="'.$post->ID.'"]') ?></div>
									<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
									<a href="<?php the_permalink($post->ID) ?>"><h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3></a>
									<div class="description"><?php echo wp_trim_words( get_the_excerpt($post->ID), 30 ); ?></div>
									<span class="autor"><?php ludo_the_author_no_link( $post->ID ); ?></span>
								</div>
							</li>

					<?php endwhile; ?>
				</div>
				<div class="posts">
				<?php
					$args = array(
						'post_type'      => 'qd_entrevista',
						'post_status'    => 'publish',
						'posts_per_page' => 8,
						'offset'	     => 1,
						'category_entrevista_volume' => $keyV->slug
					);
					$query = new WP_Query($args);
					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style08.php');
					endwhile;
				?>
			</div>
			</div>
		</section>
		<?php if( $i == 0 ) include( get_stylesheet_directory() . '/inc/ads.php'); ?>
		<?php $i++; endforeach; ?>

	<?php else: ?>

		<section class="bloco-volume">

			<div class="main">
				<div class="posts">
					<?php
						$args = array(
							'post_type'      => 'qd_entrevista',
							'post_status'    => 'publish',
							'posts_per_page' => $showposts,
							'offset'  		 => $offset,
						);

						if( isset($_GET['busca']) ){
							$args['s'] = $_GET['busca'];
						}

						if( isset($_GET['ordem']) ){
							if ( $_GET['ordem'] == 'cadastro' ) {
								$args['orderby'] = 'date';
								$args['order'] = 'DESC';
							}else{
								$args['orderby'] = 'title';
								$args['order'] = 'ASC';
							}
						}

						$query = new WP_Query($args);
						while ($query->have_posts()) : $query->the_post();
							include(get_stylesheet_directory() . '/inc/post/post-style08.php');
						endwhile;
					?>
				</div>

				<?php
				echo '<div class="pagination">';
					echo paginate_links(array(
					    'format' => '?pagina=%#%',
					    'current' => $paged,
					    'total' => ceil($query->found_posts / $showposts ),
					    'prev_text' => __('<'),
						'next_text' => __('>'),
					));
					echo '</div>';
				?>
			</div>
		</section>

	<?php endif; ?>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();