<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

?>
<main class="quem-somos-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo')['titulo'];
				$imagem = get_field('banner_topo')['imagem']['url'];
			?>
			<div class="title">
				<h3 class="title"><?php echo $title ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="bloco-first">
		<div class="main">
			<div class="left"><?php echo get_field('texto_bloco01') ?></div>
			<div class="right"><img src="<?php echo get_field('imagem_bloco01')['sizes']['large'] ?>"></div>
		</div>
	</section>

	<section class="bloco-second">
		<div class="main">
			<img src="<?php echo $url_tema ?>assets/images/icones/missao.png" class="icon">
			<h3>Missão</h3>
			<div class="text"><?php echo get_field('texto_bloco02') ?></div>
		</div>
	</section>

	<section class="bloco-second">
		<div class="main">
			<h3>Documentos</h3>
			<div class="links">
				<?php
					if( get_field('documentos_lista') )
						foreach (get_field('documentos_lista') as $key)
							echo '<a style="margin: 0 5px; display: inline-block;" target="_blank" href="'.$key['arquivo'].'" class="btn">'.$key['titulo'].'</a>';
				?>
			</div>
		</div>
	</section>

	<section class="bloco-third">
		<div class="main">
			<h3>O que você vai encontrar?</h3>
			<div class="text"><?php echo get_field('texto_bloco03') ?></div>
		</div>
	</section>

	<section class="bloco-fourth">
		<div class="main">
			<h3>Parceiros</h3>
			<ul class="slider">
				<?php
				$args = array(
					'post_type'      => 'qd_partner',
					'post_status'    => 'publish',
					'posts_per_page' => 60
				);

				$query = new WP_Query($args);

				while ($query->have_posts()) : $query->the_post();
					$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
					if( get_field('link') ){
						echo '<li class="iten"><a href="'.get_field('link').'" target="_blank"><img src="'.$urlThumb.'"></a></li>';
					}else{
						echo '<li class="iten"><img src="'.$urlThumb.'"></li>';
					}
					
				endwhile;
				wp_reset_postdata();
				?>
			</ul>
		</div>
	</section>

	<section class="bloco-fifth">
		<div class="main">
			<div class="left">
				<h3>Editora Ludopédio</h3>
				<div class="text"><?php echo get_field('texto_bloco05') ?></div>
			</div>
			<a href="<?php echo $link_blog ?>/editora" class="btn">Conheça</a>
		</div>
		<img src="<?php echo get_field('background_bloco05')['url'] ?>" class="bg">
	</section>

</main>
<?php
get_footer();