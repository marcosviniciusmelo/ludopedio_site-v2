<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
?>
<main class="colaborador-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo')['titulo'];
				$text = get_field('banner_topo')['subtitulo'];
				$imagem = get_field('banner_topo')['imagem']['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $title ?></h3>
				<div class="text"><?php echo $text ?></div>
			</div>

			<div class="btns">
				<a href="<?php echo $link_blog ?>/registrar" class="btn">CADASTRE-SE</a>
				<a href="<?php echo $link_blog ?>/quem-somos/politica-privacidade/" class="btn">LEIA A POLÍTICA DE PRIVACIDADE</a>
				<a href="<?php echo $link_blog ?>/quem-somos/termos-de-uso/" class="btn">LER TERMOS DE USO</a>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="contentText">
		<div class="main">
			<?php the_content() ?>
		</div>
	</div>

</main>
<?php
get_footer();