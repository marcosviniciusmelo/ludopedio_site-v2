<?php
/* Template Name: #Poroutrofutebol */
get_header();

$arrPost = '';
$tipos = array('qd_arquibancada', 'post', 'qd_entrevista', 'qd_gallery_museu', 'qd_gallery', 'qep_type_event', 'qd_memoria', 'qd_biblioteca');
?>

<main class="home-page outrofutebol-page">

	<section class="bannerTop" style="border-color: <?php echo get_field('cor_base', $post->ID) ?>">
		<div class="main">
			<?php
				$title = get_field('banner_topo')['titulo'];
				$subtitulo = get_field('banner_topo')['subtitulo'];
				$imagem = get_field('banner_topo')['imagem']['url'];
			?>
			<div class="title">
				<span class="title"><?php echo $title; ?></span>
				<h3 class="subtitle"><?php echo $subtitulo ?></h3>
			</div>
		</div>

		<img src="<?php echo $imagem  ?>" alt="" class="bg">
	</section>


	<section class="bloco-posts first">
		<div class="main">
			<div class="title">
				<h3>Últimas notícias</h3>
			</div>

			<div class="posts col-crazy">
				<?php
					
					$args = array(
						'post_type'         => $tipos,
						'post_status'       => 'publish',
						'posts_per_page'	=> 6,
						'tax_query' => array(
					        array(
					            'taxonomy' => 'post_tag',
					            'field'    => 'name',
					            'terms'    => get_field('tag')
					        )
					    )
					);

					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						$arrPost .= ','.$post->ID;
						include(get_stylesheet_directory() . '/inc/post/post-style01.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>

	</section>


	<section class="bloco-posts third">
		<div class="main">
			<div class="title">
				<h3>Ludosfera</h3>
				<div class="right">
					<a href="<?php echo $link_blog ?>/categoria/podcast/" class="btn-ver">Ver todas</a>
					<div class="arrows">
						<div class="arrow prevNav primeiro"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav primeiro"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="posts slider-2" data-name="primeiro">
				<?php
					$args = array(
						'post_type'         => 'post',
						'post_status'       => 'publish',
						'posts_per_page'    => 6,
						'post__not_in' => explode(',', substr($arrPost, 1, -1)),
						'tax_query' => array(
							'relation' => 'AND',
					        array(
					            'taxonomy' => 'post_tag',
					            'field'    => 'name',
					            'terms'    => get_field('tag'),
					        ),
					        array(
					            'taxonomy' => 'category',
					            'field'    => 'slug',
					            'terms'    => array('ludopedio-em-campo'),
					            'operator' => 'NOT IN'
					        )
					    )
					);
					$query = new WP_Query($args);


					if(!$query->have_posts() OR $query->found_posts < 2 ){
						$args['post__not_in'] = array();
						$query = new WP_Query($args);

						if(!$query->have_posts() OR $query->found_posts < 2 ){
							$args = array('post_type' => 'post', 'post_status' => 'publish','posts_per_page' => 6);
							$query = new WP_Query($args);
						}
					} 
					
					while ($query->have_posts()) : $query->the_post();
						$arrPost .= ','.$post->ID;
						include(get_stylesheet_directory() . '/inc/post/post-style02.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>

		<div class="circleYellow" style="border-color: <?php echo get_field('cor_base', $post->ID) ?>"></div>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>


	<section class="bloco-posts loja" style="background-color: <?php echo get_field('cor_base', $post->ID) ?>">
		<div class="main">
			<div class="title">
				<h3 <?php if( $subtitulo == 'Antirracista' ) echo 'style="color: #fff"'; ?>>Loja</h3>
				<a href="<?php echo $link_blog ?>/loja" class="btn-ver">Conheça a loja  ></a>
			</div>

			<?php echo do_shortcode('[recent_products limit=4 category="apoie, cursos" cat_operator="NOT IN"]') ?>
		</div>	
	</section>


	<section class="bloco-posts">
		<div class="main">
			<div class="title">
				<h3>Em Campo</h3>
				<div class="right">
					<a href="<?php echo $link_blog ?>/categoria/ludopedio-em-campo/" class="btn-ver">Ver todas</a>
					<div class="arrows">
						<div class="arrow prevNav grid-3-crazy"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav grid-3-crazy"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="slider-1" data-name="grid-3-crazy">
				<div class="posts col-3-crazy">
					<?php
						$args = array(
							'post_type'         => 'post',
							'post_status'       => 'publish',
							'posts_per_page'    => 6,
							'category_name'		=> 'ludopedio-em-campo',
							'post__not_in' => explode(',', substr($arrPost, 1, -1))
						);
						$i=1;
						$query = new WP_Query($args);
					?>
					<?php
						while ($query->have_posts()) : $query->the_post();
							$arrPost .= ','.$post->ID;
							include(get_stylesheet_directory() . '/inc/post/post-style01.php');

							if( $i%3 == 0 && $i < $query->post_count )
								echo '</div><div class="posts col-3-crazy">';

						$i++;
						endwhile;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</div>	
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

	<section class="bloco-posts grid-6-posts">
		<div class="main">
			<div class="title">
				<h3>Outros destaques</h3>
			</div>
			<div class="ctn-down">
				<div class="left">
					<?php
						$args = array(
							'post_type'         => $tipos,
							'post_status'       => 'publish',
							'posts_per_page'    => 6,
							'tax_query' => array(
						        array(
						            'taxonomy' => 'post_tag',
						            'field'    => 'name',
						            'terms'    => get_field('tag'),
						        ),
						    ),
						);

						// if( get_field('tag') != 'LGTBQIA+' )
						$args['post__not_in'] = explode(',', substr($arrPost, 1, -1));
						
						$query = new WP_Query($args);
					?>
					<?php
						while ($query->have_posts()) : $query->the_post();
							$arrPost .= ','.$post->ID;
							include(get_stylesheet_directory() . '/inc/post/post-style04.php');
						endwhile;
						wp_reset_postdata();
					?>
				</div>
				<div class="ads-right">
					<?php echo do_shortcode('[adrotate group="11"]') ?>
				</div>
			</div>
		</div>	
	</section>

	<?php
		if( get_field('acervo') ){
			$opAcervo = get_field('acervo');
		}else{
			$opAcervo = get_field('acervo', 'options');
		}

		if( $_GET['dev'] )
			print_r( $arrPost );

	?>
	<section class="bloco-posts acervo">
		<div class="main">
			<div class="title">
				<h3><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icones/acervo.png" alt="">Acervo</h3>

				<div class="center">
					<?php
						if( isset($opAcervo['titulo_acervo']) )
							echo $opAcervo['titulo_acervo']
					?>
				</div>

				<div class="right">
					<div class="arrows">
						<div class="arrow prevNav quinto"><i class="fas fa-chevron-left"></i></div>
						<div class="arrow nextNav quinto"><i class="fas fa-chevron-right"></i></div>
					</div>					
				</div>
			</div>

			<div class="posts slider-3" data-name="quinto">
				<?php
					$args = array(
						'post_type' => array('qd_arquibancada', 'post', 'qd_entrevista', 'qd_gallery_museu', 'qd_gallery', 'qd_memoria', 'qd_biblioteca'),
						'post_status'       => 'publish',
						'posts_per_page'    => 6,
						'post__not_in' => explode(',', $arrPost),
					);

					if( $opAcervo['tipo_do_filtro'] == 'Por data' ){
						$args['date_query'] = array(
						    'column' => 'post_date',
						    'after' =>  $opAcervo['data_inicio'],
							'before' => $opAcervo['data_fim'] 
						);
					}else{
						$tags = explode(',', $opAcervo['tag']);
						$args['tax_query'] = array(
					        array(
					            'taxonomy' => 'post_tag',
					            'field'    => 'name',
					            'terms'    => $tags,
					            'operator' => 'IN',
					        )
					    );
					}

					if( $_GET['dev'] )
						print_r( $args );


					$query = new WP_Query($args);

					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style05.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>
		<div class="circleYellow" style="border-color: <?php echo get_field('cor_base', $post->ID) ?>"></div>
	</section>

</main>	

<?php
get_footer();