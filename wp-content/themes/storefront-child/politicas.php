<?php

/* Template Name: Politicas */
get_header();
global $url_tema, $nome_blog, $link_blog;

?>
<main class="politicas-page">

	<div class="main">
		<?php the_content() ?>
	</div>

</main>
<?php
get_footer();