<?php
/* Template Name: Membros */
get_header();
global $url_tema, $nome_blog, $link_blog;

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 20;
$offset = ($paged - 1) * $showposts;

$args = [];

$args = array(
	'role__in' => array('contributor', 'administrator'),
	'number'   => $showposts,
	'offset'   => $offset,
	'meta_query' => [
        'relation' => 'OR',
        ['key' => 'user_last_login', 'compare' => 'NOT EXISTS'], // this comes first!
        ['key' => 'user_last_login', 'compare' => 'EXISTS'],
    ],
	'orderby'  => 'meta_value',
	'order'    => 'DESC'
);

if( isset($_GET['busca']) ){
	$args['search'] = '*'.$_GET['busca'].'*';
}

if( isset($_GET['ordem']) ){
	if ( $_GET['ordem'] == 'cadastro' ) {
		$args['orderby'] = 'user_registered';
		$args['order'] = 'DESC';
	}else{
		$args['orderby'] = 'display_name';
		$args['order'] = 'ASC';
	}
}


$users = new WP_User_Query($args);
$total = $users->get_total();

?>
<main class="membros-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo')['titulo'];
				$imagem = get_field('banner_topo')['imagem']['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $title ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/membros">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
					<?php
						if ( !is_user_logged_in() )
							echo '<a href="'.$link_blog.'/registrar" class="btn">REGISTRE-SE</a>';
					?>
					
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="blocoGrid">
		<div class="main">
			<div class="title">
				<h3>Membros</h3>
				<span>Total de membros <?php echo $total ?></span>
			</div>

			<?php
				echo '<div class="pagination">';
				echo paginate_links(array(
				    'format' => '?pagina=%#%',
				    'current' => $paged,
				    'total' => ceil($total / $showposts ),
				    'prev_text' => __('<'),
					'next_text' => __('>'),
				));
				echo '</div>';
			?>

			<ul class="grid">

				<?php
					
					foreach ( $users->get_results() as $user ) {

						// print_r(get_user_meta($user->ID));

						$foto = get_avatar_url($user->ID, ['size' => '200' ]);
						$link = $link_blog.'/autor/?p='.$user->user_nicename;

					    echo '<li class="_item">
					    		<a href="'.$link.'">
									<figure><img src="'.$foto.'" alt=""></figure>
									<h4>'.esc_html( $user->display_name ).'</h4>
								</a>
							</li>';
					}
					
				?>
			</ul>

			<?php
				echo '<div class="pagination">';
				echo paginate_links(array(
				    'format' => '?pagina=%#%',
				    'current' => $paged,
				    'total' => ceil($total / $showposts ),
				    'prev_text' => __('<'),
					'next_text' => __('>'),
				));
				echo '</div>';
			?>
		</div>
	</section>


</main>

<?php
get_footer();