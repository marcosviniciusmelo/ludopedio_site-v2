<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

?>
<main class="quem-somos-page">


	<div class="main">
		<ul class="expedienteLista">
			<?php
				foreach (get_field('listas') as $key):
					echo '<li class="_item">
							<h3>'.$key['titulo'].'</h3>
							<div class="grid">';
							foreach ($key['membros'] as $membro):
								$user_info = get_userdata($membro['membro']);
								$foto = get_avatar_url($user_info->ID, ['size' => '200' ]);
								$link = $link_blog.'/author/'.$user_info->user_nicename;
								echo '<div class="item">
										<a href="'.$link.'"><figure><img src="'.$foto.'"></figure></a>
										<a href="'.$link.'"><h5>'. $user_info->display_name .'</h5></a>
										<span>'. $membro['legenda'] .'</span>
									  </div>';
							endforeach;
						echo '</div>
						</li>';
				endforeach;
			?>
		</ul>
	</div>

</main>
<?php
get_footer();