<?php
	
	//Remove notes field on checkout
	// add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

add_action( 'after_setup_theme', 'remove_pgz_theme_support', 100 );
function remove_pgz_theme_support() { 
	remove_theme_support( 'wc-product-gallery-zoom' );

}

// Change 'add to cart' text on single product page
add_filter( 'woocommerce_product_add_to_cart_text', 'kaksi_add_to_cart_text' );
function kaksi_add_to_cart_text() {
	global $product;
	if( $product->get_stock_status() == 'outofstock' ):
		return __( 'Fora de estoque', 'woocommerce' );
	else:
		return __( 'Adicionar ao carrinho', 'woocommerce' );
	endif;
	
}

// add_filter( 'woocommerce_account_menu_items', 'custom_remove_downloads_my_account', 999 );
// function custom_remove_downloads_my_account( $items ) {
// 	unset($items['downloads']);
	// unset($items['dashboard']);
// 	return $items;
// }



/**
 * Exclude products from a particular category on the shop page
 */
function custom_pre_get_posts_query( $q ) {
    $tax_query = (array) $q->get( 'tax_query' );
    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field' => 'slug',
           'terms' => array( 'apoie', 'cursos' ), // Don't display products in the clothing category on the shop page.
           'operator' => 'NOT IN'
    );
    $q->set( 'tax_query', $tax_query );
}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );  