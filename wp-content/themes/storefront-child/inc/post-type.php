<?php

/*
* Arquibancada
*/
function custom_arquibancada() {
    $labels = array(
      'name'                => ( 'Arquibancada'),
      'singular_name'       => ( 'Arquibancada'),
      );
    
    register_post_type( 'qd_arquibancada',
        array(
            'menu_icon'   => 'dashicons-admin-post',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array( 'title', 'editor', 'author', 'excerpt', 'thumbnail'),
            'rewrite' => array(
                'slug' => 'arquibancada',
            ),
            'taxonomies' => array('post_tag')
        )
    );

    register_taxonomy(
        'category_arquibancada',
        'qd_arquibancada',
        array(
            'hierarchical' => true,
            'label' => 'Categorias',
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'arquibancada-categoria',
                'with_front' => false
            )
        )
    );

    register_taxonomy(
        'category_arquibancada_volume',
        'qd_arquibancada',
        array(
            'hierarchical' => true,
            'label' => 'Volume',
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'arquibancada-volumes',
                'with_front' => false
            )
        )
    );
}
add_action('init', 'custom_arquibancada');


/*
* Entrevistas
*/
function custom_entrevista() {
    $labels = array(
      'name'                => ( 'Entrevistas'),
      'singular_name'       => ( 'Entrevista'),
      );
    
    register_post_type( 'qd_entrevista',
        array(
            'menu_icon'   => 'dashicons-microphone',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array( 'title', 'editor','author', 'excerpt', 'thumbnail'),
            'rewrite' => array(
                'slug' => 'entrevista',
            ),
            'taxonomies' => array('post_tag')
        )
    );

    register_taxonomy(
        'category_entrevista',
        'qd_entrevista',
        array(
            'hierarchical' => true,
            'label' => 'Cat. Entrevistas',
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'entrevista-categoria',
                'with_front' => false
            )
        )
    );

    register_taxonomy(
        'category_entrevista_volume',
        'qd_entrevista',
        array(
            'hierarchical' => true,
            'label' => 'Volume',
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'entrevistas-volumes',
                'with_front' => false
            )
        )
    );

}
add_action('init', 'custom_entrevista');



/**
*
* Museu
*
**/

function custom_museu() {
    $labels = array(
      'name'                => ('Museu'),
      'singular_name'       => ('Museu'),
      );
    
    register_post_type( 'qd_gallery_museu',
        array(
            'menu_icon'   => 'dashicons-format-gallery',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array('title','excerpt','thumbnail','author','comments'),
            'rewrite' => array(
                'slug' => 'museu-galeria',
            ),
            'taxonomies' => array('post_tag')
        )
    );

    register_taxonomy(
        'category_gallery_museu',
        'qd_gallery_museu',
        array(
            'hierarchical' => true,
            'label' => 'Categorias',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'museu-categoria',
            )
        )
    );

}
add_action('init', 'custom_museu');



/**
*
* Futebol arte
*
**/

function custom_futebol() {
    $labels = array(
      'name'                => ('Futebol Arte'),
      'singular_name'       => ('Futebol Arte'),
      'slug'                => 'futebol-arte',
    );
    
    register_post_type( 'qd_gallery',
        array(
            'menu_icon'   => 'dashicons-format-gallery',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array('title','excerpt','thumbnail','author','comments'),
            'rewrite' => array(
                'slug' => 'galeria',
            ),
            'taxonomies' => array('post_tag')
        )
    );

    register_taxonomy(
        'category_gallery',
        'qd_gallery',
        array(
            'hierarchical' => true,
            'label' => 'Categorias',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'futebol-arte-categoria',
            )
        )
    );

}
add_action('init', 'custom_futebol');


/**
*
* Eventos
*
**/
function custom_eventos() {
    $labels = array(
      'name'                => ('Eventos'),
      'singular_name'       => ('Eventos'),
      'slug'                => 'agenda-de-eventos',
    );
    
    register_post_type( 'qep_type_event',
        array(
            'menu_icon'   => 'dashicons-calendar-alt',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array('title','thumbnail', 'excerpt','author', 'editor'),
            'rewrite' => array(
                'slug' => 'agenda-de-eventos',
            ),
            'taxonomies' => array('post_tag')
        )
    );

    
    // register_taxonomy('qep_type_event', 'post', array(
    //     'taxonomy_name' => 'qep_type_event',
    //     'singular' => 'Evento',
    //     'plural' => 'Eventos',
    //     'slug' => 'agenda-eventos'
    // ));

}
add_action('init', 'custom_eventos');


/**
*
* Parceiros
*
**/
function custom_parceiros() {
    $labels = array(
      'name'                => ('Parceiros'),
      'singular_name'       => ('Parceiros'),
    );
    
    register_post_type( 'qd_partner',
        array(
            'menu_icon'   => 'dashicons-businessman',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'rewrite' => array(
                'slug' => 'parceiro',
            ),
            'supports'    => array('title','thumbnail','author', 'editor'),
            'taxonomies' => array('post_tag')
        )
    );
}
add_action('init', 'custom_parceiros');


/**
*
* Linha do tempo
*
**/
function custom_memoria() {
    $labels = array(
      'name'                => ('Linha do tempo'),
      'singular_name'       => ('Linha do tempo'),
      'slug'                => 'agenda-de-eventos',
    );
    
    register_post_type( 'qd_memoria',
        array(
            'menu_icon'   => 'dashicons-backup',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array('title','thumbnail','author', 'editor'),
            'rewrite' => array(
                'slug' => 'memoria',
            ),
            'taxonomies' => array('post_tag')
        )
    );
    
    register_taxonomy(
        'category_memoria',
        'qd_memoria',
        array(
            'hierarchical' => true,
            'label' => 'Cat. Memória',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'memoria-categoria',
            )
        )
    );
}
add_action('init', 'custom_memoria');


/**
*
* Bibioteca
*
**/
function custom_biblioteca() {
    $labels = array(
      'name'                => ('Biblioteca'),
      'singular_name'       => ('Biblioteca'),
      'slug'                => 'agenda-de-eventos',
    );
    
    register_post_type( 'qd_biblioteca',
        array(
            'menu_icon'   => 'dashicons-book-alt',
            'labels'      => $labels,
            'public'      => true,
            'has_archive' => true,
            'supports'    => array('title','thumbnail', 'excerpt','author','editor'),
            'rewrite' => array(
                'slug' => 'biblioteca',
            ),
            'taxonomies' => array('post_tag')
        )
    );
    
    register_taxonomy(
        'category_biblioteca',
        'qd_biblioteca',
        array(
            'hierarchical' => true,
            'label' => 'Cat. Biblioteca',
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'biblioteca-categoria',
            )
        )
    );
}
add_action('init', 'custom_biblioteca');


function custom_author() {
    register_taxonomy(
        'category_autor',
        array('qd_arquibancada', 'qd_entrevista', 'post', 'qd_gallery_museu', 'qd_gallery', 'qd_memoria', 'qd_biblioteca', 'product'),
        array(
            'label' => 'Autor(es)',
            'query_var' => true,
            'show_admin_column' => true,
            'rewrite' => array(
                'slug' => 'autores',
            )
        )
    );
}
add_action('init', 'custom_author');

function add_admin_column($column_title, $post_type, $cb){

    // Column Header
    add_filter( 'manage_' . $post_type . '_posts_columns', function($columns) use ($column_title) {
        $columns[ sanitize_title($column_title) ] = $column_title;
        return $columns;
    } );

    // Column Content
    add_action( 'manage_' . $post_type . '_posts_custom_column' , function( $column, $post_id ) use ($column_title, $cb) {

        if(sanitize_title($column_title) === $column){
            $cb($post_id);
        }

    }, 10, 2 );
}