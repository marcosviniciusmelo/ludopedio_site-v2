<div class="authorCtn">
	<?php
		$authorID = get_the_author_meta('ID');

		$userInsta = get_field( 'instagram', 'user_'.$authorID );
		$userFace = get_field( 'facebook', 'user_'.$authorID );
		$userTwitter = get_field( 'twitter', 'user_'.$authorID );
		$foto = get_avatar_url($authorID, ['size' => '100' ]);
		$nome = get_the_author_meta('display_name');
		$link = get_the_author_meta('user_nicename');

	?>

	<figure><img src="<?php echo $foto ?>"></figure>
	<div class="right">
		<div class="line">
			<h5><a href="<?php echo get_bloginfo('url').'/autor/?p='.$link ?>'"><?php echo $nome ?></a></h5>
			<?php
				echo '<div class="redes">';
					if( $userInsta != '' )
						echo '<a href="'.$userInsta.'" target="_blank" class="link-instagram"><i class="fab fa-instagram"></i></a>';

					if( $userFace != '' )
						echo '<a href="'.$userFace.'" target="_blank" class="link-facebook"><i class="fab fa-facebook-f"></i></a>';

					if( $userTwitter != '' )
						echo '<a href="'.$userTwitter.'" target="_blank" class="link-twitter"><i class="fab fa-twitter"></i></a>';

				echo '</div>';
			?>
		</div>
		<div class="bio"></div>
	</div>
</div>