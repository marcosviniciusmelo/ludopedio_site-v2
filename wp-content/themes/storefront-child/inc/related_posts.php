<div class="relateds">
	<?php
		$label = 'Veja';
		if( get_post_type() == 'post' ){
			if( function_exists('powerpress_get_enclosure_data') ){
				$EpisodeData = powerpress_get_enclosure_data($post->ID);
				    if( $EpisodeData )
						$label = 'Ouça';
			}
		}
	?>
	<div class="title"><?php echo $label; ?> também:</div>

	<div class="scroll">
		<ul class="posts col-3">
			<?php
				switch ( get_post_type($post_id) ) {
			        case 'qd_arquibancada':
			            $category = 'category_arquibancada';
			            break;

			        case 'qd_entrevista':
			            $category = 'category_entrevista';
			            break;

			        case 'qd_gallery_museu':
			            $category = 'category_gallery_museu';
			            break;

			        case 'qd_gallery':
			            $category = 'category_gallery';
			            break;

			        case 'qd_memoria':
			            $category = 'category_memoria';
			            break;

			        default:
			            $category = 'category';
			            break;
			    }
			    

				$args = array(
				    'post_type' 	 => get_post_type(),
				    'posts_per_page' => 3,
				    'post_status'    => 'publish',
				    'post__not_in'   => array(get_the_ID()),
				);

				if( get_post_type($post_id) != 'qep_type_event' ):
					$cate = wp_get_object_terms(get_the_ID(), $category);

					$args['tax_query'] = array(
				        array(
				            'taxonomy' => $cate[0]->taxonomy,
				            'field'    => 'slug',
				            'terms'    => $cate[0]->slug,
				        ),
				    );

					
					if( $_GET['dev'] ):
						$tagsPost = get_the_tags($post_id);
						print_r( $tagsPost );
						print_r( $post_id );

						if( $tagsPost ):
							$tagsArr = [];
							foreach ($tagsPost as $key) {
								array_push($tagsArr, $key->term_id);
							}
							$args['tag_slug__in'] = $tagsArr;
							print_r( $args );
						endif;
					endif;
				endif;

				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
					include(get_stylesheet_directory() . '/inc/post/post-style02.php');
				endwhile;
				wp_reset_postdata();
			?>
		</ul>
	</div>
</div>