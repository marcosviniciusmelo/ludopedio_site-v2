<?php
global $url_tema, $link_blog, $nome_blog; the_post();
get_header();
$cate = get_the_category()[0];

$args = array(
	'role__in' => array('contributor', 'administrator'),
	'meta_query' => array(
        array(
        	'relation' => 'OR',
            array(
            	'key'     => 'user_last_login',
	            'compare' => 'NOT EXISTS'
            ),
            array(
            	'key'     => 'user_last_login',
	            'compare' => 'EXISTS'
            )
        )
    ),
	'orderby'  => 'meta_value',
	'order'    => 'DESC'
);

$users = new WP_User_Query($args);
// $total = $users->get_total();
$arrayUser = [];
foreach ( $users->get_results() as $user ) {
	$grupo = get_field('grupo_de_pesquisa', 'user_'.$user->ID);
	if( $grupo ):
		if ( in_array($post->ID, $grupo)) {
			array_push($arrayUser, $user->ID);
		}
	endif;
}

?>

<main class="single-post-page grupo">

	<section class="bannerTop">
		<div class="main">
			<?php
				$imagem = get_field('background_topo', 'category_'.$cate->term_id)['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $cate->name ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog . '/categoria/' . $cate->slug ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="content-grupo">
		
		<div class="main">
			<div class="left">
				<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
				<img src="<?php echo $urlThumb ?>" alt="">
			</div>
			<div class="right">
				<div class="topo"><?php the_title() ?></div>
				<div class="contentText"><?php the_content(); ?></div>

				<?php if( $arrayUser ): ?>
				<h4 class="titleGrid">Integrantes do grupo de pesquisa</h4>
				<ul class="grid">
					<?php
						foreach ( $arrayUser as $user ) {
							$user_info = get_userdata($user);
							$foto = get_avatar_url($user, ['size' => '200' ]);
							$link = $link_blog.'/autor/?p='.$user_info->user_nicename;

						    echo '<li class="_item">
						    		<a href="'.$link.'">
										<figure><img src="'.$foto.'" alt=""></figure>
										<h5>'.esc_html( $user_info->display_name ).'</h5>
									</a>
								</li>';
						}
					?>
				</ul>
				<?php endif; ?>
				
			</div>
		</div>	

	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();