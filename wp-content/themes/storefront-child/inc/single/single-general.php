<?php
global $url_tema, $link_blog, $nome_blog; the_post();
get_header();
$cate = get_queried_object();
?>

<main class="single-post-page">

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<div class="ctnPostContent">
		<section class="post-content">
			<div class="main">

				<div class="ctnScroll">

					<div class="lineTitlePost">
						<div class="category">
							<?php echo getCategoryPost($post->ID);?>
						</div>
						<h2><?php the_title() ?></h2>

						<?php if( !get_field('qep_event_start_date') && !get_field('qep_event_end_date') ): ?>
						<div class="date">
							<span><?php ludo_the_author_link( $post->ID ); ?></span>
							<span class="bar"></span>
							<span>
							<?php
								if( get_field('qd_article_time_line_date') ):
									if( strripos(get_field('qd_article_time_line_date'), '/') === false ){
										echo strftime('%d de %B de %Y', get_field('qd_article_time_line_date') );
									}else{
										echo get_field('qd_article_time_line_date');
									}
								else:
									echo get_the_date('j \d\e F \d\e Y');
								endif;
									
							?>
							</span>
						</div>
						<?php endif; ?>

						<?php
						if( is_singular('qep_type_event') ):
							$arrInfos = [];

							if( get_field('qep_event_start_date') )
									array_push($arrInfos, '<span>Data início: '.date('d/m/Y', strtotime(get_field('qep_event_start_date'))).'</span>');

							if( get_field('qep_event_end_date') )
								array_push($arrInfos, '<span>Data de encerramento: '.date('d/m/Y', strtotime(get_field('qep_event_end_date'))).'</span>');

							if( get_field('qep_event_start_time') )
								array_push($arrInfos, '<span>Horário de início: '.get_field('qep_event_start_time').'</span>');

							if( get_field('qep_event_end_time') )
								array_push($arrInfos, '<span>Horário de encerramento: '.get_field('qep_event_end_time').'</span>');

							if( get_field('qep_event_place') )
								array_push($arrInfos, '<span>Local: '.get_field('qep_event_place').'</span>');

							if( get_field('qd_event_date_limit_form') )
								array_push($arrInfos, '<span>Data limite inscrição: '.date('d/m/Y', strtotime(get_field('qd_event_date_limit_form'))).'</span>');

							echo '<div class="date">';
								echo implode('<span class="bar"></span>', $arrInfos);
							echo '</div>';

						endif;
						?>

					</div>

					<div class="barFloatLeft">
						<?php include(get_stylesheet_directory() . '/inc/barShared.php') ?>
					</div>

					<div class="contentText">

						<?php the_content() ?>
						<?php ludo_the_author_box() ?>

					</div>

					<?php if( get_field('url_do_video') != '' ): ?>
					<figure class="imageMain">
						<?php
							$urlVideo = get_field('url_do_video');
							preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $urlVideo, $matches);

							echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$matches[1].'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
						?>
					</figure>
					<?php endif; ?>

					<?php
						include(get_stylesheet_directory() . '/inc/related_posts.php');
						include(get_stylesheet_directory() . '/inc/comments.php');
					?>
				</div>

			</div>
		</section>

		<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>
		
	</div>

</main>

<?php
get_footer();