<li class="_itemGrid style-06">
	<div class="ctn">
		<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
		<div class="bottom">
			<div class="main">
				<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
				<span class="date">
					<?php
						if( get_field('qd_article_time_line_date') ):
							setlocale(LC_ALL, 'pt_BR.utf8');
							echo date('d/m/Y', get_field('qd_article_time_line_date'));
						else:
							echo get_the_date('j \d\e F \d\e Y');
						endif;
					?>	
				</span>
				<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
				<span class="autor"><?php ludo_the_author_no_link( $post->ID ); ?></span>
			</div>
		</div>
		<figure class="bg">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'full'); ?>
			<img src="<?php echo $urlThumb ?>" alt="">
		</figure>
	</div>
</li>