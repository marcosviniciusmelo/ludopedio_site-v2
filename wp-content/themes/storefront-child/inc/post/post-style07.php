<li class="_itemGrid style-07">
	<div class="ctn">
		<figure class="bg">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
			<a href="<?php the_permalink() ?>"><img src="<?php echo $urlThumb ?>" alt=""></a>
		</figure>
		<div class="bottom">
			<a href="<?php the_permalink() ?>"><h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3></a>
			<div class="description"><?php echo wp_trim_words( get_the_excerpt($post->ID), 20 ); ?></div>
			<div class="lineBottom">
				<?php
					$dates = '';

					if( get_field('qep_event_start_date', $post->ID) != '' ):
						$dateStart = get_field('qep_event_start_date', $post->ID);
						$dates .= date('d/m/Y', strtotime($dateStart));
					endif;

					if( get_field('qep_event_end_date', $post->ID) != '' && get_field('qep_event_end_date', $post->ID) != get_field('qep_event_start_date', $post->ID) ):
						$dateEnd = get_field('qep_event_end_date', $post->ID);
						$dates .= ' - ' . date('d/m/Y', strtotime($dateEnd));
					endif;
				?>
				<span class="date"><i class="far fa-calendar-alt"></i> <?php echo $dates ?></span>

				<?php
					if( get_field('qd_event_date_limit_form', $post->ID) != '' ){
						if( get_field('qd_event_date_limit_form', $post->ID) >= date("Ymd") ){
							echo '<a href="'.get_the_permalink().'" class="btn">INSCRIÇÕES ABERTAS</a>';
						}else{
							echo '<a href="'.get_the_permalink().'" class="btn red">INSCRIÇÕES ENCERRADAS</a>';
						}
					}
				?>

			</div>
			
		</div>
	</div>
</li>