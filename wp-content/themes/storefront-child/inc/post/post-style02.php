<li class="_itemGrid style-02">
	<div class="ctn">
		<a href="<?php the_permalink($post->ID) ?>" class="link"></a>

		<?php $termsPost = get_the_terms($post->ID, 'category'); ?>

		<?php

			if ( in_category('video') ) {
				echo '<i class="far fa-play-circle iconMicrophone"></i>';
			}else{
				if( function_exists('powerpress_get_enclosure_data') ) {
				    $EpisodeData = powerpress_get_enclosure_data($post->ID);
				    if( $EpisodeData ) {
				       echo '<i class="fas fa-microphone-alt iconMicrophone"></i>';
				    }
				}
			}

			if( get_field('qd_article_time_line_date') ):
				$date = date('d/m/Y', get_field('qd_article_time_line_date'));
			else:
				$date = get_the_date('j \d\e F \d\e Y');
			endif;
		?>

		<div class="bottom">
			<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
			<span class="date"><?php echo $date ?></span>
			<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
			<span class="autor"><?php ludo_the_author_no_link( $post->ID ); ?></span>
		</div>
		<figure class="bg">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
			<img src="<?php echo $urlThumb ?>" alt="">
		</figure>
	</div>
</li>