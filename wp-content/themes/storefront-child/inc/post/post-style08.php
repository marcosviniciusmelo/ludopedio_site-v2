<li class="_itemGrid style-06">
	<div class="ctn">
		<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
		<div class="number">
			<?php
				if( get_post_type($post->ID) == 'qd_arquibancada' ){
					$term = get_the_terms($post->ID, 'category_arquibancada_volume');
				}else{
					$term = get_the_terms($post->ID, 'category_entrevista_volume');
				}
				$numero = explode('Volume ', $term[0]->name);
				echo $numero[1].'.'.get_field('qd_article_number', $post->ID);
			?>
		</div>
		<div class="totalViews"><?php //echo do_shortcode('[post-views id="'.$post->ID.'"]') ?></div>
		<div class="bottom">
			<div class="main">
				<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
				<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
				<span class="autor"><?php ludo_the_author_no_link( $post->ID ); ?></span>
			</div>
		</div>
		<figure class="bg">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
			<img src="<?php echo $urlThumb ?>" alt="">
		</figure>
	</div>
</li>