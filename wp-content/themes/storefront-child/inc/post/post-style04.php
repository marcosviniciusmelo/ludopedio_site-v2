<li class="_itemGrid style-04">
	<div class="ctn">
		<figure>
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
			<a href="<?php the_permalink($post->ID) ?>"><img src="<?php echo $urlThumb ?>" alt=""></a>
		</figure>
		<div class="right">
			<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
			<a href="<?php the_permalink($post->ID) ?>"><h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3></a>
			<a href="<?php the_permalink($post->ID) ?>" class="more">Ler mais ></a>
		</div>
	</div>
</li>