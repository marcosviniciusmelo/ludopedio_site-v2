<li class="_itemGrid">
	<div class="ctn">
		<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
		<div class="bottom">
			<ul class="tags"><?php echo getCategoryPost($post->ID); ?></ul>
			<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
			<span class="autor"><?php ludo_the_author_no_link( $post->ID, true, true ); ?></span>
		</div>
		<figure class="bg">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
			<img src="<?php echo $urlThumb ?>" alt="">
		</figure>
	</div>
</li>