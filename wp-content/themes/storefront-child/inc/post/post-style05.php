<li class="_itemGrid style-05">
	<div class="ctn">
		<a href="<?php the_permalink($post->ID) ?>" class="link"></a>
		<figure class="bg">
			<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'medium'); ?>
			<img src="<?php echo $urlThumb ?>" alt="">
		</figure>
		<div class="bottom">
			<span class="date"><?php echo get_the_date('j \d\e F \d\e Y') ?></span>
			<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
			<span class="autor"><?php ludo_the_author_no_link( $post->ID, true, true ); ?></span>
		</div>
	</div>
</li>