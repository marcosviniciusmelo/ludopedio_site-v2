<?php
	global $wpdb;

	// get all subscriptions IDS
	$subscriptions_ids = $wpdb->get_col("
	    SELECT ID  FROM {$wpdb->prefix}posts 
	    WHERE post_type LIKE 'shop_subscription'
	");

	$totalPerson = [];

	foreach($subscriptions_ids as $subscription_id){

	    $subscription = new WC_Subscription( $subscription_id );
	    $data = $subscription->get_data();

	    if( $data['status'] == 'active' ):
		    if( floatval($data['customer_id']) > 0 )
		    	if (!in_array($data['customer_id'], $totalPerson))
		    		array_push($totalPerson, $data['customer_id']);

		endif;

	}
?>

<div class="clever-core-ads"></div>

<div class="barAds first">
	<div class="ads apoie">
		<span class="left">Seja um dos <strong><span class="_numberAnimated"><?php echo count($totalPerson) ?></span> <?php echo (count($totalPerson) > 1) ? 'apoiadores' : 'apoiador'  ?> do Ludopédio</strong> e faça parte desse time!</span>
		<a href="<?php echo $link_blog ?>/apoie" class="btn btn-purple">APOIAR AGORA</a>
	</div>
</div>