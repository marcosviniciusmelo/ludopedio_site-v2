<div class="barShared">
	<label>COMPARTILHE</label>
	<div class="redes">
		<?php
			$url = urlencode( get_the_permalink() );
			$title = strip_tags( get_the_title() );
		?>
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&t=<?php echo $title; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" class="link-facebook"><i class="fab fa-facebook-f"></i></a>
		<a href="https://twitter.com/share?url=<?php echo $url; ?>&via=Ludopedio&text=<?php echo $title; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" class="link-twitter"><i class="fab fa-twitter"></i></a>
		<a data-network="whatsapp" data-url="<?php echo get_the_permalink() ?>" class="st-custom-button" class="link-whatsapp"><i class="fab fa-whatsapp"></i></a>
	</div>
</div>