<?php
// if ( $GLOBALS['wp_query']->max_num_pages <= 1 ) {
//   return;
// }

$big = 999999999; // need an unlikely integer
if( !isset($showposts) )
	$showposts = 8;

$page = 'page';
if( isset($pageType) )
    $page = $pageType;

echo '<div class="pagination">';
echo paginate_links(array(
    'format' => '?'.$page.'=%#%',
    'current' => $paged,
    'total' => ceil($query->found_posts / $showposts ),
    'prev_text' => __('<'),
	'next_text' => __('>'),
));
echo '</div>';

