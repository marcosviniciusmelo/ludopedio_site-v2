<?php
global $url_tema, $link_blog, $nome_blog; the_post();
get_header();
$page = get_page_by_path('entrevistas');
$category = get_the_terms( $post->ID, 'category_entrevista_volume' );
$term = get_the_terms($post->ID, 'category_entrevista_volume');
$numero = explode('Volume ', $term[0]->name);
?>

<main class="single-post-page entrevista">

	<section class="bannerTop entrevista">
		<div class="main">
			<?php
				$imagem = get_field('banner_topo', $page->ID)['imagem'];
			?>
			<div class="title">
				<span class="title">Entrevistas</span>
			</div>
		</div>

		<div class="barBottom">
			<div class="main">
				<div class="left">
					<a href="<?php echo $link_blog ?>/entrevistas" class="item">Arquivo</a>
					<a href="<?php echo $link_blog ?>/normas-entrevistas" class="item">Normas</a>
					<a href="<?php echo $link_blog ?>/expediente-entrevistas" class="item">Expediente</a>
				</div>
				<div class="issn">ISSN: <?php echo get_field('issn_entrevistas', 'options'); ?></div>
			</div>
		</div>

		<img src="<?php echo $imagem['url']  ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/entrevistas">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
					<div class="linkDropdown">
						<span>Volumes <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$termsV = get_terms([
							    'taxonomy' => 'category_entrevista_volume',
							    'hide_empty' => true,
							    'orderby' => 'ID',
    							'order' => 'DESC',
							]);
							foreach ($termsV as $key) {
								$name = explode('Volume ', $key->name);
								echo '<a href="'.get_term_link($key->slug, 'category_entrevista_volume').'">Vol. '.$name[1].' - '.$key->description.'</a>';
							}
						?>
						</div>
					</div>
					<div class="linkDropdown">
						<span>Entrevistas <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$terms = get_terms([
							    'taxonomy' => 'category_entrevista',
							    'hide_empty' => true,
							]);
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug, 'category_entrevista').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="ctnPostContent">
		<section class="post-content">
			<div class="main">

				<div class="ctnScroll">

					<div class="lineTitlePost">
						<div class="numero"><?php echo $numero[1].'.'.get_field('qd_article_number', $post->ID); ?></div>
						<div class="category"><a href="<?php echo get_term_link($category[0]->slug, 'category_entrevista_volume'); ?>"><?php echo $category[0]->name; ?></a></div>
						<h2><?php the_title() ?></h2>

						<div class="date">
							<span><?php ludo_the_author_link( $post->ID ); ?></span>
							<span class="bar"></span>
							<?php if( ludo_group_author_link( $post->ID ) ): ?>
								<span><?php print_r( ludo_group_author_link( $post->ID ) ); ?></span>
								<span class="bar"></span>
							<?php endif; ?>
							<span><i class="far fa-calendar-alt"></i> <?php echo get_the_date('j \d\e F \d\e Y') ?></span>
						</div>

					</div>

					<div class="barFloatLeft">
						<?php include(get_stylesheet_directory() . '/inc/barShared.php') ?>
					</div>

					<div class="contentText">

						<?php the_content() ?>
						<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>
						<?php ludo_the_author_box() ?>

					</div>

					<div class="relateds">
						<div class="title">Leia também:</div>

						<ul class="posts col-3 pd-20">
							<?php

								$term = get_the_terms($post->ID, 'category_entrevista_volume');

								$args = array(
								    'post_type' 	 => 'qd_entrevista',
								    'posts_per_page' => 3,
								    'post_status'    => 'publish',
								    'post__not_in'   => array(get_the_ID()),
								    'category_entrevista_volume' => $term[0]->name
								);

								$query = new WP_Query($args);
								while ($query->have_posts()) : $query->the_post();
									include(get_stylesheet_directory() . '/inc/post/post-style08.php');
								endwhile;
								wp_reset_postdata();
							?>
						</ul>
					</div>

					<?php
						include(get_stylesheet_directory() . '/inc/comments.php');
					?>
				</div>

			</div>
		</section>

		<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>
		
	</div>

</main>

<?php
get_footer();