<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 10;
$offset = ($paged - 1) * $showposts;
$not_id = get_term_by('slug', 'video', 'category' );
?>
<main class="ludosfera-page">

	<section class="sliderCategory">
		<div class="slider">
			<?php
				$args = array(
				'post_type'      => 'post',
				'post_status'    => 'publish',
				'category_name'  => 'podcast',
				'posts_per_page' => 4,
				'category__not_in' => array($not_id->term_id)
			);
			$query = new WP_Query($args);

			while ($query->have_posts()) : $query->the_post();

				include(get_stylesheet_directory() . '/inc/post/post-style06.php');

			endwhile;
			wp_reset_postdata();
			?>
		</div>
	</section>

	<div class="barSearch">
		<form action="<?php echo get_the_permalink() ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
					<div class="linkDropdown">
						<span>Filtrar por <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$terms = get_terms([
							    'taxonomy' => 'category',
							    'hide_empty' => false,
							]);
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug, 'category').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="bloco-posts">
		<div class="main">

			<div class="posts col-2">
				<?php

					$args = array(
						'post_type'      => 'post',
						'post_status'    => 'publish',
						'category_name'  => 'podcast',
						'category__not_in'   => array($not_id->term_id),
						'posts_per_page' => $showposts,
						'offset'		 => $offset,
					);

					if( isset($_GET['busca']) ){
						$args['s'] = $_GET['busca'];
					}

					if( isset($_GET['ordem']) ){
						if ( $_GET['ordem'] == 'cadastro' ) {
							$args['orderby'] = 'date';
							$args['order'] = 'DESC';
						}else{
							$args['orderby'] = 'title';
							$args['order'] = 'ASC';
						}
					}

					$query = new WP_Query($args);
				?>
				<?php
					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style02.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>
	</section>

	<?php

		echo '<div class="pagination">';
		echo paginate_links(array(
		    'format' => '?pagina=%#%',
		    'current' => $paged,
		    'total' => ceil($query->found_posts / $showposts ),
		    'prev_text' => __('<'),
			'next_text' => __('>'),
		));
		echo '</div>';

	?>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();