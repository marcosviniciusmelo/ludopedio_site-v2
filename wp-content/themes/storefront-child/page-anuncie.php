<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
?>
<main class="anuncie-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo')['titulo'];
				$imagem = get_field('banner_topo')['imagem']['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $title ?></h3>
			</div>
			<?php if( get_field('midia_kit') ): ?>
			<a href="<?php echo get_field('midia_kit') ?>" class="btn" download>BAIXAR MÍDIA KIT</a>
			<?php endif; ?>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<section class="bloco-form">
		<div class="main">
			<div class="title">Preencha o formulário e seja um anunciante do Ludopédio</div>
			<div class="ctnForm"><?php echo do_shortcode('[contact-form-7 id="72568" title="Anuncie"]'); ?></div>
		</div>
	</section>

</main>
<?php
get_footer();