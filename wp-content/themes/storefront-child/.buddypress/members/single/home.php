<?php
//Vornei: 07/07/2020 - [error] 3423#0: *4952949 FastCGI sent in stderr: "PHP message: PHP Notice:  Undefined variable: taxonomy in /usr/share/nginx/html/v2/content/themes/ludopedio/buddypress/members/single/home.php on line 63

if (! isset($taxonomy))
{
    $taxonomy = '';
}
?>

<div id="buddypress">

	<?php do_action( 'bp_before_member_home_content' ); ?>

	<div id="item-header" role="complementary">

		<?php bp_get_template_part( 'members/single/member-header' ) ?>

	</div><!-- #item-header -->

	<div id="item-body" role="main">
		<?php
		if ( bp_is_user_profile() ):
			bp_get_template_part( 'members/single/profile'  );
		else:
			if( !bp_is_user_profile() && !bp_is_user_settings() ):
				/**
				*
				* Sobre
				*
				**/
/**
Vornei: 12/04/2020 - parou de aparecer a visualização do ícone do Instagram para os usuários que destacaram o link.
Removendo esse if

				if( bp_get_profile_field_data('field=Sobre') ): ?>
					<div class="single-post-content">
						<div class="title-section title-section-alt">
							<h2>Sobre</h2>
						</div>
						<?php bp_member_profile_data('field=Sobre'); ?>
					</div>
					<?php
				endif;
**/
                ?>
				<?php
				/**
				*
				* Publicações do membro
				*
				**/

				$displayed_user_id = bp_displayed_user_id();
				$username = bp_core_get_username($displayed_user_id);

				$args = array(
					'post_type' => array( 'qd_arquibancada', 'qd_entrevista', 'qd_biblioteca', 'qd_memoria', 'qd_gallery', 'qd_gallery_museu' ),
					'category_autor' => $username,
					'numberposts' => 8
					);
				$author_posts_data = get_posts( $args );
				if( $author_posts_data ):
					?>
					<div class="ultimos-artigos">
						<div class="title-section title-section-alt">
							<h3>Últimos artigos publicados</h3>
						</div>
						<div class="row">
						<?php
						foreach( $author_posts_data as $post ): setup_postdata( $post );
							$post_id = get_the_ID();
							$post_terms = wp_get_post_terms( $post_id, $taxonomy, $args );
							?>
							<div class="col-xs-6">
								<div class="media media-ultimos prev-post">
									<figure>
										<a class="center-block" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail', array( 'class' => 'media-object img-responsive' ) ); ?></a>
									</figure>
									<div class="media-body">
										<small class="category"><b><?php ludo_the_custom_taxonomies_terms_links(); ?></b></small>
										<h4 itemprop="name" class="media-heading"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
									</div>
								</div>
							</div>
							<?php
						endforeach; ?>
						</div>
						<a href="<?php echo home_url() .'/autores/'.$username; ?>" class="btn btn-primary">Todos os artigos deste autor</a>
					</div>
					<?php
				endif;
			endif;
		endif; ?>
	</div>

	<?php
	$show = false;
	if( $show ):
		?>
		<div id="item-nav">
			<div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
				<ul>

					<?php bp_get_displayed_user_nav(); ?>

					<?php do_action( 'bp_member_options_nav' ); ?>

				</ul>
			</div>
		</div><!-- #item-nav -->

		<div id="item-body" role="main">

			<?php do_action( 'bp_before_member_body' );

			if ( bp_is_user_activity() || !bp_current_component() ) :
				bp_get_template_part( 'members/single/activity' );

			elseif ( bp_is_user_blogs() ) :
				bp_get_template_part( 'members/single/blogs'    );

			elseif ( bp_is_user_friends() ) :
				bp_get_template_part( 'members/single/friends'  );

			elseif ( bp_is_user_groups() ) :
				bp_get_template_part( 'members/single/groups'   );

			elseif ( bp_is_user_messages() ) :
				bp_get_template_part( 'members/single/messages' );

			elseif ( bp_is_user_profile() ) :
				bp_get_template_part( 'members/single/profile'  );

			elseif ( bp_is_user_forums() ) :
				bp_get_template_part( 'members/single/forums'   );

			elseif ( bp_is_user_notifications() ) :
				bp_get_template_part( 'members/single/notifications' );

			elseif ( bp_is_user_settings() ) :
				bp_get_template_part( 'members/single/settings' );

			// If nothing sticks, load a generic template
			else :
				bp_get_template_part( 'members/single/plugins'  );

			endif;

			do_action( 'bp_after_member_body' ); ?>

		</div><!-- #item-body -->
		<?php
	endif; ?>

	<?php do_action( 'bp_after_member_home_content' ); ?>

</div><!-- #buddypress -->
