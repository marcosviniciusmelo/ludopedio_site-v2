<?php do_action( 'bp_before_profile_loop_content' ); ?>

<?php if ( bp_has_profile() ) : ?>

	<?php bp_the_profile_group(); //while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

		<?php if ( bp_profile_group_has_fields() ) : ?>

			<?php do_action( 'bp_before_profile_field_content' ); ?>

			<div class="bp-widget <?php bp_the_profile_group_slug(); ?>">
				<?php
				/**
				*
				* Interesses
				*
				**/

				if( bp_get_profile_field_data('field=Interesses') ):?>
					<div class="ultimos-artigos">
					<div class="title-section title-section-alt">
						<h3>Interesses</h3>
<?php $id = xprofile_get_field_id_from_name( 'Interesses' );
global $field;
$field = xprofile_get_field($id);
$type = $field->type;
//$value = $field->data->value;
$values = bp_unserialize_profile_field( xprofile_get_field_data($field->name, bp_get_member_user_id() ));
$vals = array();
foreach ($values as $value) {
xprofile_set_field_data($field, bp_get_member_user_id(), $value);
remove_filter( 'bp_get_the_profile_field_value', 'wpautop' );
$vals[]=apply_filters( 'bp_get_the_profile_field_value', $value, $type, 2);
}
?>
<p> <?php echo implode(',', $vals);?> </p>

					</div>
					<?php
				endif; ?>

				<?php
				/**
				*
				* Publicações do membro
				*
				**/

				$displayed_user_id = bp_displayed_user_id();
				$user_data = get_userdata( $displayed_user_id );
				$user_name = $user_data->user_login;

				$args = array(
					'post_type' => array('qd_arquibancada','qd_entrevista','qd_gallery','qd_gallery_museu','qd_biblioteca'),
					'category_autor' => $user_name,
					'numberposts' => 8
					);
				$author_posts_data = get_posts( $args );
				if( $author_posts_data ):
					?>
					<div class="ultimos-artigos">
						<div class="title-section title-section-alt">
							<h3>Últimos artigos publicados</h3>
						</div>
						<div class="row">
						<?php
						foreach( $author_posts_data as $post ): setup_postdata( $post ); ?>
							<div class="col-xs-6">
								<div class="media media-ultimos prev-post">
									<figure>
										<a class="center-block" href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail', array( 'class' => 'media-object img-responsive' ) ); ?></a>
									</figure>
									<div class="media-body">
										<small class="category"><b><?php ludo_the_custom_taxonomies_terms_links(); ?></b></small>
										<h5 itemprop="name" class="media-heading"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
									</div>
								</div>
							</div>
							<?php
						endforeach; ?>
						</div>
						<a href="<?php echo home_url() . '/autores/' . $user_name; ?>" class="btn btn-primary">Todos os artigos deste autor</a>
					</div>
					<?php
				endif;
				?>
			</div>

			<?php do_action( 'bp_after_profile_field_content' ); ?>

		<?php endif; ?>

	<?php //endwhile; ?>

	<?php do_action( 'bp_profile_field_buttons' ); ?>

<?php endif; ?>

<?php do_action( 'bp_after_profile_loop_content' ); ?>
