<?php

/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php do_action( 'bp_before_member_header' ); ?>

<?php
/**
*
* Header perfil
*
**/

?>
<div class="single-post-header proflie-header">
		<div class="row">
		<div class="col-xs-3">
			<div id="item-header-avatar item-avatar">
				<a href="<?php bp_displayed_user_link(); ?>">
					<?php bp_displayed_user_avatar( 'type=full&width=180&height=180' ); ?>
				</a>
			</div>
			<div class="col-xs-1 col-lg-12">
                <ul class="social-media comunidade-profile-social-media">
		        <?php if( bp_get_profile_field_data('field=Twitter @') ):?>
					<li class="foundation-twitter-link"><a target="_blank" href="https://twitter.com/<?php bp_member_profile_data('field=Twitter @'); ?>"><i class="icon-twitter"></i></a></li>
		        <?php endif; ?>
		        <?php if( bp_get_profile_field_data('field=facebook.com/') ):?>
					<li class="foundation-facebook-link"><a target="_blank" href="https://www.facebook.com/<?php bp_member_profile_data('field=facebook.com/'); ?>"><i class="icon-facebook"></i></a></li>
		        <?php endif; ?>
                <?php if( bp_get_profile_field_data('field=http://lattes.cnpq.br/') ):?>
                    <li class="foundation-lattes-link">
                    	<a target="_blank" href="<?php bp_member_profile_data('field=http://lattes.cnpq.br/'); ?>">
                    		<img src="<?php echo  get_stylesheet_directory_uri(); ?>/assets/img/logo-lattes.png" width="16" height="16" style="margin-top: -5px;" />
                    	</a>
                    </li>
                <?php endif; ?>
                <?php
                //Vornei: 21/03/2020
                if( bp_get_profile_field_data('field=Instagram@') ):?>
                    <li class="foundation-instagram-link"><a target="_blank" href="https://www.instagram.com/<?php bp_member_profile_data('field=Instagram@'); ?>"><i class="icon-instagram"></i></a></li>
                <?php endif; ?>
				</ul>
			</div>
		</div>

		<div class="col-xs-9">
			<div class="title-section title-section-alt">
				<?php if ( bp_is_active( 'activity' ) && bp_activity_do_mentions() ) : ?>
					<h1 class="single-post-title"><?php the_title(); ?></h2>
				<?php endif; ?>
			</div>

			<span class="activity"><?php bp_last_activity( bp_displayed_user_id() ); ?></span>

			<?php if( bp_get_profile_field_data('field=Cidade') && bp_get_profile_field_data('field=UF') ): ?>
				<p class="local"><?php echo bp_member_profile_data('field=Cidade'); ?> - <?php echo bp_member_profile_data('field=UF'); ?></p>
			<?php endif ?>

			<?php
			if( bp_get_profile_field_data('field=Sobre') ):
				?>
				<h3>Sobre</h3>
				<p class="interesses"><?php bp_member_profile_data('field=Sobre'); ?></p>
			<?php
			endif; ?>

			<div id="item-header-content">
				<div id="item-meta">
					<?php if ( bp_is_active( 'activity' ) ) : ?>
						<div id="latest-update">
							<?php bp_activity_latest_update( bp_displayed_user_id() ); ?>
						</div>
					<?php endif;

					$show = false;
					if( $show ):
						?>
						<div id="item-buttons">
							<?php do_action( 'bp_member_header_actions' ); ?>
						</div><!-- #item-buttons -->
						<?php
					endif;
					/***
					 * If you'd like to show specific profile fields here use:
					 * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
					 */
					 do_action( 'bp_profile_header_meta' );
					 ?>
				</div><!-- #item-meta -->
			</div><!-- #item-header-content -->
		</div>
	</div>
</div>

<?php do_action( 'bp_before_member_header_meta' ); ?>

<?php do_action( 'bp_after_member_header' ); ?>

<?php do_action( 'template_notices' ); ?>
