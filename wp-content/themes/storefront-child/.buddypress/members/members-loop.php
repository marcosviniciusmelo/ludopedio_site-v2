<?php

/**
 * BuddyPress - Members Loop
 *
 * Querystring is set via AJAX in _inc/ajax.php - bp_legacy_theme_object_filter()
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php do_action( 'bp_before_members_loop' ); ?>

<?php if ( bp_has_members( bp_ajax_querystring( 'members' ) . '&per_page=48' ) ) : ?>

	<div id="pag-top" class="pagination">

		<div class="pag-count" id="member-dir-count-top">

			<?php bp_members_pagination_count(); ?>
		</div>

		<div class="pagination-links" id="member-dir-pag-top">

			<?php bp_members_pagination_links(); ?>
		</div>

	</div>

	<?php do_action( 'bp_before_directory_members_list' ); ?>

	<div class="comunidade-list">
		<div class="row">
			<?php
			$count = 1;
			while ( bp_members() ) : bp_the_member(); ?>

				<div class="col-xs-2">
					<div class="media media-comunidade">
						<figure>
							<div class="item-avatar">
								<a href="<?php bp_member_permalink(); ?>"><?php bp_member_avatar('type=full&width=180&height=180'); ?></a>
							</div>
						</figure>
						<div class="media-body">
							<h5 itemprop="name" class="media-heading"><a href="<?php bp_member_permalink(); ?>"><?php bp_member_name(); ?></a></h5>
						</div>
					</div>
					<div class="action">
						<?php do_action( 'bp_directory_members_actions' ); ?>
					</div>
				</div>
			<?php
			if( ( $count % 6 ) == 0 ) echo '<div class="clearfix"></div>'; // Line break
			$count++;
			endwhile; ?>
		</div>
	</div>

	<?php do_action( 'bp_after_directory_members_list' ); ?>

	<?php bp_member_hidden_fields(); ?>

	<div id="pag-bottom" class="pagination">

		<div class="pag-count" id="member-dir-count-bottom">

			<?php bp_members_pagination_count(); ?>

		</div>

		<div class="pagination-links" id="member-dir-pag-bottom">

			<?php bp_members_pagination_links(); ?>

		</div>

	</div>

<?php else: ?>

	<div id="message" class="info">
		<p><?php _e( "Sorry, no members were found.", 'buddypress' ); ?></p>
	</div>

<?php endif; ?>

<?php do_action( 'bp_after_members_loop' ); ?>
