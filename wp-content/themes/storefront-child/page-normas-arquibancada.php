<?php
global $url_tema, $link_blog, $nome_blog; the_post();
get_header();
$page = get_page_by_path('arquibancada');
?>

<main class="single-post-page entrevista">

	<section class="bannerTop entrevista">
		<div class="main">
			<?php
				$imagem = get_field('banner_topo', $page->ID)['imagem'];
			?>
			<div class="title">
				<span class="title">Arquibancada</span>
			</div>
		</div>

		<div class="barBottom">
			<div class="main">
				<div class="left">
					<a href="<?php echo $link_blog.'/arquibancada'; ?>" class="item">Arquivo</a>
					<a href="<?php echo $link_blog.'/normas-arquibancada'; ?>" class="item active">Normas</a>
					<a href="<?php echo $link_blog.'/expediente-arquibancada'; ?>" class="item">Expediente</a>
				</div>
				<div class="issn">ISSN: <?php echo get_field('issn_arquibancada', 'options'); ?></div>
			</div>
		</div>

		<img src="<?php echo $imagem['url']  ?>" alt="" class="bg">
	</section>

	<div class="ctnPostContent">
		<section class="post-content">
			<div class="main">
				<div class="ctnScroll">
					<div class="lineTitlePost">
						<h2><?php the_title() ?></h2>
					</div>
					<div class="contentText">
						<?php the_content() ?>
					</div>
				</div>
			</div>
		</section>

		<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>
		
	</div>

</main>

<?php
get_footer();