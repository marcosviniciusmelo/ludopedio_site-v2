<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );

// Variáveis para todo o tema
$nome_blog = get_bloginfo('name');
$link_blog = get_bloginfo('url');
$url_tema  = get_stylesheet_directory_uri() . '/';


/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}


register_sidebars( 2, array( 'name' => 'sidebar-product' ) );


function admin_style() {
  wp_enqueue_style('admin-styles', get_stylesheet_directory_uri().'/assets/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');


function favicon4admin() {
echo '<link rel="Shortcut Icon" type="image/x-icon" href="' . get_stylesheet_directory_uri() . '/assets/images/favicon.png" />';
}
add_action( 'admin_head', 'favicon4admin' );

/**
*
* Add category_autor taxonomy to post type 'post'
*
**/

/*
* Add new thumnail size
*/
// add_image_size( 'cover-small', 100, 140 );
// add_image_size( 'full-size', 1920 );
// add_image_size( 'box-medium', 670, 550 );
// add_image_size( 'box-small', 690, 260 );



function remove_menu_items() {

    // remove_menu_page( 'index.php' ); //Painel
    // remove_menu_page('edit.php'); // Posts
    remove_menu_page('edit-comments.php'); // Comentarios

}
add_action( 'admin_menu', 'remove_menu_items' );

function sno_register_menus() 
{
    register_nav_menus(
        array(
            'nav-main' => 'Principal',
            'nav-secundario' => 'Secundário',
            'nav-top-left' => 'Top left',
            'nav-footer' => 'Footer',
            'nav-conta' => 'Minha Conta',
        )
    );
}

add_action('init', 'sno_register_menus');


// Função para exibir menu
function sno_show_menu($name_menu) 
{
	$output = wp_nav_menu(array(
		'theme_location' => $name_menu,
		'items_wrap' => '<ul class="listaMenu">%3$s</ul>',
		'echo' => 0,
		'link_before' => '',
		'link_after' => ''
	));
    return $output;
}

// Função para exibir menu
function my_acf_init() {

    // acf_update_setting('google_api_key', 'AIzaSyD98GTZK-eHP4wk0XGwM6E0S1Vf1nUwt3Q');
    
    if( function_exists('acf_add_options_page') ) {
        
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Opções do Tema', 'my_text_domain'),
            'menu_title'    => __('Opções do Tema', 'my_text_domain'),
            'menu_slug'     => 'opcoes',
        ));
    }
}
add_action('acf/init', 'my_acf_init');

include('inc/post-type.php');
include('inc/custom-woo.php');



/*
 * Save date login user
 */
add_action('wp_login','user_last_login', 0, 2);
function user_last_login($login, $user) {  
    $user = get_user_by('login',$login);
    $now = time();
    update_user_meta( $user->ID, 'user_last_login', $now );
}

/*
* Update number cart by ajax
*/
function updateNumberCart(){
    global $woocommerce;
    $count = $woocommerce->cart->cart_contents_count;

    echo $count;
    die();
}
add_action('wp_ajax_updateNumberCart', 'updateNumberCart');
add_action('wp_ajax_nopriv_updateNumberCart', 'updateNumberCart');


add_filter( 'woocommerce_upsell_display_args', 'wc_change_number_related_products', 20 );
function wc_change_number_related_products( $args ) {
    $args['posts_per_page'] = 4;
    $args['columns'] = 4; //change number of upsells here
    return $args;
}



add_filter( 'woocommerce_product_single_add_to_cart_text', 'wc_ninja_change_backorder_button', 10, 2 );
function wc_ninja_change_backorder_button( $text, $product ){
    if ( $product->is_on_backorder( 1 ) ) {
        $text = __( 'Pré-venda', 'woocommerce' );
    }else{
        $text = __( 'Compre agora', 'woocommerce' );
    }
    return $text;
}


add_filter( 'woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text', 10, 2 );
function woocommerce_custom_product_add_to_cart_text( $text, $product ){
    if ( $product->is_on_backorder( 1 ) ) {
        $text = __( 'Pré-venda', 'woocommerce' );
    }else{
        $text = __( 'Compre agora', 'woocommerce' );
    }
    return $text;
}


function ludo_the_author_link( $post_id = false, $echo = true, $link = false ){

    $taxonomy = 'category_autor';

    if( $post_id == false ) $post_id = get_the_ID();
    $post_terms_authors = wp_get_post_terms( $post_id, $taxonomy );
    $authors_count = count( $post_terms_authors );
    $count = 1;

    $author_links_new_order = '';
    $authors_order_meta = get_post_meta( $post_id, 'qd_post_autor_custom_order', true );
    $authors_order = explode( ',', $authors_order_meta );

    $slug = 'autores';
    if( $link == 'autores' )
        $slug = $link;

    if( $authors_order_meta ):

        foreach( $authors_order as $author_order ):

            foreach( $post_terms_authors as $author_term ):

                if( trim($author_order) == $author_term->name ):
                    $author_links_new_order .= '<a href="'. get_bloginfo('url') .'/autor/?p='.$author_term->slug.'">' .  $author_term->name .'</a>';
                    if( $count < $authors_count ) $author_links_new_order .= ', ';
                endif;
            endforeach;

            $count++;
        endforeach;
    else:

        foreach( $post_terms_authors as $author_term ):

            $author_links_new_order .= '<a href="'. get_bloginfo('url') .'/autor/?p='.$author_term->slug.'">' .  $author_term->name .'</a>';
            if( $count < $authors_count ) $author_links_new_order .= ', ';

            $count++;
        endforeach;
    endif;

    if( $echo ):
        echo $author_links_new_order;
    else:
        return $author_links_new_order;
    endif;
}


function ludo_group_author_link( $post_id = false ){

    $taxonomy = 'category_autor';

    if( $post_id == false ) $post_id = get_the_ID();
    $post_terms_authors = wp_get_post_terms( $post_id, $taxonomy );
    $authors_count = count( $post_terms_authors );
    $count = 1;

    $author_links_new_order = '';
    $authors_order_meta = get_post_meta( $post_id, 'qd_post_autor_custom_order', true );
    $authors_order = explode( ',', $authors_order_meta );
    $array = [];
    $grupoArr = [];

    if( $authors_order_meta ):
        foreach( $authors_order as $author_order ):
            foreach( $post_terms_authors as $author_term ):
                if( trim($author_order) == $author_term->name )
                    array_push($array, $author_term->name);
            endforeach;
        endforeach;
    else:
        foreach( $post_terms_authors as $author_term ):
            array_push($array, $author_term->name);
        endforeach;
    endif;

    $titleGrupo = '';
    foreach ($array as $key) {

        if (strpos($key, '’') !== false)
            $key = explode('’', $key)[0];

        $users = new WP_User_Query( array(
            'search'         => '*' . $key . '*',
            'search_columns' => array(
                'user_login',
                'display_name',
            ),
            'number' => 1
        ) );
         
        $getUsers = $users->get_results();
        if( isset($getUsers) ):
            if( isset($getUsers[0]->data->ID) ):
                $ID = $getUsers[0]->data->ID;
                $grupo = get_field('grupo_de_pesquisa', 'user_'.$ID);
                if( isset($grupo) ):
                    if( $grupo ):
                        $titleGrupo = '<a href="'.get_the_permalink($grupo[0]).'">'.get_the_title($grupo[0]).'</a>';
                    endif;
                endif;
            endif;
        endif;
    }

    return $titleGrupo;
}

function getUsersBySurname($surname)
{
    global $wpdb;

    $users = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT ID FROM {$wpdb->prefix}users WHERE display_name = %s",
            $surname),
        ARRAY_A);

    if( $users )
        return $users[0]['ID'];
}

function ludo_the_author_no_link( $post_id = false, $echo = true, $foto = false ){

    $taxonomy = 'category_autor';

    if( $post_id == false ) $post_id = get_the_ID();
    $post_terms_authors = wp_get_post_terms( $post_id, $taxonomy );
    $authors_count = count( $post_terms_authors );
    $count = 1;

    $author_links_new_order = '';
    $authors_order_meta = get_post_meta( $post_id, 'qd_post_autor_custom_order', true );
    $authors_order = explode( ',', $authors_order_meta );

    if( $authors_order_meta ):

        $i=0; 
        foreach( $authors_order as $author_order ):

            foreach( $post_terms_authors as $author_term ):

                if( trim($author_order) == $author_term->name ):
                    $author_links_new_order .= $author_term->name;
                    if( $count < $authors_count )
                        $author_links_new_order .= ', ';

                endif;
            endforeach;

            $count++;
        endforeach;
    else:

        foreach( $post_terms_authors as $author_term ):

            $author_links_new_order .= $author_term->name;
            if( $count < $authors_count )
                $author_links_new_order .= ', ';

            $count++;
        endforeach;
    endif;

    if( $foto == true ):
        $first = explode(',', $author_links_new_order)[0];

        $userID = getUsersBySurname($first);
        
        $foto = get_avatar_url($userID, ['size' => '100' ]);
        $return = '<img src="'.$foto.'">' . $author_links_new_order;
        
    else:
        $return = $author_links_new_order;
    endif;

    if( $echo ):
        echo $return;
    else:
        return $return;
    endif;
}

function ludo_the_author_slug( $post_id = false ){

    $taxonomy = 'category_autor';

    if( $post_id == false ) $post_id = get_the_ID();
    $post_terms_authors = wp_get_post_terms( $post_id, $taxonomy );
    $authors_count = count( $post_terms_authors );
    $count = 1;

    $author_links_new_order = '';
    $authors_order_meta = get_post_meta( $post_id, 'qd_post_autor_custom_order', true );
    $authors_order = explode( ',', $authors_order_meta );

    foreach( $post_terms_authors as $author_term ):
            
        $author_links_new_order = $author_term->slug;

    endforeach;

    return $author_links_new_order;
}


/**
*
* Author box info
*
**/
function ludo_the_author_box(){
    // Se o post for de mais de um autor, não vamos exibir vários boxes, pois ficaria entranho, o box vai ser exibido somente quando o post for somente de um autor
    $post_id = get_the_ID();
    $taxonomy = 'category_autor';
    $post_authors = wp_get_post_terms( $post_id, $taxonomy);

    $post_terms_authors = wp_get_post_terms( $post_id, $taxonomy );
    $authors_count = count( $post_terms_authors );

    $author_links_new_order = '';
    $authors_order_meta = get_post_meta( $post_id, 'qd_post_autor_custom_order', true );
    $authors_order = explode( ',', $authors_order_meta );

    if( $authors_order_meta ):

        foreach( $authors_order as $author_order ):

            /**
            *
            * Ordem customizada
            *
            **/

            foreach( $post_terms_authors as $autor ):
                $author_term_name = $autor->name;
                $author_term_slug = $autor->slug;

                $user = get_user_by( 'login', $author_term_slug );

                // Localizando dados do usuário
                if( !$user ):
                    $username_formated = str_replace( '-', '.', $author_term_slug );
                    $user = get_user_by( 'login', $username_formated );

                    if( !$user ):
                        $username_formated = str_replace( '-', '_', $author_term_slug );
                        $user = get_user_by( 'login', $username_formated );

                        if( !$user ):
                            $username_formated = str_replace( '-', ' ', $author_term_slug );
                            $user = get_user_by( 'login', $username_formated );
                        else:
                            if( $author_term_description ):
                                $user = get_user_by( 'id', $author_term_description );
                            endif;
                        endif;
                    endif;
                endif;

                if( trim($author_order) == $autor->name ):

                    ?>
                    <div class="author-box">
                        <?php
                        if( $user ): // ( prioridade 1 )
                            $post_author_id = $user->ID;
                            $author_name = get_the_author_meta( 'display_name', $post_author_id );
                            $autor_url = home_url() . '/autor/?p=' . $author_term_slug;
                            $author_avatar = get_avatar( $post_author_id, 200 );

                            $userInsta = xprofile_get_field_data('Instagram@', $post_author_id);
                            $userFace = xprofile_get_field_data('facebook.com/', $post_author_id);
                            $userTwitter = xprofile_get_field_data('Twitter @', $post_author_id);

                            $author_community_bio = xprofile_get_field_data('Sobre', $post_author_id);

                            if ($userInsta != '' && strripos($userInsta, 'http') === false)
                                $userInsta = 'https://www.instagram.com/'.$userInsta;
                            if ($userFace != '' && strripos($userFace, 'http') === false)
                                $userFace = 'https://www.facebook.com/'.$userFace;
                            if ($userTwitter != '' && strripos($userTwitter, 'http') === false)
                                $userTwitter = 'https://twitter.com/'.$userTwitter;
                            ?>
                            <figure>
                                <a class="center-block" href="<?php echo $autor_url; ?>"><?php echo $author_avatar; ?></a>
                            </figure>
                            <div class="author-box-content">
                                <h4 class="author-box-name">
                                    <a href="<?php echo $autor_url; ?>"><?php echo $author_name; ?></a>
                                </h4>
                                <?php
                                    echo '<div class="redes">';
                                        if( $userInsta != '' )
                                            echo '<a href="'.$userInsta.'" target="_blank" class="link-instagram"><i class="fab fa-instagram"></i></a>';

                                        if( $userFace != '' )
                                            echo '<a href="'.$userFace.'" target="_blank" class="link-facebook"><i class="fab fa-facebook-f"></i></a>';

                                        if( $userTwitter != '' )
                                            echo '<a href="'.$userTwitter.'" target="_blank" class="link-twitter"><i class="fab fa-twitter"></i></a>';

                                    echo '</div>';
                                ?>
                                <p><?php echo $author_community_bio; ?></p>
                            </div>
                            <?php
                        else: //  ( prioridade 2 )
                            $autor_url = home_url() . '/autor/?p=' . $author_term_slug;
                        ?>
                            <figure>
                                <a class="center-block" href="<?php echo $autor_url; ?>"><?php echo get_avatar( null, 200 ); ?></a>
                            </figure>
                            <div class="author-box-content">
                                <h4 class="author-box-name"><a href="<?php echo $autor_url; ?>"><?php echo $author_term_name ; ?></a></h4>
                            </div>
                            <?php
                        endif; ?>
                    </div><!-- / .author-box -->
                    <?php
                endif;
            endforeach;

        endforeach;


    else:

        /**
        *
        * Ordem alfabetica
        *
        **/

        foreach( $post_authors as $autor ):
            $author_term_name = $autor->name;
            $author_term_slug = $autor->slug;
            $author_term_description = $autor->description;

            $user = get_user_by( 'login', $author_term_slug );

            // Localizando dados do usuário
            if( !$user ):
                $username_formated = str_replace( '-', '.', $author_term_slug );
                $user = get_user_by( 'login', $username_formated );

                if( !$user ):
                    $username_formated = str_replace( '-', '_', $author_term_slug );
                    $user = get_user_by( 'login', $username_formated );

                    if( !$user ):
                        $username_formated = str_replace( '-', ' ', $author_term_slug );
                        $user = get_user_by( 'login', $username_formated );
                    else:
                        if( $author_term_description ):
                            $user = get_user_by( 'id', $author_term_description );
                        endif;
                    endif;
                endif;
            endif;
            ?>
            <div class="author-box">
                <?php
                if( $user ): // ( prioridade 1 )
                    $post_author_id = $user->ID;
                    $author_name = get_the_author_meta( 'display_name', $post_author_id );
                    $autor_url = home_url() . '/autor/?p=' . $author_term_slug;
                    $author_avatar = get_avatar( $post_author_id, 200 );

                    $userInsta = xprofile_get_field_data('Instagram@', $user->ID);
                    $userFace = xprofile_get_field_data('facebook.com/', $user->ID);
                    $userTwitter = xprofile_get_field_data('Twitter @', $user->ID);

                    $author_community_bio = xprofile_get_field_data('Sobre', $post_author_id);

                    if ($userInsta != '' && strripos($userInsta, 'http') === false)
                        $userInsta = 'https://www.instagram.com/'.$userInsta;
                    if ($userFace != '' && strripos($userFace, 'http') === false)
                        $userFace = 'https://www.facebook.com/'.$userFace;
                    if ($userTwitter != '' && strripos($userTwitter, 'http') === false)
                        $userTwitter = 'https://twitter.com/'.$userTwitter;

                ?>
                    <figure>
                            <a class="center-block" href="<?php echo $autor_url; ?>"><?php echo $author_avatar; ?></a>
                    </figure>
                    <div class="author-box-content">
                            <h4 class="author-box-name">
                                <a href="<?php echo $autor_url; ?>"><?php echo $author_name; ?></a>
                                <?php
                                    echo '<div class="redes">';
                                        if( $userInsta != '' )
                                            echo '<a href="'.$userInsta.'" target="_blank" class="link-instagram"><i class="fab fa-instagram"></i></a>';

                                        if( $userFace != '' )
                                            echo '<a href="'.$userFace.'" target="_blank" class="link-facebook"><i class="fab fa-facebook-f"></i></a>';

                                        if( $userTwitter != '' )
                                            echo '<a href="'.$userTwitter.'" target="_blank" class="link-twitter"><i class="fab fa-twitter"></i></a>';

                                    echo '</div>';
                                ?>
                            </h4>
                           <?php echo $author_community_bio; ?>
                    </div>
                    <?php
                else: //  ( prioridade 2 ) ?>

                    <figure>
                        <?php
                            $author_avatar = get_avatar( 0, 200 );
                            $autor_url = home_url() . '/autor/?p=' . $author_term_slug;
                        ?>
                        <a class="center-block" href="<?php echo $autor_url; ?>"><?php echo $author_avatar; ?></a>
                    </figure>
                    <div class="author-box-content">
                        <h4 class="author-box-name">
                            <a href="<?php echo $autor_url; ?>"><?php echo $author_term_name ; ?></a>
                        </h4>
                    </div>
                    <?php
                endif; ?>
            </div><!-- / .author-box -->
            <?php
        endforeach;
    endif;
}


/*
* Get content plano
*/
function contentPlano(){
    $id = $_POST['id'];
    $product = wc_get_product( $id );

    $return = '<h4>'.get_the_title($id).'</h4>';
    $return .= '<div class="text">'. apply_filters( 'woocommerce_short_description', $product->get_description() ) .'<div>';

    if( $_POST['mode'] == 'apoie' )
        $return .= '<a href="?add-to-cart='.$id.'" data-quantity="1" class=" button product_type_simple add_to_cart_button ajax_add_to_cart btn" data-product_id="'.$id.'" rel="nofollow">APOIAR COM R$'.number_format($product->get_price(), 2, ',', ' ').' AGORA!</a>';

    echo $return;

    die();
}
add_action('wp_ajax_contentPlano', 'contentPlano');
add_action('wp_ajax_nopriv_contentPlano', 'contentPlano');


/*
 * Get category post
 */
function getCategoryPost($post_id, $format = "li"){

    switch ( get_post_type($post_id) ) {
        case 'qd_arquibancada':
            $category = 'category_arquibancada';
            break;

        case 'qd_entrevista':
            $category = 'category_entrevista';
            break;

        case 'qd_gallery_museu':
            $category = 'category_gallery_museu';
            break;

        case 'qd_gallery':
            $category = 'category_gallery';
            break;

        case 'qd_memoria':
            $category = 'category_memoria';
            break;

        case 'qd_biblioteca':
            $category = 'category_biblioteca';
            break;

        case 'qep_type_event':
            return;
            break;
        
        default:
            $category = 'category';
            break;
    }

    $ic=0;
    $arr = get_the_terms($post_id, $category);
    if( $arr ):
        foreach ( get_the_terms($post_id, $category) as $key):
            if( $ic > 0 ) continue;

            if( $format == "li" ){
                $return = '<li><a style="background-color: '.get_field('cor', $category.'_'.$key->term_id).';" href="'.get_term_link($key->slug, $category).'">'.$key->name.'</a></li>';
            }else{
                $return = '<a style="color: '.get_field('cor', $category.'_'.$key->term_id).';" href="'.get_term_link($key->slug, $category).'">'.$key->name.'</a>';
            }
            
            $ic++;
        endforeach;

        return $return;

    else:
        return '';
    endif;
}


/*
 * 
 */
 function getAllTypeByAuthor($autor){

    $types = [
        'post' => 'Ludosfera',
        'qd_arquibancada' => 'Ludosfera',
        'qd_gallery_museu' => 'Museu',
        'qd_gallery' => 'Futebol Arte',
        'qep_type_event' => 'Eventos',
        'qd_biblioteca' => 'Biblioteca',
        'qd_entrevista' => 'Entrevista'
    ];
    $has = [];

    foreach($types as $key => $value):

        $args = array(
            'post_type'      => $key,
            'post_status'    => 'publish',
            'posts_per_page' => 1,
            'category_autor' => $autor
        );
        $query = new WP_Query($args);

        if( $query->have_posts() )
            array_push($has, $key);
        wp_reset_postdata();

    endforeach;

    return $has;

 }



add_action( 'admin_menu', 'linked_url' );
function linked_url() {
    add_menu_page( 'linked_url', 'Em campo', 'read', 'my_slug', '', 'dashicons-text', 1 );
}

add_action( 'admin_menu' , 'linkedurl_function' );
function linkedurl_function() {
    global $menu;
    $menu[1][2] = get_bloginfo('url')."/wp-admin/edit.php?s&post_status=all&post_type=post&action=-1&m=0&cat=1941";
}


add_action('restrict_manage_posts', 'product_tags_sorting');
function product_tags_sorting() {
    global $typenow;

    $taxonomy  = 'post_tag';

    if ( $typenow == 'post' ) {


        $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);

        wp_dropdown_categories(array(
            'show_option_all' => __("Todas as {$info_taxonomy->label}"),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ));
    };
}

add_action('parse_query', 'product_tags_sorting_query');
function product_tags_sorting_query($query) {
    global $pagenow;

    $taxonomy  = 'post_tag';

    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == 'post' && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}



add_action('restrict_manage_posts', 'cats_sorting');
function cats_sorting() {
    global $typenow;

    $array = array(
        'category_entrevista' => 'qd_entrevista',
        'category_arquibancada' => 'qd_arquibancada',
        'category_gallery_museu' => 'qd_gallery_museu',
        'category_gallery' => 'qd_gallery',
        'category_memoria' => 'qd_memoria',
        'category_biblioteca' => 'qd_biblioteca'
    );

    foreach ($array as $key => $value) {
        $taxonomy  = $key;

        if ( $typenow == $value ) {

            $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
            $info_taxonomy = get_taxonomy($taxonomy);

            wp_dropdown_categories(array(
                'show_option_all' => __("Todas as Categorias"),
                'taxonomy'        => $taxonomy,
                'name'            => $taxonomy,
                'orderby'         => 'name',
                'selected'        => $selected,
                'show_count'      => true,
                'hide_empty'      => true,
            ));
        };
    }
    
}

add_action('parse_query', 'cats_sorting_query');
function cats_sorting_query($query) {
    global $pagenow;

    $array = array(
        'category_entrevista' => 'qd_entrevista',
        'category_arquibancada' => 'qd_arquibancada',
        'category_gallery_museu' => 'qd_gallery_museu',
        'category_gallery' => 'qd_gallery',
        'category_memoria' => 'qd_memoria',
        'category_biblioteca' => 'qd_biblioteca'
    );

    foreach ($array as $key => $value):

        $taxonomy  = $key;

        $q_vars    = &$query->query_vars;
        if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $value && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
            $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
            $q_vars[$taxonomy] = $term->slug;
        }

    endforeach;
}


/**
*
* Creative Commons post icons
*
**/
function ludo_post_creative_commons_icons( $post_id = null ){
    global $post;
    if( !isset($post_id) ) $post_id = get_the_ID();
    $licence_post = get_post_meta( $post_id, 'qd_biblioteca_cc', true );
    $licence_attachment = get_post_meta( $post_id, 'ludo_media_photo_licence', true );
    if( $licence_post ):
            $licence = get_post_meta( $post_id, 'qd_biblioteca_cc', true );
    endif;
    if( $licence_attachment ):
            $licence = get_post_meta( $post_id, 'ludo_media_photo_licence', true );
    endif;
    if( isset( $licence ) ):
            echo '<div class="creative-commons">';
            switch( $licence ) {
                    case 'cc-pd':
                            echo '<!-- Attribution  --><div class="cc-pd"><a target="_blank" href="http://creativecommons.org/about/pdm"><span class="iconcc-cc-pd"></span></a></div>';
                            break;
                    case 'cc-c':
                            echo '<!-- Attribution  --><div class="cc-pd"><a target="_blank" href="http://pt.wikipedia.org/wiki/Direito_autoral"><span class="icon-cc-c-sprite"></span></a></div>';
                            break;
                    case 'cc-by':
                            echo '<!-- Attribution  --><div class="cc-by"><a target="_blank" href="http://creativecommons.org/licenses/by/4.0"><span class="iconcc-cc"></span><span class="iconcc-cc-by"></span></a></div>';
                            break;
                    case 'cc-by-nd':
                            echo '<!-- Attribution-NoDerivs  --><div class="cc-by-nd"><a target="_blank" href="http://creativecommons.org/licenses/by-nd/4.0"><span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nd"></span></a></div>';
                            break;
                    case 'cc-by-nc-sa':
                            echo '<!-- Attribution-NonCommercial-ShareAlike --><div class="cc-by-nc-sa"><a target="_blank" href="http://creativecommons.org/licenses/by-nc-sa/4.0"><span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nc"></span><span class="iconcc-cc-sa"></span></a></div>';
                            break;
                    case 'cc-by-sa':
                            echo '<!-- Attribution-ShareAlike --><div class="cc-by-sa"><a target="_blank" href="http://creativecommons.org/licenses/by-sa/4.0"><span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-sa"></span></a></div>';
                            break;
                    case 'cc-by-nc':
                            echo '<!-- Attribution-NonCommercial --><div class="cc-by-nc"><a target="_blank" href="http://creativecommons.org/licenses/by-nc/4.0"><span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nc"></span></a></div>';
                            break;
                    case 'cc-by-nc-nd':
                            echo '<!-- Attribution-NonCommercial-NoDerivs --><div class="cc-by-nc-nd"><a target="_blank" href="http://creativecommons.org/licenses/by-nc-nd/4.0"><span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nc"></span><span class="iconcc-cc-nd"></span></a></div>';
                            break;
                    case 'cc-pd':
                            echo '<!-- Public-Domain --><div class="cc-by-pd"><a target="_blank" href="https://wiki.creativecommons.org/Public_domain"><span class="iconcc-cc-pd"></span></a></div>';
                            break;
                    case 'cc-co':
                            echo '<!-- Coprights --><div class="cc-by-pd"><a target="_blank" href="http://en.wikipedia.org/wiki/Copyright"><span class="icon-cc-c-sprite"></span></a></div>';
                            break;
            }
            echo '</div>';
    endif;
}


function hide_siteadmin() {  

    global $menu; // Global to get menu array
    // $menu[3][1] = 'Ludosfera'; // Change name of posts to Ludosfera

    if (current_user_can('contributor')) {
        remove_menu_page( 'index.php' ); // Dashboard + submenus
        remove_menu_page( 'wpcf7' ); //Contato
        remove_menu_page( 'opcoes' ); //Opções
        remove_menu_page( 'tools.php' ); //Tools
    }
}
add_action('admin_head', 'hide_siteadmin');


function preparaLinks($link){
    if (strripos($link, 'ludopedio') === false && strripos($link, 'uploads') === false) {
        return $link;
    }else{
        $link = explode('/uploads', $link);
        return get_bloginfo('url').'/wp-content/uploads'.$link[1];
    }
}


function remove_columns( $columns ) {
    unset($columns['author']);
    return $columns;
}

function remove_column_init() {
    add_filter( 'manage_posts_columns' , 'remove_columns' );
}
add_action( 'admin_init' , 'remove_column_init' );



add_filter('manage_qd_arquibancada_posts_columns', 'my_columns');
function my_columns($columns) {
    $columns['numero'] = 'Número';
    return $columns;
}

add_action('manage_qd_arquibancada_posts_custom_column',  'my_show_columns');
function my_show_columns($name) {
    global $post;
    switch ($name) {
        case 'numero':
            $views = get_post_meta($post->ID, 'qd_article_number', true);
            echo $views;
    }
}


add_filter('manage_qd_entrevista_posts_columns', 'my_columns_en');
function my_columns_en($columns) {
    $columns['numero'] = 'Número';
    return $columns;
}

add_action('manage_qd_entrevista_posts_custom_column',  'my_show_columns_en');
function my_show_columns_en($name) {
    global $post;
    switch ($name) {
        case 'numero':
            $views = get_post_meta($post->ID, 'qd_article_number', true);
            echo $views;
    }
}



add_action( 'quick_edit_custom_box', 'ws365150_custom_edit_box_pt', 10, 3 );
function ws365150_custom_edit_box_pt( $column_name, $post_type, $taxonomy ) {
    global $post;

    switch ( $post_type ) {
        case 'post':
        case 'qd_arquibancada':

        if( $column_name === 'qd_article_number' ): // same column title as defined in previous step
        ?>
                <?php // echo get_post_meta( $post->ID, 'remark', true ); ?>
            <fieldset class="inline-edit-col-right" id="#edit-">
                <div class="inline-edit-col">
                    <label>
                        <span class="title">Summary</span>
                        <span class="input-text-wrap"><input type="text" name="summary" class="inline-edit-menu-order-input" value=""></span>
                    </label>
                </div>
            </fieldset>
            <?php
        endif;
            // echo 'custom page field';
            break;

        default:
            break;
    }
}


function remove_dashboard_widgets() {
    global $wp_meta_boxes;
  
    unset($wp_meta_boxes['dashboard']);
  
}

if (!current_user_can('manage_options')) {
    add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
}





function advance_search_where($where){
    global $wpdb;
    if (is_search())
        $where .= "OR (t.name LIKE '%".get_search_query()."%' AND {$wpdb->posts}.post_status = 'publish')";
    return $where;
}




function ludo_new_term_for_new_user( $user_id ){
    // Register user term for control of users posts list
    $new_user_data = get_userdata( $user_id );
    $new_user_login = $new_user_data->user_login;
    $new_user_first_name = $new_user_data->user_firstname;
    $new_user_last_name = $new_user_data->user_lastname;
    $new_user_term_name = $new_user_first_name . ' ' . $new_user_last_name;

    if( !term_exists( $new_user_login, 'category_autor' ) ):
        wp_insert_term(
            $new_user_term_name, // the term name
            'category_autor', // the taxonomy
            array( 'slug' => $new_user_login, 'description' => $user_id ) // the term slug
        );
    endif;
}
add_action( 'user_register', 'ludo_new_term_for_new_user', 10, 1 );
add_action( 'bp_core_activated_user', 'ludo_new_term_for_new_user', 10, 5 );
add_action( 'profile_update', 'ludo_new_term_for_new_user', 10, 1 );




class WP_Query_Taxonomy_Search {
    public function __construct() {
        add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ) );
    }

    public function pre_get_posts( $q ) {
        if ( is_admin() ) return;

        $wp_query_search_tax_query = filter_var( 
            $q->get( 'search_tax_query' ), 
            FILTER_VALIDATE_BOOLEAN 
        );

        // WP_Query has 'tax_query', 's' and custom 'search_tax_query' argument passed
        if ( $wp_query_search_tax_query && $q->get( 'tax_query' ) && $q->get( 's' ) ) {
            add_filter( 'posts_groupby', array( $this, 'posts_groupby' ), 10, 1 );
        }
    }

    public function posts_groupby( $groupby ) {
        return '';
    }
}

new WP_Query_Taxonomy_Search();


function suedsicht_theme_add_editor_assets() {
  wp_enqueue_script('suedsicht-admin-js', get_stylesheet_directory_uri() . '/assets/js/admin.js?'.rand(000,999));
}
add_action( 'admin_enqueue_scripts', 'suedsicht_theme_add_editor_assets' );




add_filter('user_contactmethods', 'yoast_seo_admin_user_remove_social', 99);

function yoast_seo_admin_user_remove_social ( $contactmethods ) {
    unset( $contactmethods['facebook'] );
    unset( $contactmethods['instagram'] );
    unset( $contactmethods['linkedin'] );
    unset( $contactmethods['myspace'] );
    unset( $contactmethods['pinterest'] );
    unset( $contactmethods['soundcloud'] );
    unset( $contactmethods['tumblr'] );
    unset( $contactmethods['twitter'] );
    unset( $contactmethods['youtube'] );
    unset( $contactmethods['wikipedia'] );
    unset( $contactmethods['description'] );
    return $contactmethods;
}



/*
* Change logo on wp-admin
*/