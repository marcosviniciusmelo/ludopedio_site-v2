<?php
global $url_tema, $link_blog, $nome_blog; the_post();
get_header();
$page = get_page_by_path('entrevistas');
?>

<main class="single-post-page entrevista">

	<section class="bannerTop entrevista">
		<div class="main">
			<?php
				$imagem = get_field('banner_topo', $page->ID)['imagem'];
			?>
			<div class="title">
				<span class="title">Entrevistas</span>
			</div>
		</div>

		<div class="barBottom">
			<div class="main">
				<div class="left">
					<a href="<?php echo $link_blog.'/entrevistas'; ?>" class="item">Arquivo</a>
					<a href="<?php echo $link_blog.'/normas-entrevistas'; ?>" class="item">Normas</a>
					<a href="<?php echo $link_blog.'/expediente-entrevistas'; ?>" class="item active">Expediente</a>
				</div>
				<div class="issn">ISSN: <?php echo get_field('issn_entrevistas', 'options'); ?></div>
			</div>
		</div>

		<img src="<?php echo $imagem['url']  ?>" alt="" class="bg">
	</section>

	<div class="ctnPostContent no-sidebar">
		<section class="post-content">
			<div class="main">
				<div class="ctnScroll">
					<div class="lineTitlePost">
						<h2><?php the_title() ?></h2>
					</div>
					<div class="contentText">
						<?php the_content() ?>
					</div>

					<ul class="expedienteLista">
						<?php
							foreach (get_field('listas') as $key):
								echo '<li class="_item">
										<h3>'.$key['titulo'].'</h3>
										<div class="grid">';
										foreach ($key['membros'] as $membro):
											$user_info = get_userdata($membro['membro']);
											$foto = get_avatar_url($user_info->ID, ['size' => '200' ]);
											$link = $link_blog.'/author/'.$user_info->user_nicename;
											echo '<div class="item">
													<a href="'.$link.'"><figure><img src="'.$foto.'"></figure></a>
													<a href="'.$link.'"><h5>'. $user_info->display_name .'</h5></a>
													<span>'. $membro['legenda'] .'</span>
												  </div>';
										endforeach;
									echo '</div>
									</li>';
							endforeach;

							echo '<li class="_item">
									<h3>Email de contato</h3>
									<p>ludopedio@ludopedio.com.br</p>
								</li>';
						?>
					</ul>	
				</div>
			</div>
		</section>

		<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>
		
	</div>

</main>

<?php
get_footer();