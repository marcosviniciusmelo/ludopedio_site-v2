<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
?>
<main class="editora-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('titulo_bloco01');
				$subtitulo = get_field('subtitulo_bloco01');
				$texto = get_field('texto_banner_topo');
				$imagem = get_field('background_bloco01');
			?>
			<div class="title">
				<span class="title"><?php echo $title; ?></span>
				<h3 class="subtitle"><?php echo $subtitulo ?></h3>
				<div class="text"><?php echo $texto ?></div>
			</div>
		</div>

		<img src="<?php echo $imagem['url']  ?>" alt="" class="bg">
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="section02">
		<div class="main">
			<div class="left">
				<h3>Por que criamos uma editora?</h3>
				<div class="texto"><?php echo get_field('texto_bloco02') ?></div>
			</div>
			<div class="right"><img src="<?php echo get_field('imagem_bloco02')['sizes']['large'] ?>" alt=""></div>
		</div>
	</section>

	<section class="bloco-posts loja">
		<div class="main">
			<div class="title">
				<h3>Loja</h3>
				<a href="#" class="btn-ver">Conheça a loja  ></a>
			</div>

			<?php echo do_shortcode('[recent_products limit=4 category="apoie, cursos" cat_operator="NOT IN"]') ?>
		</div>	
	</section>

	<section class="section03">
		<div class="main">
			<h3>Sobre a editora</h3>
			<div class="texto"><?php echo get_field('texto_bloco03') ?></div>
		</div>
		<img src="<?php echo get_field('imagem_bloco03')['url'] ?>" class="bg">
	</section>

	<section class="section04">
		<div class="main">
			<div class="left"><img src="<?php echo get_field('imagem_bloco04')['sizes']['large'] ?>" alt=""></div>
			<div class="right">
				<h3><?php echo get_field('titulo_bloco04') ?></h3>
				<div class="texto"><?php echo get_field('texto_bloco04') ?></div>
			</div>
		</div>
	</section>

	<section class="section05">
		<div class="main">
			<div class="titleCtn">Entre em contato</div>
			<div class="ctnForm"><?php echo do_shortcode('[contact-form-7 id="76172" title="Formulário de contato - Editora"]') ?></div>
		</div>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();