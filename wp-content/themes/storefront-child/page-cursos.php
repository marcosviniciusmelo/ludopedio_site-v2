<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
?>
<main class="apoie-page cursos">

	<section class="bannerTop">
		<div class="main">
			<div class="left">
				<h3><?php the_field('titulo_bloco01') ?></h3>
				<h2><?php echo get_field('subtitulo_bloco01') ?></h2>
				<div class="text"><?php echo get_field('texto_bloco01') ?></div>
			</div>
		</div>

		<img src="<?php echo get_field('background_bloco01')['url'] ?>" alt="" class="bg">
	</section>


	<section class="section01">
		<div class="main">

			<div class="titleCtn">Cursos</div>

			<?php

				$args = array(
					'post_type'         => 'product',
					'post_status'       => 'publish',
					'posts_per_page'    => 60,
					'tax_query' => array(
				        array(
				            'taxonomy' => 'product_cat',
				            'field'    => 'slug',
				            'terms'    => array('cursos'),
				        ),
				    ),
				);
				$query = new WP_Query($args);

			?>

			<div class="conteudoPlano active">
				<ul class="grid">
					<?php 
						while ($query->have_posts()) : $query->the_post();
						$product = wc_get_product( $post->ID );
					
							echo '<li class="item">
								<div class="toolTip" data-id="'.$post->ID.'" data-mode="curso"></div>
								<h4>'.get_the_title().'</h4>
								<div class="price">
									<span>R$</span>
									<h5>'.number_format($product->get_price(), 2, ',', ' ').'</h5>
								</div>
								<a href="?add-to-cart='.$post->ID.'" data-quantity="1" class=" button product_type_simple add_to_cart_button ajax_add_to_cart btn" data-product_id="'.$post->ID.'" rel="nofollow">Comprar</a>
							</li>';

						endwhile;
						wp_reset_postdata();
					?>
				</ul>
			</div>

		</div>
	</section>

	<section class="section02">
		<div class="main">
			<div class="titleCtn"><?php the_field('titulo_bloco02') ?></div>
			<div class="text">
				<?php echo get_field('texto_bloco02') ?>
			</div>
		</div>
	</section>


	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<div class="modalPlano">
	<div class="close"><i class="fas fa-times"></i></div>
	<div class="content"></div>
</div>
<div class="overflowPlano"></div>

<?php
get_footer();