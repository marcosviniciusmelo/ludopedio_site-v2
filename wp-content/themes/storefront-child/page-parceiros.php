<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 10;
$offset = ($paged - 1) * $showposts;
?>
<main class="parceiros-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo')['titulo'];
				$imagem = get_field('banner_topo')['imagem']['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $title ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/parceiros">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<div class="main">
		<ul class="grid">
			<?php
				$args = array(
				'post_type'      => 'qd_partner',
				'post_status'    => 'publish',
				'posts_per_page' => $showposts,
				'offset'		 => $offset
			);

			if( isset($_GET['busca']) ){
				$args['s'] = $_GET['busca'];
			}

			if( isset($_GET['ordem']) ){
				if ( $_GET['ordem'] == 'cadastro' ) {
					$args['orderby'] = 'date';
					$args['order'] = 'DESC';
				}else{
					$args['orderby'] = 'title';
					$args['order'] = 'ASC';
				}
			}

			$query = new WP_Query($args);

			while ($query->have_posts()) : $query->the_post();
				$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
			
				echo '<li class="_item">
						<a href="'.get_field('link').'" target="_blank"><figure><img src="'.$urlThumb.'"></figure></a>
						<a href="'.get_field('link').'" target="_blank"><h4>'.get_the_title().'</h4></a>
						<div class="text">'.get_the_content().'</div>
					</li>';

			endwhile;
			wp_reset_postdata();
			?>
		</ul>

		<?php
			echo '<div class="pagination">';
			echo paginate_links(array(
			    'format' => '?pagina=%#%',
			    'current' => $paged,
			    'total' => ceil($query->found_posts / $showposts ),
			    'prev_text' => __('<'),
				'next_text' => __('>'),
			));
			echo '</div>';
		?>
	</div>

</main>
<?php
get_footer();