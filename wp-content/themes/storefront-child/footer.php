<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */
?>
<?php global $url_tema, $nome_blog, $link_blog; ?>
<?php if( !is_front_page() && !is_page('contato') ): ?>
		</div><!-- .col-full -->
	</div><!-- #content -->
<?php endif; ?>

	<?php do_action( 'storefront_before_footer' ); ?>


	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<div class="lineTopFixed"></div>

<div class="modalBusca">
	<div class="topo">
		<div class="main">
			<a href="<?php echo $link_blog ?>"><img src="<?php echo $url_tema ?>assets/images/brand.png"></a>
			<div class="close"><img src="<?php echo $url_tema ?>assets/images/icones/close.png"></div>
		</div>
	</div>
	<div class="bottom">
		<div class="main">
			<form action="<?php echo $link_blog ?>" method="GET">
				<input type="text" name="s" placeholder="Faça sua pesquisa">
				<button class="btn btn-bordered" type="submit">Buscar</button>
			</form>
		</div>
	</div>
</div>
<div class="overModalBusca"></div>

<sectio class="newsletter" <?php if(is_page_template('poroutrofutebol.php')) echo 'style="background-color: '.get_field('cor_base').'"'; ?>>
	<div class="main">
		<div class="title" <?php if(is_page_template('poroutrofutebol.php')){ if( get_field('cor_base') == '#000000' ){ echo 'style="color: #fff"'; } } ?>>Cadastre-se para receber novidades</div>
		<?php //echo do_shortcode('[contact-form-7 id="76599" title="Newsletter"]') ?>
		<form action="" id="formNewsletter">
			<div class="field"><input type="email" name="email" placeholder="Seu Email" required></div>
			<div class="field"><input type="text" name="time" placeholder="Torce para qual clube?" required></div>
			<button type="submit" class="btn btn-black" <?php if(is_page_template('poroutrofutebol.php')){ if( get_field('cor_base') == '#000000' ){ echo 'style="background-color: #ccc; color: #000"'; } } ?>>ENVIAR</button>
		</form>

		<!-- Begin Mailchimp Signup Form -->
		<div id="mc_embed_signup" style="display: none;">
			<form action="https://ludopedio.us10.list-manage.com/subscribe/post?u=c2bd2bf2843e4a83c7bb89efd&amp;id=1b1bf7661e" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			    <div id="mc_embed_signup_scroll">
				
			<div class="mc-field-group">
				<label for="mce-EMAIL">Email </label>
				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
			</div>
			<div class="mc-field-group">
				<label for="mce-MMERGE5">Torce para qual clube? </label>
				<input type="text" value="" name="MMERGE5" class="required" id="mce-MMERGE5">
			</div>
				<div id="mce-responses" class="clear">
					<div class="response" id="mce-error-response" style="display:none"></div>
					<div class="response" id="mce-success-response" style="display:none"></div>
				</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c2bd2bf2843e4a83c7bb89efd_1b1bf7661e" tabindex="-1" value=""></div>
			    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
			    </div>
			</form>
		</div>

		<!--End mc_embed_signup-->
	</div>
</sectio>

<footer>
	<div class="lineTop">
		<div class="main">
			<div class="left">
				<div class="brand"><a href="<?php echo $link_blog ?>"><img src="<?php echo $url_tema ?>assets/images/brand.png"></a></div>
				<div class="substantivo">
					<p><strong>Substantivo Masculino</strong></p>
					<p>01. Futebol</p>
					<p>02. Jogo que se joga com os pés</p>
				</div>
			</div>
			<div class="right">
				<p><strong>Siga o Ludopédio nas redes socias:</strong></p>
				<div class="redes">
					<a href="https://www.youtube.com/c/Ludop%C3%A9dio?sub_confirmation=1" target="_blank"><i class="fab fa-youtube"></i></a>
					<a href="https://twitter.com/ludopedio" target="_blank"><i class="fab fa-twitter"></i></a>
					<a href="https://www.instagram.com/ludopedio/" target="_blank"><i class="fab fa-instagram"></i></a>
					<a href="https://www.facebook.com/ludopediofutebol" target="_blank"><i class="fab fa-facebook"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="lineMiddle">
		<div class="main">
			<?php echo sno_show_menu('nav-footer') ?>
		</div>
	</div>
	<div class="lineDown">
		<div class="main">
			<div class="left">
				2009 - <?php echo date('Y') ?> | Ludopédio - O maior portal de produção e divulgação científica sobre futebol da América Latina.
			</div>
			<div class="selo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icones/selossl.png"></div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<?php if ( !is_checkout() && !is_page('registrar') ): ?>
	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> -->
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

	<script src="https://code.jquery.com/jquery-1.8.2.js"></script>
	<script src="https://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

<?php endif; ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/slick.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/functions.js?<?php echo rand(000,9999) ?>"></script>

</body>
</html>
