<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$cate = get_queried_object();

$paged = 1;
if( isset($_GET['page']) )
	$paged = $_GET['page'];
$showposts = 20;
$offset = ($paged - 1) * $showposts;

?>
<main class="grupo-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$imagem = get_field('background_topo', 'category_'.$cate->term_id)['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $cate->name ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog . '/categoria/' . $cate->slug ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="gridPost col-5">
		<div class="main">
			<ul class="grid">
			<?php

				$args = array(
					'post_type'      => 'post',
					'post_status'    => 'publish',
					'category_name' => $cate->slug,
					'posts_per_page' => $showposts,
					'offset'  		 => $offset,
				);

				if( isset($_GET['busca']) ){
					$args['s'] = $_GET['busca'];
				}

				if( isset($_GET['ordem']) ){
					if ( $_GET['ordem'] == 'cadastro' ) {
						$args['orderby'] = 'date';
						$args['order'] = 'DESC';
					}else{
						$args['orderby'] = 'title';
						$args['order'] = 'ASC';
					}
				}
				$query = new WP_Query($args);

				while ($query->have_posts()) : $query->the_post();
					$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
					echo '<li class="pd-20">
							<a href="'.get_the_permalink().'">
								<img src="'.$urlThumb.'" class="no-cut">
								<h4>'.get_the_title().'</h4>
							</a>
						</li>';
				endwhile;
			?>
			</ul>
			<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>
		</div>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/pagination.php'); ?>

</main>

<?php
get_footer();