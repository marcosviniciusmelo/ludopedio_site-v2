<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$page = get_page_by_path('futebol-arte');
$termC = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

$paged = 1;
if( isset($_GET['page']) )
	$paged = $_GET['page'];
$showposts = 12;
$offset = ($paged - 1) * $showposts;

$terms = get_terms([
    'taxonomy' => 'category_gallery',
    'hide_empty' => false,
]);

?>
<main class="museu-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo', $page->ID)['titulo'];
				$imagem = get_field('banner_topo', $page->ID)['imagem']['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $title ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/museu-galeria">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>

					<div class="linkDropdown">
						<span>Álbuns <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug,'category_gallery').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
					
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>
	
	<?php
		$args = array(
			'post_type'              => 'qd_gallery',
			'post_status'      		 => 'publish',
			'posts_per_page'    	 => 1,
			'category_gallery' => $termC->slug
		);
		$query = new WP_Query($args);

		$terms = get_the_terms($post->ID, 'category_gallery');
		$urlThumb = get_the_post_thumbnail_url($post->ID, 'large');
	?>
	<section class="contentMain">
		<div class="main">
			<div class="top">
				<span><?php echo $terms[0]->name ?></span>
				<h2><?php the_title() ?></h2>
			</div>
			<div class="slider">
				<?php next_post_link( '%link', '<i class="fas fa-chevron-left"></i>', TRUE, '', 'category_gallery') ?>
				<?php previous_post_link( '%link', '<i class="fas fa-chevron-right"></i>', TRUE, '', 'category_gallery') ?>
				<figure><img src="<?php echo $urlThumb ?>" alt=""></figure>
			</div>
			<div class="bottom">
				<div class="left">
					<?php
						switch (get_field('ludo_media_photo_licence', $post->ID)) {
							case 'cc-by':
								$icone = '<span class="iconcc-cc"></span><span class="iconcc-cc-by"></span>';
								break;
							case 'cc-by-nd':
								$icone = '<span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nd"></span>';
								break;
							case 'cc-by-nc-sa':
								$icone = '<span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nc"></span><span class="iconcc-cc-sa"></span>';
								break;
							case 'cc-by-sa':
								$icone = '<span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-sa"></span>';
								break;
							case 'cc-by-nc':
								$icone = '<span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nc"></span>';
								break;
							case 'cc-by-nc-nd':
								$icone = '<span class="iconcc-cc"></span><span class="iconcc-cc-by"></span><span class="iconcc-cc-nc"></span><span class="iconcc-cc-nd"></span>';
								break;
							case 'cc-pd':
								$icone = '<span class="iconcc-cc-pd"></span>';
								break;
							case 'cc-co':
								$icone = '<span class="icon-cc-c-sprite"></span>';
								break;

							default:
								$icone = '';
								break;
						}
						echo $icone;
					?>
					<?php if( get_field('ludo_media_photographer_url', $post->ID) != '' ): ?>
					<a class="linkExternal" href="<?php echo get_field('ludo_media_photographer_url', $post->ID); ?>" target="_blank"><i class="fas fa-external-link-alt"></i></a>
					<?php endif; ?>

					<div class="autor">
						Fotógrafo: <?php ludo_the_author_link( $post->ID ); ?>
					</div>
				</div>
				<div class="share">
					<span>Compartilhe:</span>
					<div class="redes">
						<?php
							$url = urlencode( get_the_permalink() );
							$title = strip_tags( get_the_title() );
						?>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>&t=<?php echo $title; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fab fa-facebook-f"></i></a>
						<a href="https://twitter.com/share?url=<?php echo $url; ?>&text=<?php echo $title; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fab fa-twitter"></i></a>
						<a data-network="whatsapp" data-url="<?php echo get_the_permalink() ?>" class="st-custom-button"><i class="fab fa-whatsapp"></i></a>
						<a href="https://telegram.me/share/url?url=<?php echo get_the_permalink() ?>" target="_blank"><i class="fab fa-telegram-plane"></i></a>
					</div>
				</div>
			</div>
			<div class="content">
				<h4><div class="totalViews"><?php //echo do_shortcode('[post-views id="'.$post->ID.'"]') ?></div></h4>
				<div class="text"><?php echo get_the_excerpt() ?></div>
			</div>
		</div>	
	</section>

	<section class="gridGaleryMin">
		<div class="main">
			<ul class="grid">
			<?php
				$args = array(
					'post_type'              => 'qd_gallery',
					'post_status'      		 => 'publish',
					'posts_per_page'    	 => $showposts,
					'offset'  				 => $offset,
					'category_gallery' => $termC->slug,
					'post__not_in' => array($post->ID)
				);
				$query = new WP_Query($args);

				while ($query->have_posts()) : $query->the_post();
					$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
					echo '<li><a href="'.get_the_permalink().'"><img src="'.$urlThumb.'"></a></li>';
				endwhile;
			?>
			</ul>

			<?php include( get_stylesheet_directory() . '/inc/pagination.php'); ?>
		</div>
	</section>	

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();