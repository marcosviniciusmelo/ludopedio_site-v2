<?php
get_header();
global $url_tema, $nome_blog, $link_blog, $wpdb;

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 12;
$offset = ($paged - 1) * $showposts;
// $s = str_replace(array('“', '”'), array('', ''), $_GET['s']);
$s = $_GET['s'];


$types = array('post', 'qd_arquibancada', 'qd_entrevista', 'qd_gallery_museu', 'qd_gallery', 'qep_type_event', 'qd_biblioteca');

/*
* Get posts
*/
$args = array(
	'post_type'      => $types,
	'post_status'    => 'publish',
	'posts_per_page' => 2000,
	's'				 => $s
);


/*
* Get by editoras
*/
// $argsEditoras = array(
// 	'post_type'      => $types,
// 	'post_status'    => 'publish',
// 	'posts_per_page' => 2000,
// 	'meta_query' 	 => array(
// 		'relation' => 'OR',
//         array(
//             'key' => 'qd_biblioteca_article_editora',
//             'value' => $s,
//             'compare' => 'LIKE'
//         ),
//         array(
//             'key' => 'qd_biblioteca_article_subtitulo',
//             'value' => $s,
//             'compare' => 'LIKE'
//         ),
//         array(
//             'key' => 'qd_biblioteca_article_periodico',
//             'value' => $s,
//             'compare' => 'LIKE'
//         )
//     )
// );

// $queryEd = new WP_Query($argsEditoras);
// wp_reset_query();


$query = new WP_Query($args);
wp_reset_query();


$termIds = get_terms('category_autor', [
    'name__like' => $s,
    'fields' => 'ids'
]);

// echo '<pre>';
// print_r( $termIds );
// die();

$argsAuthor = array(
	'post_type'      => $types,
	'post_status'    => 'publish',
	'posts_per_page' => 2000,
	'search_tax_query' => true,
	'tax_query' => array(
        array(
            'taxonomy'  => 'category_autor',
            'field'     => 'id',
            'terms'     => $termIds
        )
    )
);
$queryAuthor = new WP_Query($argsAuthor);
wp_reset_query();


$result = new WP_Query();
$result->posts = array_unique(array_merge(
    $query->posts,
    $queryAuthor->posts,
    // $queryEd->posts
), SORT_REGULAR);

$postTypes = [];
foreach ($result->posts as $key):
	$postTypes[ get_post_type($key->ID) ] += 1;
endforeach;

?>
<main class="search-page">

	<section class="bannerTop">
		<div class="main">
			<div class="title">
				<span class="title">Resultados para “<?php echo $s ?>”</span>
			</div>
		</div>
		<img src="<?php echo get_field('background_busca', 'options')['url'] ?>" alt="" class="bg">
	</section>
	
	<div class="barSearch">
		<div class="scroll">
			<form action="<?php echo $link_blog . '/categoria/' . $cate->slug ?>">
				<div class="main">
					<div class="typesSearch">
						<?php
							foreach ($postTypes as $key => $value):
								switch ($key) {
									case 'qd_arquibancada':
										$label = 'Arquibancada';
										break;
									case 'qd_entrevista':
										$label = 'Entrevistas';
										break;
									case 'qd_gallery_museu':
										$label = 'Museu';
										break;
									case 'qd_gallery':
										$label = 'Futebol Arte';
										break;
									case 'qd_biblioteca':
										$label = 'biblioteca';
										break;
									case 'qep_type_event':
										$label = 'Eventos';
										break;
									default:
										$label = 'Ludosfera';
										break;
								}
								$class = '';
								if( $_GET['tipo'] )
									if($_GET['tipo'] == $key)
										$class = 'active';
								echo '<a href="?s='.$s.'&tipo='.$key.'" class="'.$class.'"><strong>'.$value.'</strong>'.$label.'</a>';
							endforeach;
						?>
					</div>
					<!-- <div class="right">
						<select name="ordem">
							<option value="">Organizar por</option>
							<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
							<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
						</select>
					</div> -->
				</div>
			</form>
		</div>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="bloco-posts">
		<div class="main">

			<div class="posts col-4">
				<?php

					$i=1;
					$iF=1;
					foreach ($result->posts as $key):

						if( isset($_GET['tipo']) )
							if( get_post_type($key->ID) != $_GET['tipo'] )
								continue;

						if( $i > $offset && $iF <= $showposts ):
							$iF++;
							$post = get_post($key->ID);
							include(get_stylesheet_directory() . '/inc/post/post-style02.php');
						endif;
						
						$i++;
					endforeach;


					if( !$result->posts ):
						echo 'Nenhum resultado encontrado.';
					endif;
				?>
			</div>
		</div>
	</section>

	<?php
		$totalPosts = $i-1;
		// echo $totalPosts;
		echo '<div class="pagination">';
			echo paginate_links(array(
			    'format' => '?pagina=%#%',
			    'current' => $paged,
			    'total' => ceil($totalPosts / $showposts ),
			    'prev_text' => __('<'),
				'next_text' => __('>'),
			));
		echo '</div>';
	?>

</main>

<?php
get_footer();