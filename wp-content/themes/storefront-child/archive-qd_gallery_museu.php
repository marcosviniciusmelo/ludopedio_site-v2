<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$page = get_page_by_path('museu-galeria-page');
$terms = get_terms([
    'taxonomy' => 'category_gallery_museu',
    'hide_empty' => false,
]);

?>
<main class="museu-page">

	<section class="bannerTop">
		<div class="main">
			<?php
				$title = get_field('banner_topo', $page->ID)['titulo'];
				$imagem = get_field('banner_topo', $page->ID)['imagem']['url'];
			?>
			<div class="title">
				<h3 class="subtitle"><?php echo $title ?></h3>
			</div>
		</div>
		<img src="<?php echo $imagem; ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/museu-galeria">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>

					<div class="linkDropdown">
						<span>Filtrar por <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug,'category_gallery_museu').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
					
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<?php if( !isset($_GET['busca']) && !isset($_GET['ordem']) ): ?>
	<section class="blocoGalerias">
		<div class="main">
			
			<ul class="grid">
			<?php
			foreach ($terms as $key) {

				$gallery_post = get_posts( array( 'post_type' => 'qd_gallery_museu', 'numberposts' => 1, 'category_gallery_museu' => $key->slug  ) );
				$urlThumb = get_the_post_thumbnail_url($gallery_post[0]->ID, 'medium');

				echo '<li class="_item">
						<a href="'.get_term_link($key->slug,'category_gallery_museu').'">
							<figure><img src="'.$urlThumb.'" class="image"><span><img src="'.$url_tema.'assets/images/icones/camera.png"> '.$key->count.' fotos</span></figure>
							<h4>'.$key->name.'</h4>
						</a>
					</li>';

			}
			?>
			</ul>	

		</div>
	</section>

	<?php else: ?>

	<section class="gridGaleryMin">
		<div class="main">
			<ul class="grid">
			<?php

			$paged = 1;
if( isset($_GET['page']) )
	$paged = $_GET['page'];
			$showposts = 16;
			$offset = ($paged - 1) * $showposts;

				$args = array(
					'post_type'      => 'qd_gallery_museu',
					'post_status'    => 'publish',
					'posts_per_page' => $showposts,
					'offset'  		 => $offset,
					's' 			 => $_GET['busca']
				);

				if( isset($_GET['ordem']) ){
					if ( $_GET['ordem'] == 'cadastro' ) {
						$args['orderby'] = 'date';
						$args['order'] = 'DESC';
					}else{
						$args['orderby'] = 'title';
						$args['order'] = 'ASC';
					}
				}
				$query = new WP_Query($args);

				while ($query->have_posts()) : $query->the_post();
					$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
					$terms = get_the_terms($post->ID, 'category_gallery_museu');
					echo '<li>
							<a href="'.get_the_permalink().'">
								<img src="'.$urlThumb.'">
								<span class="cat">'.$terms[0]->name.'</span>
								<h4>'.get_the_title().'</h4>
								<span class="data">'.get_the_date().'</span>
							</a>
						</li>';
				endwhile;
			?>
			</ul>

			<?php include( get_stylesheet_directory() . '/inc/pagination.php'); ?>
		</div>
	</section>

	<?php endif; ?>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();