<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$cate = get_queried_object();

get_header( 'shop' );

echo '<div class="products-page">';

do_action( 'woocommerce_before_main_content' );

if( is_shop() ):

	$args = array(
		'post_type'         => 'product',
		'post_status'       => 'publish',
		'posts_per_page'    => 1,
		'tax_query' => array(
	        array(
	            'taxonomy' => 'product_cat',
	            'field'    => 'slug',
	            'terms'    => array( 'apoie', 'cursos' ),
	            'operator' => 'NOT IN',
	        ),
	    )
	);
	$query = new WP_Query($args);

	while ($query->have_posts()) : $query->the_post();

		$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
		$background = get_field('background');
		$product = wc_get_product( $post->ID );

		$tag = '';
		$btn = 'Compre agora';
		if( $product->get_stock_status() == 'onbackorder' ){
			$tag = '<span>Pré-venda</span>';
			$btn = 'Pré-venda';
		}

		echo '<section class="productDestaque">
				<div class="main">
					<div class="left">
						'.$tag.'
						<a href="'.get_the_permalink().'"><h3>'.get_the_title().'</h3></a>
						<div class="descricao">'.$short_description.'</div>
						<div class="linePrice">
							<div class="price">
								<span class="regularPrice">R$ '.number_format($product->get_regular_price(), 2, ',', ' ').'</span>
								<h5>R$ '.number_format($product->get_price(), 2, ',', ' ').'</h5>
							</div>
							<a href="?add-to-cart='.$post->ID.'" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="'.$post->ID.'" rel="nofollow">'.$btn.'</a>
						</div>
					</div>
					<div class="right"><a href="'.get_the_permalink().'"><img src="'.get_the_post_thumbnail_url($post->ID, 'large').'"></a></div>
				</div>
				<img src="'.$background['url'].'" class="bg">
			</section>';

	endwhile;
	?>
<?php else: ?>
	<section class="bannerTop catProduct">
		<div class="main">
			<div class="title">
				<h3 class="subtitle"><?php echo $cate->name ?></h3>
			</div>
		</div>
	</section>
<?php endif; ?>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

		
	<div class="main">
		<?php do_action( 'woocommerce_before_shop_loop' ); ?>
		<?php dynamic_sidebar('sidebar-product') ?>
	</div>

	<hr>
	<div class="main">

	<?php
	if ( woocommerce_product_loop() ) {

		woocommerce_product_loop_start();

		if ( wc_get_loop_prop( 'total' ) ) {
			while ( have_posts() ) {
				the_post();

				do_action( 'woocommerce_shop_loop' );

				wc_get_template_part( 'content', 'product' );
			}
		}

		woocommerce_product_loop_end();

		do_action( 'woocommerce_after_shop_loop' );
	} else {

		do_action( 'woocommerce_no_products_found' );
	}

	do_action( 'woocommerce_after_main_content' );

	do_action( 'woocommerce_sidebar' );

	echo '</div>';
	echo '</div>';


get_footer( 'shop' );
