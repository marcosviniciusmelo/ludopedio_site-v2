<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>

<?php if( get_field('links_externos') ): ?>
<div class="ctnPrice virtual">

	<div class="priceLeft <?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>">
		<span class="labelP">E-Book</span>
		<span class="woocommerce-Price-amount amount">
			<bdi><span class="woocommerce-Price-currencySymbol">R$</span><?php echo str_replace('R$', '', get_field('preco_ebook') ); ?></bdi>
		</span>
		
	</div>

	<div class="links">
		<?php
			foreach (get_field('links_externos') as $key) {
				echo '<a href="'.$key['link'].'" target="_blank" class="btn">'.$key['nome'].'</a>';
			}
		?>
	</div>	
</div>
<?php endif; ?>

<?php if ( $product->is_in_stock() ) : ?>
<div class="ctnPrice">

	<div class="priceLeft <?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) ); ?>">
		<span class="labelP"><?php echo ($product->is_virtual() ? '' : 'Livro físico') ?></span>
		<?php echo $product->get_price_html(); ?>
	</div>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input(
			array(
				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
				'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
			)
		);

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>
</div>
<?php endif; ?>
