<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

global $post;
$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
?>

<?php if ( get_field('dados_tecnicos', $post->ID) ): ?>
<div class="ctnLine dadosTecnicos">
	<h4>Dados técnicos</h4>
	<div class="ctn">
		<div class="woocommerce-product-details__short-description">
			<?php echo get_field('dados_tecnicos', $post->ID); // WPCS: XSS ok. ?>
		</div>
	</div>
</div>	
<?php endif; ?>


<div class="ctnLine autor">
	<!-- <h4>Sobre o autor <?php echo get_the_author_meta('display_name', $authorID) ?></h4>
	<div class="ctn">
		<div class="left"><?php echo $bio ?></div>
		<a href="<?php echo get_bloginfo('url').'/autor/?p='.$slug ?>" class="btn btn-black">Saiba mais</a>
	</div> -->
    <?php ludo_the_author_box() ?>
</div>	
