<?php
global $url_tema, $link_blog, $nome_blog; the_post();
get_header();
$page = get_page_by_path('arquibancada');
$category = get_the_terms( $post->ID, 'category_arquibancada' );
$term = get_the_terms($post->ID, 'category_arquibancada_volume');
$numero = [];
if( isset($term[0]->name) )
	$numero = explode('Volume ', $term[0]->name);

?>

<main class="single-post-page entrevista">

	<section class="bannerTop entrevista">
		<div class="main">
			<?php
				$imagem = get_field('banner_topo', $page->ID)['imagem'];
			?>
			<div class="title">
				<span class="title">Arquibancada</span>
			</div>
		</div>

		<div class="barBottom">
			<div class="main">
				<div class="left">
					<a href="<?php echo $link_blog.'/arquibancada'; ?>" class="item active">Arquivo</a>
					<a href="<?php echo $link_blog.'/normas-arquibancada'; ?>" class="item">Normas</a>
					<a href="<?php echo $link_blog.'/expediente-arquibancada'; ?>" class="item">Expediente</a>
				</div>
				<div class="issn">ISSN: <?php echo get_field('issn_arquibancada', 'options'); ?></div>
			</div>
		</div>

		<img src="<?php echo $imagem['url']  ?>" alt="" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/arquibancada">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
					<div class="linkDropdown">
						<span>Volumes <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$termsV = get_terms([
							    'taxonomy' => 'category_arquibancada_volume',
							    'hide_empty' => true,
							    'orderby' => 'ID',
    							'order' => 'DESC',
							]);
							foreach ($termsV as $key) {
								$name = explode('Volume ', $key->name);
								echo '<a href="'.get_term_link($key->slug, 'category_arquibancada_volume').'">Vol. '.$name[1].' - '.$key->description.'</a>';
							}
						?>
						</div>
					</div>
					<div class="linkDropdown">
						<span>Colunas <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$terms = get_terms([
							    'taxonomy' => 'category_arquibancada',
							    'hide_empty' => true,
							]);
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug, 'category_arquibancada').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="ctnPostContent">
		<section class="post-content">
			<div class="main">

				<div class="ctnScroll">

					<div class="lineTitlePost">
						<div class="numero"><?php echo $numero[1].'.'.get_field('qd_article_number', $post->ID); ?></div>
						<div class="category"><a href="<?php echo get_term_link($category[0]->slug, 'category_arquibancada'); ?>"><?php echo $category[0]->name; ?></a></div>
						<div class="totalViews"><?php //echo do_shortcode('[post-views]') ?></div>
						<h2><?php the_title() ?></h2>

						<div class="date">
							<span><?php ludo_the_author_link( $post->ID ); ?></span>
							<span class="bar"></span>
							<?php if( ludo_group_author_link( $post->ID ) ): ?>
								<span><?php print_r( ludo_group_author_link( $post->ID ) ); ?></span>
								<span class="bar"></span>
							<?php endif; ?>
							<span><i class="far fa-calendar-alt"></i> <?php echo get_the_date('j \d\e F \d\e Y') ?></span>
						</div>

					</div>

					<div class="barFloatLeft">
						<?php include(get_stylesheet_directory() . '/inc/barShared.php') ?>
					</div>

					<div class="contentText">
						<?php wp_reset_query(); ?>
						<?php the_content() ?>
						<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>
						<?php ludo_the_author_box() ?>

						<?php
                            /** 
                            *
                            * Como citar
                            *
                            **/
                            $authors = ludo_the_author_no_link( get_the_id(), false );
							$authors_array = explode( ',', $authors );
							$authors_total = count( $authors_array );
							$count = 1;
							$author_ref = '';
							$numero_article = '';

							// Autores
							foreach( $authors_array as $author ):

								$author_array_1 = explode( ' ', $author );

								$last_name = end($author_array_1);

								if( $last_name == 'Junior' || $last_name == 'Júnior' || $last_name == 'Neto' || $last_name == 'FIlho' ):
									$array_total_indices = count( $author_array_1 );
									$last_indice = $array_total_indices - 1;
									unset( $author_array_1[$last_indice] );
									$array_total_indices = count( $author_array_1 );
									$last_indice = $array_total_indices - 1;
									$author_array_1[$last_indice] = $author_array_1[$last_indice] . ' ' . $last_name;
								endif;

								if( in_array(" de ", $author_array_1) ):
									$author_array_2 = explode( ' de ', $author );
									$total_words = count($author_array_2);
									$last_name = $author_array_2[ $total_words - 1 ];
									$first_name = $author_array_2[0] . 'de';
									// Autor
									$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
									if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
								else:
									if( in_array(" da ", $author_array_1) ):
										$author_array_2 = explode( ' da ', $author );
										$total_words = count($author_array_2);
										$last_name = $author_array_2[ $total_words - 1 ];
										$first_name = $author_array_2[0] . 'da';
										// Autor
										$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
										if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
									else:
										if( in_array(" dos ", $author_array_1) ):
											$author_array_2 = explode( ' dos ', $author );
											$total_words = count($author_array_2);
											$last_name = $author_array_2[ $total_words - 1 ];
											$first_name = $author_array_2[0] . 'dos';
											// Autor
											$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
											if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
										else:
											$total_words = count($author_array_1);
											$last_name = $author_array_1[ $total_words - 1 ];
											$first_name = null;
											for( $x=0; $x<($total_words - 1); $x++  ):
												$first_name .= $author_array_1[$x];
												if( $x < ( $total_words - 2 ) ) $first_name .= ' ';
											endfor;
											// Autor
											$author_ref .= '<span class="text-uppercase">' . strtoupper( $last_name ) . '</span>' . ', ' . $first_name;
											if( $count < $authors_total ): $author_ref .= '; '; else: $author_ref .= '. '; endif;
										endif;
									endif;
								endif;
								$count++;
							endforeach;

							// Termo do post corrente
							$post_terms = wp_get_post_terms( get_the_ID(), 'category_arquibancada' );
							$post_term_slug = $post_terms [0]->slug;

							// Titulo
							$title = get_the_title();

                            //Vornei: arrumando numero article
                            $numero_article = '';
                            if( $numero[1] ):
                            	
		                        $numero_article = 'v. '.$numero[1].', n. '. get_field('qd_article_number', $post->ID);
                        	endif;

                            //Ano
                            $arr_data = explode(' ', get_the_date());
                            $ano = $arr_data[4];

                            echo '<div class="box-commons-como">';
                            echo '<h4>Como citar</h4>';
                            $como_citar = $author_ref . $title . '. <b>Ludopédio</b>, São Paulo, ' .$numero_article .', '. $ano .'. ';
                            echo $como_citar;
                            echo '</div>';
                        ?>

                        <div class="box-commons-como"> <?php ludo_post_creative_commons_icons() ?></div>

                        <?php
	                        /**
	                        *
	                        * External URL
	                        *
	                        **/

	                    	$redirect_url = get_field( 'ludo_redirect_url' );
	                    	if( $redirect_url ):
		                        echo '<a class="linkExternal" href="'.$redirect_url.'" target="_blank">
		                        		<i class="fas fa-external-link-alt"></i>
		                        	</a>';
					        endif;
					    ?>
					</div>

					<div class="relateds">
						<div class="title">Leia também:</div>
						<div class="scroll">
							<ul class="posts col-3 pd-20">
								<?php

								$tags = get_the_terms(get_the_ID(), 'post_tag');
								$tagsArr = [];
								foreach ($tags as $key):
									array_push($tagsArr, $key->slug);
								endforeach;

								$args = array(
									'post_type' 	 => 'qd_arquibancada',
									'posts_per_page' => 3,
									'post_status'    => 'publish',
									'post__not_in'   => array(get_the_ID()),
									'tax_query' => array(
										array(
											'taxonomy' => 'post_tag',
											'field'    => 'slug',
											'terms'    => $tagsArr,
											'operator ' => 'IN'
										)
									)
								);

								$productIds = [];
								$query = new WP_Query($args);
								while ($query->have_posts()) : $query->the_post();
									array_push($productIds, get_the_id());
								endwhile;
								wp_reset_postdata();

								if( count($productIds) < 3 ):

									$args = array(
										'post_type' => 'qd_arquibancada',
										'post_status' => 'publish',
										'posts_per_page' => 3,
										'post__not_in' => array(get_the_ID()),
										'tax_query' => array(
											array(
												'taxonomy'  => 'category_autor',
												'field'     => 'slug',
												'terms'     => ludo_the_author_slug(get_the_id())
											)
										)
									);
									$query = new WP_Query($args);
									while ($query->have_posts()) : $query->the_post();
										array_push($productIds, get_the_id());
									endwhile;
									wp_reset_postdata();
								endif;

								$args = array(
									'post_type' 	 => 'qd_arquibancada',
									'posts_per_page' => 3,
									'post_status'    => 'publish',
									'post__in'   => $productIds,
								);
								$query = new WP_Query($args);
								while ($query->have_posts()) : $query->the_post();
									include(get_stylesheet_directory() . '/inc/post/post-style08.php');
								endwhile;
								wp_reset_postdata();
								?>
							</ul>
						</div>
					</div>

					<?php
						include(get_stylesheet_directory() . '/inc/comments.php');
					?>
				</div>

			</div>
		</section>

		<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>
		
	</div>

</main>

<?php
get_footer();