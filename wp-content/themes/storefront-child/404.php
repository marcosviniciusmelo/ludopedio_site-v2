<?php
global $url_tema, $link_blog, $nome_blog;
get_header();
?>

<main class="page-404">

	<div class="ctn">
		<h4>404</h4>
		<h2>Página não encontrada</h2>
		<a href="<?php echo $link_blog ?>" class="btn orange">VOLTE PARA A PÁGINA INICIAL</a>
	</div>
	
</main>

<?php
get_footer();