<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$cate = get_queried_object();

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 12;
$offset = ($paged - 1) * $showposts;

?>
<main class="biblioteca-page">

	<section class="bannerTop">
		<div class="main">
			<div class="title">
				<h3 class="subtitle">Biblioteca</h3>
			</div>
		</div>
		<img src="<?php echo $url_tema; ?>assets/images/banner-top-biblioteca.jpg" class="bg">
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/biblioteca">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>

					<div class="linkDropdown">
						<span>Categoria <i class="fas fa-bars"></i></span>
						<div class="dropdown">
						<?php
							$terms = get_terms( 'category_biblioteca', array( 'hide_empty' => false, 'parent' => 0 ) );
						    foreach( $terms as $parent_term ):
						        $term_children = get_term_children($parent_term->term_id, 'category_biblioteca');

						        $class = '';
						        if( $term_children )
						        	$class = 'menu-item-has-children';
						        	echo '<li class="menu-item '.$class.'">
						        			<a href="'.get_term_link($parent_term->slug, 'category_biblioteca').'">'.$parent_term->name.'</a>';

								        if( $term_children ){
								        	echo '<ul class="sub-menu">';
								        	foreach ($term_children as $key) {
								        		$t = get_term_by('id', $key, 'category_biblioteca');
								        		echo '<li><a href="'.get_term_link($t->slug, 'category_biblioteca').'">'.$t->name.'</a></li>';
								        	}
								        	echo '</ul>';
								        }

						       		echo '</li>';

						    endforeach;
						?>
						</div>
					</div>

				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<?php if( !isset($_GET['busca']) && !isset($_GET['ordem']) ): ?>
		<section class="listBilioteca">
			<div class="main">
				<?php
					$terms = get_terms( 'category_biblioteca', array( 'hide_empty' => true, 'parent' => 0 ) );
					foreach( $terms as $term ):

						if( $term->name == 'Ludopédio TV' ) continue;

						echo '<div class="topList">
								<h3>'.$term->name.'</h3>
								<a href="'.get_term_link($term->slug, 'category_biblioteca').'" class="btn-ver">Ver todas ></a>
							  </div>';

						echo '<ul class="gridBiblioteca">';
							$args = array(
								'post_type' 		  => 'qd_biblioteca',
								'post_status' 		  => 'publish',
								'posts_per_page'      => 4,
								'category_biblioteca' => $term->slug
							);

							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
								$urlThumb = get_the_post_thumbnail_url($post->ID, 'medium');
				
								echo '<li class="_item">
										<a href="'.get_the_permalink().'">
											<figure><img src="'.$urlThumb.'"></figure>
											<h4>'.get_the_title().'</h4>
											<span>'.ludo_the_author_no_link( $post->ID, false ).'</span>
										</a>
									</li>';
							endwhile;
							wp_reset_postdata();

						echo '</ul>';

					endforeach;
				?>
			</div>
		</section>
	<?php else: ?>
		<section class="listBilioteca">
			<div class="main">
			<?php
				$s = $_GET['busca'];
				echo '<ul class="gridBiblioteca">';
					$args = array(
						'post_type' 	  => 'qd_biblioteca',
						'post_status' 	  => 'publish',
						'posts_per_page'  => $showposts,
						'offset'  		  => $offset,
						's'				  => $s
					);

					if( isset($_GET['ordem']) ){
						if ( $_GET['ordem'] == 'cadastro' ) {
							$args['orderby'] = 'date';
							$args['order'] = 'DESC';
						}else{
							$args['orderby'] = 'title';
							$args['order'] = 'ASC';
						}
					}

					$query = new WP_Query($args);

					$termIds = get_terms('category_autor', [
						'name__like' => $s,
						'fields' => 'ids'
					]);

					if( isset($_GET['dev']) )
						print_r($termIds);
					
					// filter Author
					$argsAuthor = array(
						'post_type' 	   => 'qd_biblioteca',
						'post_status'      => 'publish',
						'posts_per_page'   => 500,
						'search_tax_query' => true,
						'tax_query' => array(
							array(
								'taxonomy'  => 'category_autor',
								'field'     => 'id',
								'terms'     => $termIds
							)
						)
					);
					$queryAuthor = new WP_Query($argsAuthor);
					wp_reset_query();


					// filter Magazine
					$argsRevista = array(
						'post_type' 	   => 'qd_biblioteca',
						'post_status'      => 'publish',
						'posts_per_page'   => 500,
						'search_tax_query' => true,
						'meta_query' => array(
							array(
								'key'  		=> 'qd_biblioteca_article_periodico',
								'value'     => $s,
								'compare'   => 'LIKE'
							)
						)
					);
					$queryRevista= new WP_Query($argsRevista);
					wp_reset_query();


					// filter Publisher
					$argsEd = array(
						'post_type' 	   => 'qd_biblioteca',
						'post_status'      => 'publish',
						'posts_per_page'   => 500,
						'search_tax_query' => true,
						'meta_query' => array(
							array(
								'key'  		=> 'qd_biblioteca_article_editora',
								'value'     => $s,
								'compare'   => 'LIKE'
							)
						)
					);
					$queryEd= new WP_Query($argsEd);
					wp_reset_query();


					// filter Coorientador
					$argsCoorientador = array(
						'post_type' 	   => 'qd_biblioteca',
						'post_status'      => 'publish',
						'posts_per_page'   => 500,
						'search_tax_query' => true,
						'meta_query' => array(
							array(
								'key'  		=> 'qd_biblioteca_article_co_orientador',
								'value'     => $s,
								'compare'   => 'LIKE'
							)
						)
					);
					$queryCoorientador= new WP_Query($argsCoorientador);
					wp_reset_query();

					$result = new WP_Query();
					$result->posts = array_unique(array_merge(
						$query->posts,
						$queryAuthor->posts,
						$queryRevista->posts,
						$queryEd->posts,
						$queryCoorientador->posts
					), SORT_REGULAR);
					
					foreach ($result->posts as $key):
						$urlThumb = get_the_post_thumbnail_url($key->ID, 'medium');
		
						echo '<li class="_item">
								<a href="'.get_the_permalink($key->ID).'">
									<figure><img src="'.$urlThumb.'"></figure>
									<h4>'.get_the_title($key->ID).'</h4>
									<span>'.ludo_the_author_no_link( $key->ID, false ).'</span>
								</a>
							</li>';
					endforeach;
					wp_reset_postdata();

				echo '</ul>';
				$pageType = 'pagina';
				include( get_stylesheet_directory() . '/inc/pagination.php');
			?>
			</div>
		</section>
	<?php endif; ?>

	<section class="bloco-posts grey" style="margin: 0;">
		<div class="main">
			<div class="title">
				<h3>Loja</h3>
				<a href="<?php echo $link_blog ?>/loja" class="btn-ver">Conheça a loja  ></a>
			</div>

			<?php echo do_shortcode('[recent_products limit=4 category="apoie" cat_operator="NOT IN"]') ?>
		</div>	
	</section>
	
</main>

<?php
get_footer();