<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */	

setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
?>
<?php global $url_tema, $nome_blog, $link_blog; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo $url_tema ?>assets/images/favicon.png" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css?<?php echo rand(000,999) ?>" />

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<?php if( is_single() ): ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5f9fdd353b1cfe00120b72ed&product=inline-share-buttons" async="async"></script>
	<?php endif; ?>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />

	<script>
	    var url = '<?php echo $link_blog ?>';
	</script>

	<?php wp_head(); ?>

	<?php echo get_field('codigo_header', 'options') ?>

</head>

<body <?php body_class(); ?>>

	<?php echo get_field('codigo_body', 'options') ?>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">

	<header id="masthead" class="site-header">

		<div class="headerTop">
			<div class="main">
				<div class="left">
					<?php echo sno_show_menu('nav-top-left') ?>
				</div>
				<div class="redes">
					<a href="https://www.youtube.com/c/Ludop%C3%A9dio?sub_confirmation=1" target="_blank"><i class="fab fa-youtube"></i></a>
					<a href="https://twitter.com/ludopedio" target="_blank"><i class="fab fa-twitter"></i></a>
					<a href="https://www.instagram.com/ludopedio/" target="_blank"><i class="fab fa-instagram"></i></a>
					<a href="https://www.facebook.com/ludopediofutebol" target="_blank"><i class="fab fa-facebook"></i></a>
				</div>
			</div>
		</div>	
	
		<div class="main">
			<h1 class="brand">
				<a href="<?php echo $link_blog ?>">
					<?php the_title() ?>
					<img src="<?php echo $url_tema ?>assets/images/brand.png" alt="<?php the_title() ?>">
				</a>
			</h1>
			<div class="right">

				<nav class="main-menu">
				<?php echo sno_show_menu('nav-main') ?>
				<a href="<?php echo $link_blog ?>/loja" class="btn" <?php if(is_page_template('poroutrofutebol.php') && get_field('cor_base') != '#000000' ) echo 'style="background-color: '.get_field('cor_base').'"'; ?>><img src="<?php echo $url_tema ?>assets/images/icones/cart2.png" alt=""> Nossa Loja</a>
				</nav>
				
				<div class="infosCtn">
					<div class="account clickMenuDrop">
						<a href="<?php echo $link_blog ?>/minha-conta"><img src="<?php echo $url_tema ?>assets/images/icones/account.png"></a>
						<div class="submenu">
							<?php echo sno_show_menu('nav-conta'); ?>
						</div>
					</div>
					<a href="javascript:;" class="link clickBusca"><img src="<?php echo $url_tema ?>assets/images/icones/search.png"></a>

					<a href="<?php echo $link_blog ?>/carrinho" class="link linkCart">
						<div class="number"></div>
						<img src="<?php echo $url_tema ?>assets/images/icones/cart.png" alt="">
					</a>

					<div class="link clickMenuDrop">
						<img src="<?php echo $url_tema ?>assets/images/icones/menu.png" alt="">
						<div class="submenu">
							<div class="mobile">
								<?php echo sno_show_menu('nav-main') ?>
								<?php echo sno_show_menu('nav-top-left') ?>
							</div>
							<?php echo sno_show_menu('nav-secundario') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<?php if( is_singular('product') ): ?>
	<section class="breadcrumbBar">
		<div class="main">
			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs">','</p>' ); } ?>
		</div>
	</section>
	<?php endif; ?>
	
	<?php if( is_page('minha-conta') || is_singular('product') || is_page('carrinho') || is_page('finalizar-compra') ): ?>
	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">
	<?php endif; ?>
		<?php do_action( 'storefront_content_top' );
