<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

global $wpdb;


// get all subscriptions IDS
$subscriptions_ids = $wpdb->get_col("
    SELECT ID  FROM {$wpdb->prefix}posts 
    WHERE post_type LIKE 'shop_subscription'
");

$totalValor = 0.00;
$totalPerson = [];

foreach($subscriptions_ids as $subscription_id){

    $subscription = new WC_Subscription( $subscription_id );
    $data = $subscription->get_data();

    if( $data['status'] == 'active' ):
    	$totalValor += floatval ($data['total']);
	    if( floatval($data['customer_id']) > 0 )
	    	if (!in_array($data['customer_id'], $totalPerson))
	    		array_push($totalPerson, $data['customer_id']);

	endif;
}

?>
<main class="apoie-page">

	<section class="bannerTop">
		<div class="main">
			<div class="left">
				<h3><?php the_field('titulo_bloco01') ?></h3>
				<h2><?php echo get_field('subtitulo_bloco01') ?></h2>
				<div class="text"><?php echo get_field('texto_bloco01') ?></div>
			</div>
			<div class="right">
				<div class="ctn">
					<img src="<?php echo $url_tema; ?>assets/images/icones/cifrao.png" class="cifrao">
					<strong>Total arrecadado</strong>
					<h4 id="totalApoio">R$ <?php echo number_format($totalValor,2,",","."); ?></h4>
					<span>Apoiado por <?php echo count($totalPerson) ?> pessoas</span>
					<hr>
					<a href="#pacotes" class="btn">VEJA OS PACOTES DE APOIO</a>
				</div>
			</div>
		</div>

		<img src="<?php echo get_field('background_bloco01')['url'] ?>" alt="" class="bg">
	</section>


	<section class="section01">
		<div class="main">
			<div class="embed">
				<?php
					preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#",
                            get_field('video_bloco02'), $urlVideo);
				?>
				<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo $urlVideo[0][0] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>

			<div class="titleCtn">Escolha como você quer apoiar</div>

			<?php

				$argsMensal = array(
					'post_type'         => 'product',
					'post_status'       => 'publish',
					'posts_per_page'    => 20,
					'tax_query' => array(
				        array(
				            'taxonomy' => 'product_cat',
				            'field'    => 'slug',
				            'terms'    => array( 'mensal' ),
				            'operator' => 'IN',
				        ),
				    ),
				    'orderby' => 'meta_value_num',
					'order' => 'asc',
					'meta_key' => '_price'
				);
				$mensal = new WP_Query($argsMensal);

				$argsSemestre = array(
					'post_type'         => 'product',
					'post_status'       => 'publish',
					'posts_per_page'    => 20,
					'tax_query' => array(
				        array(
				            'taxonomy' => 'product_cat',
				            'field'    => 'slug',
				            'terms'    => array( 'semestral' ),
				            'operator' => 'IN',
				        ),
				    ),
				    'orderby' => 'meta_value_num',
					'order' => 'asc',
					'meta_key' => '_price'
				);
				$semestre = new WP_Query($argsSemestre);


				$argsAnual = array(
					'post_type'         => 'product',
					'post_status'       => 'publish',
					'posts_per_page'    => 20,
					'tax_query' => array(
				        array(
				            'taxonomy' => 'product_cat',
				            'field'    => 'slug',
				            'terms'    => array( 'anual' ),
				            'operator' => 'IN',
				        ),
				    ),
				    'orderby' => 'meta_value_num',
					'order' => 'asc',
					'meta_key' => '_price'
				);
				$anual = new WP_Query($argsAnual);


			?>

			<ul class="abasPlanos" id="pacotes">
				<?php if( $mensal->have_posts() ): ?><li class="_item active">Por mês</li><?php endif; ?>
				<?php if( $semestre->have_posts() ): ?><li class="_item">Por semetre</li><?php endif; ?>
				<?php if( $anual->have_posts() ): ?><li class="_item">Por ano</li><?php endif; ?>
			</ul>

			<?php if( $mensal->have_posts() ): ?>
			<div class="conteudoPlano active">
				<ul class="grid">
					<?php 
						while ($mensal->have_posts()) : $mensal->the_post();
						$product = wc_get_product( $post->ID );
					
							echo '<li class="item">
								<div class="toolTip" data-id="'.$post->ID.'" data-mode="apoie"></div>
								<h4>'.get_the_title().'</h4>
								<span>todo mês</span>
								<div class="price">
									<span>R$</span>
									<h5>'.number_format($product->get_price(), 2, ',', ' ').'</h5>
								</div>
								<a href="?add-to-cart='.$post->ID.'" data-quantity="1" class=" button product_type_simple add_to_cart_button ajax_add_to_cart btn" data-product_id="'.$post->ID.'" rel="nofollow">Apoiar</a>
							</li>';

						endwhile;
						wp_reset_postdata();
					?>
				</ul>
			</div>
			<?php endif; ?>

			<?php if( $semestre->have_posts() ): ?>
			<div class="conteudoPlano">
				<ul class="grid">
					<?php 
						while ($semestre->have_posts()) : $semestre->the_post();
						$product = wc_get_product( $post->ID );
					
							echo '<li class="item">
								<div class="toolTip" data-id="'.$post->ID.'"></div>
								<h4>'.get_the_title().'</h4>
								<span>a cada 6 meses</span>
								<div class="price">
									<span>R$</span>
									<h5>'.number_format($product->get_price(), 2, ',', ' ').'</h5>
								</div>
								<a href="?add-to-cart='.$post->ID.'" data-quantity="1" class=" button product_type_simple add_to_cart_button ajax_add_to_cart btn" data-product_id="'.$post->ID.'" rel="nofollow">Apoiar</a>
							</li>';

						endwhile;
						wp_reset_postdata();
					?>
				</ul>
			</div>
			<?php endif; ?>

			<?php if( $anual->have_posts() ): ?>
			<div class="conteudoPlano">
				<ul class="grid">
					<?php 
						while ($anual->have_posts()) : $anual->the_post();
						$product = wc_get_product( $post->ID );
					
							echo '<li class="item">
								<div class="toolTip" data-id="'.$post->ID.'"></div>
								<h4>'.get_the_title().'</h4>
								<span>todo ano</span>
								<div class="price">
									<span>R$</span>
									<h5>'.number_format($product->get_price(), 2, ',', ' ').'</h5>
								</div>
								<a href="?add-to-cart='.$post->ID.'" data-quantity="1" class=" button product_type_simple add_to_cart_button ajax_add_to_cart btn" data-product_id="'.$post->ID.'" rel="nofollow">Apoiar</a>
							</li>';

						endwhile;
						wp_reset_postdata();
					?>
				</ul>
			</div>
			<?php endif; ?>

		</div>
	</section>

	<section class="section02">
		<div class="main">
			<div class="titleCtn"><?php the_field('titulo_bloco03') ?></div>
			<div class="text">
				<?php echo get_field('texto_bloco03') ?>
			</div>
		</div>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<div class="modalPlano">
	<div class="close"><i class="fas fa-times"></i></div>
	<div class="content"></div>
</div>
<div class="overflowPlano"></div>

<?php
get_footer();