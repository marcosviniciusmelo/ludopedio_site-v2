<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 20;
$offset = ($paged - 1) * $showposts;

	
$args = array(
	'post_type'              => 'qep_type_event',
	'post_status'      		 => 'publish',
	'meta_key' 				 => 'qep_event_start_date',
	'orderby'   			 => 'meta_value',
	'order' 				 => 'DESC',
	'meta_query' => array(
		array(
			'key'     => 'qep_event_end_date',
			'value'   => date("Ymd"),
			'compare' => '>='
		)
	)
);

$query = new WP_Query($args);
$arrDaySchedules = [];
while ($query->have_posts()) : $query->the_post();
	
	array_push($arrDaySchedules, date('m-d-Y', strtotime(get_field('qep_event_start_date', $post->ID))));

endwhile;

$daySchedules = implode(',', $arrDaySchedules);


?>
<main class="museu-page">

	<section class="sliderCategory">
		<div class="slider">
			<?php
				$args = array(
				'post_type'      => 'qep_type_event',
				'post_status'    => 'publish',
				'posts_per_page' => 4,
				'meta_query' => array(
					'relation' => 'AND',
					'data_end_clause' => array(
				        'key' => 'qep_event_end_date',
				        'value' => date("Ymd"),
				        'compare' => '>='
				    ),
				    'data_start_clause' => array(
				        'key' => 'qep_event_start_date',
				    )
			    ),
			    'orderby' => 'data_start_clause',
			    'order' => 'ASC'
			);
			$query = new WP_Query($args);

			while ($query->have_posts()) : $query->the_post();
			?>
				<li class="_itemGrid style-06 flex">
					<div class="ctn">
						<div class="bottom">
							<div class="main">
								<div class="left">
									<?php
										$dates = '';

										if( get_field('qep_event_start_date', $post->ID) != '' ):
											$dateStart = get_field('qep_event_start_date', $post->ID);
											$dates .= date('d/m/Y', strtotime($dateStart));
										endif;

										if( get_field('qep_event_end_date', $post->ID) != '' && get_field('qep_event_end_date', $post->ID) != get_field('qep_event_start_date', $post->ID) ):
											$dateEnd = get_field('qep_event_end_date', $post->ID);
											$dates .= ' - ' . date('d/m/Y', strtotime($dateEnd));
										endif;
									?>
									<a href="<?php the_permalink() ?>">
										<span class="date"><i class="far fa-calendar-alt"></i> <?php echo $dates ?></span>
										<h3 class="title-c"><?php echo get_the_title($post->ID) ?></h3>
										<div class="description"><?php echo wp_trim_words( get_the_excerpt($post->ID), 20 ); ?></div>
									</a>
								</div>
								<?php
									if( get_field('qd_event_date_limit_form', $post->ID) != '' ){
										if( get_field('qd_event_date_limit_form', $post->ID) >= date("Ymd") ){
											echo '<a href="'.get_the_permalink().'" class="btn">INSCRIÇÕES ABERTAS</a>';
										}else{
											echo '<a href="'.get_the_permalink().'" class="btn red">INSCRIÇÕES ENCERRADAS</a>';
										}
									}
								?>
							</div>
						</div>
						<figure class="bg">
							<?php $urlThumb = get_the_post_thumbnail_url($post->ID, 'full'); ?>
							<img src="<?php echo $urlThumb ?>" alt="">
						</figure>
					</div>
				</li>
			<?php
			endwhile;
			wp_reset_postdata();
			?>
		</div>
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog ?>/agenda-de-eventos">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar por evento" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="cadastro" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'cadastro' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="nome" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'nome' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>

				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>
	
	<section class="bloco-posts">
		<div class="main">

			<form class="filtroData" action="<?php echo $link_blog ?>/agenda-de-eventos">
				<div class="leftL">Veja os eventos por dia do mês</div>
				<div class="fields">
					<input type="hidden" name="daySchedules" value="<?php echo $daySchedules;?>">
					<input type="text" name="data" class="fieldData" placeholder="Selecionar data" value="<?php echo ( $_GET['data'] ) ? $_GET['data']: ''; ?>">
					<i class="far fa-calendar-alt"></i>
				</div>
			</form>

			<ul class="posts col-2">
			<?php
				$args = array(
					'post_type'              => 'qep_type_event',
					'post_status'      		 => 'publish',
					'posts_per_page'    	 => $showposts,
					'offset'  				 => $offset,
					'meta_key' 				 => 'qep_event_start_date',
					'orderby'   			 => 'meta_value',
					'order' 				 => 'DESC',
					'meta_query' => array(
			            array(
			            	'key'     => 'qep_event_end_date',
			            	'value'   => date("Ymd"),
				            'compare' => '>='
			            )
				    )
				);

				if( isset($_GET['data']) ){
					$data = explode('-', $_GET['data']);
					$args['meta_query'] = array(
				            array(
				            	'key'     => 'qep_event_start_date',
				            	'value'   => $data[2].$data[1].$data[0],
					            'compare' => '='
				            )
					    );
				}
				$query = new WP_Query($args);

				while ($query->have_posts()) : $query->the_post();
					include(get_stylesheet_directory() . '/inc/post/post-style07.php');
				endwhile;
			?>
			</ul>
			<?php
				echo '<div class="pagination">';
				echo paginate_links(array(
				    'format' => '?pagina=%#%',
				    'current' => $paged,
				    'total' => ceil($query->found_posts / $showposts ),
				    'prev_text' => __('<'),
					'next_text' => __('>'),
				));
				echo '</div>';
			?>
		</div>
	</section>	

	<?php include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();