<?php
get_header();
global $url_tema, $nome_blog, $link_blog;

$paged = 1;
if( isset($_GET['pagina']) )
	$paged = $_GET['pagina'];
$showposts = 11;
$offset = ($paged - 1) * $showposts;

$autor = $_GET['p'];
$user = get_user_by('login', $autor);


$authorID = $user->data->ID;

if( !$user ){

	$q = new WP_User_Query(
	    array(
	        'fields'     => 'all_with_meta',
	        'meta_query' => array(
	        	'relation' => 'OR',
	            array(
	                'key'     => 'slug',
	                'value'   => $autor,
	                'compare' => 'LIKE'
	            ),
	            array(
	                'key'     => 'email',
	                'value'   => $autor,
	                'compare' => 'LIKE'
	            )
	        )
	    ));

	$data = $q->get_results();

	foreach ($data as $key) {
		$authorID = $key->data->ID;
	}

	$nomeAutor = '';
	if( $authorID ){
		$nomeAutor = get_the_author_meta('display_name', $authorID);
	}else{

		$catAutor = get_term_by('slug', $autor, 'category_autor');
		if( $catAutor )
			$nomeAutor = $catAutor->name;
		
	}                   

}else{
	$nomeAutor = get_the_author_meta('display_name', $authorID);
}

$foto = get_avatar_url($authorID, ['size' => '200' ]);

if( $authorID ):
	$userInsta = xprofile_get_field_data('Instagram@', $authorID);
	$userFace = xprofile_get_field_data('facebook.com/', $authorID);
	$userTwitter = xprofile_get_field_data('Twitter @', $authorID);
	$userLattes = xprofile_get_field_data('http://lattes.cnpq.br/', $authorID);

endif;

if( $_GET['dev'] ):
	$field2 = xprofile_get_field($authorID);
	$field = get_userdata( bp_displayed_user_id($authorID) );
	print_r($field);
	print_r($authorID);
endif;

?>
<main class="autor-page">

	<section class="bannerTop">
		<div class="main">
			<figure><img src="<?php echo $foto; ?>" alt=""></figure>
			<div class="right">
				<div class="lineTop">
					<div class="name">
						<h2><?php echo $nomeAutor ?></h2>
					</div>
					<?php
					echo '<div class="redes">';
						if( $userInsta != '' )
							echo '<a href="https://www.instagram.com/'.$userInsta.'" target="_blank"><i class="fab fa-instagram"></i></a>';

						if( $userFace != '' )
							echo '<a href="https://www.facebook.com/'.$userFace.'" target="_blank"><i class="fab fa-facebook-f"></i></a>';

						if( $userTwitter != '' )
							echo '<a href="https://twitter.com/'.$userTwitter.'" target="_blank"><i class="fab fa-twitter"></i></a>';

						if( $userLattes != '' )
							echo '<a href="'.$userLattes.'" target="_blank">Lattes</a>';
					echo '</div></div>';
					?>
					<div class="text"><?php echo ($authorID ? xprofile_get_field_data('Sobre', $authorID) : '') ; ?></div>

				</div>
			</div>	
		</div>
	</section>

	<div class="barSearch">
		<form action="<?php echo $link_blog.'/autor' ?>">
			<input type="hidden" name="p" value="<?php echo $autor ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<?php
						$arrTypes = getAllTypeByAuthor($autor);
					?>
					<?php if( count($arrTypes) ): ?>
					<select name="tipo">
						<option value="">Filtrar por tipo</option>
						<?php
							foreach ($arrTypes as $key) {

								switch ($key) {
									case 'post':
										$valueA = 'Ludosfera';
										$slugA = 'ludosfera';
										break;
									
									case 'qd_arquibancada':
										$valueA = 'Arquibancada';
										$slugA = 'arquibancada';
										break;

									case 'qd_gallery_museu':
										$valueA = 'Museu';
										$slugA = 'museu';
										break;

									case 'qd_biblioteca':
										$valueA = 'Biblioteca';
										$slugA = 'biblioteca';
										break;

									case 'qd_gallery':
										$valueA = 'Futebol Arte';
										$slugA = 'futebol-arte';
										break;

									case 'qep_type_event':
										$valueA = 'Eventos';
										$slugA = 'eventos';
										break;

									case 'qd_entrevista':
										$valueA = 'Entrevista';
										$slugA = 'entrevista';
										break;
								}

								$selected = '';
								if( $_GET['tipo'] )
									if( $_GET['tipo'] == $slugA )
										$selected = 'selected';

								echo '<option value="'.$slugA.'" '.$selected.'>'.$valueA.'</option>';
							}
						?>
					</select>
					<?php endif; ?>
					<select name="ordem">
						<option value="">Organizar por</option>
						<option value="data" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'data' ){ echo 'selected'; } }?>>Recém-registrado</option>
						<option value="titulo" <?php if( isset($_GET['ordem']) ){ if( isset($_GET['ordem']) == 'titulo' ){ echo 'selected'; } } ?>>Ordem alfabética</option>
					</select>
				</div>
			</div>
		</form>
	</div>

	<?php include( get_stylesheet_directory() . '/inc/ads.php'); ?>


	<section class="bloco-posts first">
		<div class="main">

			<div class="posts col-crazy2">
				<?php

					if( isset($_GET['tipo']) ){
						switch ($_GET['tipo']) {
							case 'ludosfera':
								$tipo = 'post';
								break;
							
							case 'arquibancada':
								$tipo = 'qd_arquibancada';
								break;

							case 'museu':
								$tipo = 'qd_gallery_museu';
								break;

							case 'biblioteca':
								$tipo = 'qd_biblioteca';
								break;

							case 'futebol-arte':
								$tipo = 'qd_gallery';
								break;

							case 'eventos':
								$tipo = 'qep_type_event';
								break;
						}
					}else{
						// $tipo = array('post', 'qd_arquibancada');
						$tipo = array('post', 'qd_arquibancada', 'qd_entrevista', 'qd_gallery_museu', 'qd_gallery', 'qep_type_event', 'qd_biblioteca');
					}

					$args = array(
						'post_type'      => $tipo,
						'post_status'    => 'publish',
						'posts_per_page' => $showposts,
						'offset' 		 => $offset,
						'tax_query' => array(
					        array(
					            'taxonomy'  => 'category_autor',
					            'field'     => 'slug',
					            'terms'     => $autor
					        )
					    )
					);

					if( isset($_GET['busca']) )
						$args['s'] = $_GET['busca'];

					if( isset($_GET['ordem']) ){
						if ( $_GET['ordem'] == 'data' ) {
							$args['orderby'] = 'date';
							$args['order'] = 'DESC';
						}else{
							$args['orderby'] = 'title';
							$args['order'] = 'ASC';
						}
					}

					$loop = new WP_Query($args);
				?>
				<?php
					while ($loop->have_posts()) : $loop->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style01.php');
					endwhile;
				?>
			</div>

			<?php
				if( $loop->found_posts > $showposts ):
					echo '<div class="pagination">';
					echo paginate_links(array(
					    'format' => '?pagina=%#%',
					    'current' => $paged,
					    'total' => ceil($loop->found_posts / $showposts ),
					    'prev_text' => __('<'),
						'next_text' => __('>'),
					));
					echo '</div>';
				endif;
			?>
		</div>

	</section>

	<?php
		$args = array(
			'post_type'      => 'product',
			'post_status'    => 'publish',
			'category_autor' => $autor
		);
		$result = new WP_Query($args);

		$IDs = [];
		while ($result->have_posts()) : $result->the_post();
			array_push($IDs, $post->ID);
		endwhile;
	?>

	<?php if( count($IDs) > 0 ): ?>
	<section class="productsGray">
		<div class="main">
			<div class="title">Obras de <?php echo $cate->name ?></div>
			<?php echo do_shortcode('[products limit=4 category="apoie, cursos" cat_operator="NOT IN" ids="'.implode(',', $IDs).'"]') ?>
		</div>
	</section>
	<?php endif; ?>


	<?php
		if( get_field('grupo_de_pesquisa', 'user_'.$authorID) ){
			$grupoC = get_field('grupo_de_pesquisa', 'user_'.$authorID);
			$args = array(
				'role__in' => array('contributor', 'administrator'),
				'meta_query' => array(
			        array(
			        	'relation' => 'OR',
			            array(
			            	'key'     => 'user_last_login',
				            'compare' => 'NOT EXISTS'
			            ),
			            array(
			            	'key'     => 'user_last_login',
				            'compare' => 'EXISTS'
			            )
			        )
			    ),
				'orderby'  => 'meta_value',
				'order'    => 'DESC'
			);

			$users = new WP_User_Query($args);
			$arrayUser = [];
			foreach ( $users->get_results() as $user ) {
				if( $user->ID == $authorID ) continue;

				$grupo = get_field('grupo_de_pesquisa', 'user_'.$user->ID);
				if( $grupo ):
					foreach ($grupo as $keyG) {
						if ( in_array($keyG, $grupoC)) {
							if( count($arrayUser) <= 4 )
								array_push($arrayUser, $user->ID);
						}
					}
				endif;
			}
			array_unique($arrayUser);
		}
	?>

	<?php if( $arrayUser ): ?>
	<div class="ctnUserRelated">
		<div class="main">
			<h4 class="titleGrid">Integrantes do grupo de pesquisa</h4>
			<ul class="grid">
				<?php
					foreach ( $arrayUser as $user ) {
						$user_info = get_userdata($user);
						$foto = get_avatar_url($user, ['size' => '200' ]);
						$link = $link_blog.'/autor/?p='.$user_info->user_nicename;

					    echo '<li class="_item">
					    		<a href="'.$link.'">
									<figure><img src="'.$foto.'" alt=""></figure>
									<h5>'.esc_html( $user_info->display_name ).'</h5>
								</a>
							</li>';
					}
				?>
			</ul>
		</div>
	</div>
	<?php endif; ?>

</main>

<?php
get_footer();