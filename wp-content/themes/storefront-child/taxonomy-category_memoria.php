<?php
get_header();
global $url_tema, $nome_blog, $link_blog;
$cate = get_queried_object();

$paged = 1;
if( isset($_GET['page']) )
	$paged = $_GET['page'];
$showposts = 10;
$offset = ($paged - 1) * $showposts;


if( isset( $_GET['mes'] ) ): $month =  $_GET['mes']; else: $month = null; endif;
if( isset( $_GET['ano'] ) ): $year =  $_GET['ano']; else: $year = null; endif;



if( !$year && !$month ):

	$filtered_args = array(
		'post_type' => 'qd_memoria',
		'posts_per_page' => $showposts,
		'offset'		 => $offset,
		'category_memoria'  => $cate->slug,
	);

else:

	// Todos posts do ano X
	if( $year && !$month ):
		$start_date = strtotime( $year.'-01-01' );
		$end_date = strtotime( $year.'-12-31' );
	endif;


	// Todos posts de todos anos do mes X
	if( !$year && $month ):
		$args = array(
			'post_type' => 'qd_memoria',
			'numberposts' => -1
			);
		$posts_data = get_posts( $args );

		foreach( $posts_data as $post ):
			$meta_date = get_post_meta( $post->ID, 'qd_article_time_line_date', true );
			$meta_month = date( 'n', (int)$meta_date );
			if( $meta_month != $month ):
				$posts_not_in_month[] = (int)$post->ID;
			endif;
		endforeach;
	endif;

	// Todos posts do ano X e do mes X
	if( $year && $month ):
		$start_date = strtotime( $year.'-'.$month.'-01' );
		$end_date = strtotime( $year.'-'.$month.'-31' );
	endif;


	if( !$year && $month ):
		$filtered_args = array(
			'post_type' => 'qd_memoria',
			'post__not_in' => $posts_not_in_month,
			'posts_per_page' => $showposts,
			'offset'		 => $offset,
			'category_memoria'  => $cate->slug,
		);
	else:
		$filtered_args = array(
			'post_type' => 'qd_memoria',
			'posts_per_page' => $showposts,
			'offset'		 => $offset,
			'meta_query' => array(
					array(
						'key'     => 'qd_article_time_line_date',
						'value'   => $start_date,
						'type'    => 'numeric',
						'compare' => '>=',
					),
					array(
						'key'     => 'qd_article_time_line_date',
						'value'   => $end_date,
						'type'    => 'numeric',
						'compare' => '<=',
					),
				),
			'category_memoria'  => $cate->slug,
		);
	endif;
endif;

if( isset($_GET['busca']) )
	$filtered_args['s'] = $_GET['busca'];

$args = array_merge( $wp_query->query_vars, $filtered_args );
// query_posts( $args );
$query = new WP_Query($args);

?>
<main class="ludosfera-page">

	<?php if( $query->have_posts() ): ?>
	<section class="sliderCategory">
		<div class="slider">
			<?php
				$i=0;
				while ($query->have_posts()) : $query->the_post();
					if( $i > 4 ) continue;
					include(get_stylesheet_directory() . '/inc/post/post-style06.php');
					$i++;
				endwhile;
			?>
		</div>
	</section>
	<?php else: echo '<br><br>'; ?>
	<?php endif; ?>
	
	<div class="barSearch">
		<form action="<?php echo $link_blog . '/memoria-categoria/' . $cate->slug ?>">
			<div class="main">
				<div class="form">
					<input type="text" name="busca" placeholder="Pesquisar" value="<?php if( isset($_GET['busca']) ) echo $_GET['busca']; ?>">
					<button><i class="fas fa-search"></i></button>
				</div>
				<div class="right">
					<select name="mes">
						<option value="">Por Mês</option>
						<?php
							$meses = ['01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'];
							foreach ($meses as $key => $value) {
								$selected = '';
								if( $_GET['mes'] ){ if( $_GET['mes'] == $key ){ $selected = 'selected'; } }
								echo '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
							}
						?>
					</select>
					<select name="ano">
						<option value="">Por Ano</option>
						<?php
							for ($i=date('Y'); $i >= 1800; $i--) { 
								$selected = '';
								if( $_GET['ano'] ){ if( $_GET['ano'] == $i ){ $selected = 'selected'; } }
								echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
							}
						?>
					</select>
					<div class="linkDropdown">
						<span>Campeonato <i class="fas fa-bars"></i></span>
						<div class="dropdown">
							<?php
							$terms = get_terms([
							    'taxonomy' => 'category_memoria',
							    'hide_empty' => false,
							]);
							foreach ($terms as $key) {
								echo '<a href="'.get_term_link($key->slug, 'category_memoria').'">'.$key->name.'</a>';
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>

	<?php if( $query->have_posts() ) include( get_stylesheet_directory() . '/inc/ads.php'); ?>

	<section class="bloco-posts">
		<div class="main">

			<?php if( !$query->have_posts() ) echo '<h3>Nenhum registro encontrado.</h3>'; ?>

			<div class="posts col-2">
				<?php
					while ($query->have_posts()) : $query->the_post();
						include(get_stylesheet_directory() . '/inc/post/post-style02.php');
					endwhile;
					wp_reset_postdata();
				?>
			</div>
		</div>
	</section>

	<?php include( get_stylesheet_directory() . '/inc/pagination.php'); ?>

	<?php if( $query->have_posts() ) include( get_stylesheet_directory() . '/inc/ads-02.php'); ?>

</main>

<?php
get_footer();