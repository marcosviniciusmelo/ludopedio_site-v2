<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'ludopedio' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

define( 'WP_HOME', 'http://localhost/www/ludopedio_site-v2' );
define( 'WP_SITEURL', 'http://localhost/www/ludopedio_site-v2' );

// define('UPLOADS', 'https://www.ludopedio.com.br/v2/content/uploads/');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'QJf05>_$((90~KR&UiS8cfvzr-?LJ9b#VW*RUsRny^eQI:@3YFTo5s*!6]-{oPv%' );
define( 'SECURE_AUTH_KEY',  'yF?>e;705<X`BP87GE337O^V?1V<3gVwGWAiQ^hf)FmL_}<;|z |a2#ANB#B(F@:' );
define( 'LOGGED_IN_KEY',    'y!/W88J5b]h92B9KDb/b@5ac9e`tPj4O1>ElrA%knPA(?64Cv8Ge?~^VNR8C5!gO' );
define( 'NONCE_KEY',        'T6n,Rf{=ua{mvv*vY(*4[U(re}A00oZh(4GV/ff|d*>Xa]6M;RUJr:fXiEW9UW51' );
define( 'AUTH_SALT',        'z2cj}#^BD~4uy~W~!Pn&.0*E]L2!rQlpM#)>^+7X)`=V5&ub2*AV`@3@#k_zadNN' );
define( 'SECURE_AUTH_SALT', '/xl=]3uLMhG7oQhum]P>oBSkI,/TK RHC3PKfGe11Z1{[uP>3,5LHxL#H&auv=V{' );
define( 'LOGGED_IN_SALT',   'JIB1TT$f,s<WPPZJwCJzxN/Kr#:C%Na_m~wS}CrRDrarx5,TKA!uvTa8`jCs-Id`' );
define( 'NONCE_SALT',       '-@(SHhf|Ohq*av,~^_v[sv=_4<6FjA:()><>&7)tO]kLhXp[n48&RbQsD<O(q{`.' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wplpd_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
